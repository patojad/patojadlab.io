---
title: "GTK y QT"
date: "2019-03-11"
description: "Ambas librerias tiene sus ventajas y hoy vamos a ver un poco mas afondo mas informacion sobre ambas"
type: "programacion"
tags: ["gtk", "qt", "librerias", "versus"]
category: ["Programacion"]
img: "https://i.postimg.cc/pXfgbk5p/gtkvsqt-patojad.jpg"
authors: ["PatoJAD"]
---

Es importante aclarar que no vamos a hablar de ningún escritorio (ni gnome, ni qt) que, si bien, son implementaciones de estas tecnologías no nos permiten analizar al 100% su potencial.

El uso de QT, en estos últimos años aumentó a pasos agigantados, se debe a sus facilidades y su potencial que es lo que básicamente busca todo programador. Y en su contraparte GTk está ofuscada por el crecimiento de QT y la promoción que tiene sus mayores exponentes.

{{< img src="https://i.postimg.cc/dVWNwfst/gtkvsqt2-patojad.png" >}}

### Optimización

En este aspecto, la lógica (basándonos erróneamente en sus exponentes Gnome y KDE) nos dicen que QT está mucho más optimizado que GTK. Aquí la primera sorpresa para todos, esto no es real, su rendimiento es muy similar, incluso, por los datos obtenidos, se podría afirmar que la diferencia de rendimiento es despreciable.

### Componentes primitivos

En lo que respecta a componentes primitivos, QT está mucho mejor logrado y con mejores terminaciones. Este detalle es importante para aquellos con problemas para diseñar interfaces gráficas. El tener componentes primitivos más "bonitos" reduce la posibilidad de generar Apps desagradables.
Claro está que no es correcto generar Apps con componentes primitivos nada más, y a la hora de empezar a personalizar en ambas librerías llegamos a generar componentes igual de hermosos.

### Convergencia

Hoy todos buscan la convergencia, pero la realidad es que nadie llegó a esto... O al menos eso parecía...
Estos días se comenzó a hablar mucho de puri.sm quién presenta el primer sistema operativo 100% convergente. Y mientras esperamos que lancen su primer mobile para saber el resultado final de esta novedad ya conocemos su librería libhandy la que se puede definir como "El Bootstrap del escritorio", dado que su función es adaptar la aplicación a la pantalla (algo que QT no tiene hoy en día o no al menos a este nivel, si podemos usar QT para apps móviles pero no para apps convergentes, o al menos con un solo código).
Está librería es increíble porque genera una evolución increíble, puede revolucionar todo. También deja muy mal parado a QT en este aspecto quién debe poner de su parte mucho para estar a este nivel.

### Conclusión

En primer lugar, aprendimos que no podemos juzgar las librerías por sus implementaciones, sin importar que tan conocidas sean. Esto nos va a limitar dado que no sabemos cuánto le dedica cada equipo a la optimización de su proyecto.
En cuanto a que elegir a la hora de desarrollar el planteo es muy simple, solo basta con saber con cuál nos sentimos más cómodos. Y sin olvidarnos que no debemos casarnos con una sola tecnología.

---
title: "Repositorio"
date: "2020-03-13"
description: "Repositorio para distribuciones basadas en debian con archivos compilados por PatoJAD y su comunidad complementado las distribuciones"
type: "page"
tags: ["repositorio","linux","ubuntu","debian"]
category: ["Pages"]
img: "https://blog.desdelinux.net/wp-content/uploads/2018/05/banner-repositorios_desdelinux-830x500.png"
authors: ["PatoJAD"]
---

Muchas personas me piden que compile algunas cosas para probar o utilizar versiones mas actuales, por eso creamos nuestro propio repositorio complementario para poder tener las cosas que compilamos y actualizamos.

Esto nos ofrece un conjunto de aplicaciones adicionales mantenidas en un repositorio donde todos podemos colaborar de forma activa (compilando y subiendo contenido) o pasiva (recomendando software o informando de actualizaciones)




## INSTALACIÓN



Hemos creado un paquete «.deb», que al instalarlo configurará automáticamente el enlace en el sources.list y las llaves de acceso en el sistema para simplificar todo el proceso de instalacion. podes descargarlo desde aqui.


{{< link text="Descargar" url="https://www.mediafire.com/file/41dqd4fnexnvim3/patojad-repository_0.0.1-amd64.deb/file" >}}


## INFO EXTRA



Después de instalar, actualiza los repositorios de tu distribucion (recomiendo personalmente usar `sudo apt update` o `sudo apt-get update`). O puedes, tambien, usar Synaptic para actualizar. Entonces podrás descargar e instalar cualquier paquete de nuestro repositorio.

Recorda que este repositorio se crea en base a la comunidad y como tal todos podemos colaborar con el mismo, no te quedes afuera y hace que tus apps preferidas esten siempre y bien actualizadas.

¿Queres pedir alguna app? ¿Queres colaborar compilando y subiendo? ¿Queres subir tu app? No lo dudes te sabes donde podes encontrarnos


{{< link text="Participa" url="https://t.me/PatoJADCommunity" >}}


## LISTA DE APPS


{{< repotable >}}


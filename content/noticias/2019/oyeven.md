---
title: "Oyeven una radio que se viste de heroe"
date: "2019-02-12"
description: "Caminando entre grupos nos encontramos con esta radio venezolana"
type: "noticias"
tags: ["Radio","Software Libre","Venezuela"]
category: ["Noticias"]
img: "https://i.postimg.cc/FK41yq4r/avatars-000086556984-4c88xe-t500x500.jpg"
authors: ["PatoJAD"]
---

El día de ayer me encontraba contento por la inauguración del nuevo sitio, y con el apoyo de muchas personas dandome sus opiniones y comentarios, en ese momento me encontré con un héroe, de esos que llevan el mensaje que todos portamos en otro nivel y quiero hablar un poco de esto...

### Radio Oyeven

{{< img src="https://www.oyeven.com/templates/oyeven/images/logo_oyeven.png" >}}

Como encontré en su {{< textlink url="http://www.oyeven.com/somos-oyeven" text="Sitio Oficial" >}} su descripción es excelente:

>Oyeven 106.9 FM, inscripta en la filosofía ecologista de defensa y protección de los espacios verdes y parques nacionales con los que contamos en la Gran Caracas y el territorio nacional; nos sumamos al esfuerzo del Movimiento Ecologista Venezolano (MEVEN), motor conservacionista del Frente Francisco de Miranda, en cuya sede hacemos vida.

>La nuestra es una emisora de expresión joven cuyo estandarte es el compromiso democrático y la participación protagónica para emitir valores de una nueva sociedad, mediante una programación que busca entretener, formar e informar a las y los jóvenes, la familia, las organizaciones y movimientos sociales de los cinco Municipios de la Gran Caracas.

>En el marco de la transformación que vive nuestro país y situada en el espectro de la FM, somos y seremos una alternativa radial comprometida con el crecimiento permanente de los nuevos valores, de la calidad y la socialización de sus micrófonos para los nuevos talentos en todas las expresiones artísticas sonoras de nuestro país, Latinoamérica y los pueblos del mundo.

>Oyeven 106.9 FM es la emisora que transmite desde el corazón de Catia para la Gran Caracas, A tu estilo.

{{< img src="https://www.oyeven.com/images/slider/barner-pagweb.png" >}}

### Jhon y Tecnatural

Dentro de esta gran radio se encuentra nuestro héroe llamado John, el contiene un programa que como dije se llama Tecnatural, pero mejor dejemos su propia descripción del mismo.

>Bueno tengo un programa de radio en Caracas - Venezuela llamado Tecnatural que se transmite a las 8 a 10 am los jueves el cual tiene objetivo de divulgar informaciones ambientales y promover el uso de software libre mediante noticias, recomendaciones

Un proyecto tan grande como este tiene que ser reconocido por todos los que usamos software libre y predicamos sobre el mismo, para todos nosotros tenemos un compromiso todos los jueves de 8 a 10 (hs de venezuela).




¿Qué piensas de este gran proyecto?, ¿Cómo colaborarías Vos con la tarea de difundirlo ?.

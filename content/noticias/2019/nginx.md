---
title: 'La policía rusa allana la oficina de NGINX en Moscú'
date: '2019-12-16'
description: 'El problema radica en una disputa acerca de la propiedad del código fuente del popular servidor web'
type: 'noticias'
tags: ["Seminario","EducacionIT","Marketing","Online","Gratis","Digital"]
category: ["Noticias"]
img: 'https://zdnet3.cbsistatic.com/hub/i/2019/12/12/816d00eb-6dab-4108-9508-40a06c0d3712/nginx.png'
authors: ["PatoJAD"]
---

La policía rusa ha allanado hoy las oficinas de Moscú de NGINX, Inc., una subsidiaria de F5 Networks y la compañía detrás de la tecnología de servidor web más popular de Internet. El equipo fue incautado y los empleados fueron detenidos para ser interrogados.

La policía de Moscú ejecutó la redada después de la semana pasada que el Grupo Rambler presentó una violación de derechos de autor contra NGINX Inc., reclamando la propiedad total del código del servidor web NGINX. El Grupo Rambler es la empresa matriz de rambler.ru, uno de los mayores buscadores y portales de Internet de Rusia.

Según las copias de la orden de búsqueda publicada en Twitter hoy, Rambler afirma que Igor Sysoev desarrolló NGINX mientras trabajaba como administrador del sistema para la compañía, por lo tanto, son los legítimos propietarios del proyecto.




## LA HISTORIA



Sysoev creó NGINX a principios de la década de 2000 y el código NGINX de código abierto en 2004. En 2009, fundó NGINX, Inc., una compañía de EE. UU., Para proporcionar herramientas adyacentes y servicios de soporte para implementaciones de NGINX. La compañía tiene su sede en San Francisco, pero tiene oficinas en todo el mundo, incluida Moscú. El código fuente del servidor NGINX aún es gratuito y se administra a través de un modelo de código abierto, aunque una gran parte de los principales contribuyentes del proyecto son empleados de NGINX, Inc., que tienen un firme control sobre la administración del proyecto.

Sysoev nunca negó haber creado NGINX mientras trabajaba en Rambler. En una entrevista de 2012 , Sysoev afirmó que desarrolló NGINX en su tiempo libre y que Rambler ni siquiera lo supo durante años. Dijo que el servidor se implementó por primera vez en los sitios web Rate.ee y zvuki.ru y Rambler comenzó a usarlo solo después de que un colega le preguntó al respecto.

En febrero de 2019, NGINX finalmente destronó a Apache HTTPD y se convirtió en el servidor más ampliamente implementado en Internet. Según la encuesta del servidor web de Netcraft de diciembre de 2019 , NGINX tiene una participación de mercado del 38%. Un mes después, en marzo de 2019, el proveedor de equipos de ciberseguridad y redes F5 Networks adquirió NGINX Inc. por $ 670 millones .




## EMPLEADO DE NGINX INFORMA EL ARRESTO DE SYSOEV



La noticia de la redada se volvió viral hoy cuando un empleado de NGINX publicó una captura de pantalla de la orden de allanamiento en Twitter. Más tarde eliminó el tweet a petición de la policía rusa. La redada fue confirmada por otros empleados.


{{< img src="https://zdnet3.cbsistatic.com/hub/i/2019/12/12/ecb521c8-4669-4eb3-97a9-29e8640ffc7f/rambler-tweet.png" >}}


El mismo empleado dijo que dos empleados de NGINX han sido detenidos durante la redada, incluido el creador de NGINX, el cofundador de NGINX Inc. y el actual CTO Igor Sysoev, así como su compañero cofundador Maxim Konovalov.


{{< img src="https://zdnet1.cbsistatic.com/hub/i/2019/12/12/1560df31-4043-425c-9a81-706377e5c0d2/rambler-tweet-arrest.png" >}}


Un portavoz de F5 Networks confirmó la redada, pero le dijo a ZDNet que todavía están recopilando los hechos y no tienen más comentarios en este momento.

Un portavoz del Grupo Rambler no pudo comentar, citando el escaso conocimiento del idioma inglés. Sin embargo, el Grupo Rambler proporcionó declaraciones en ruso a los medios de comunicación locales Kommersant y The Bell , confirmando que efectivamente presentaron una queja por infracción de derechos de autor contra NGINX el 4 de diciembre.

Rambler dijo que cedió los derechos legales para presentar demandas de derechos de autor contra NGINX Inc. a Lynwood Investments CY Ltd , una compañía con sede en Chipre "que tiene las competencias necesarias para restaurar la justicia en el tema de la propiedad de los derechos".

Leonid Volkov, jefe de gabinete del candidato presidencial Alexei Navalny, criticó la redada y el caso legal detrás, alegando que después de 15 años, el plazo de prescripción se ha agotado para presentar un reclamo por infracción de derechos de autor.



<div class="text-center">
<blockquote class="twitter-tweet" data-lang="es"><p lang="ru" dir="ltr">Ого.<br>Обыск в офисе nginx, одной из самых или даже самой успешной на мировом рынке чисто российских ИТ-компаний: мол, 15 лет Игорь Сысоев разрабатывал nginx в рабочее время, будучи сотрудником Рамблера!<br>Сроки давности? Не слышали!<br>Ну и вообще полный бред. <a href="https://t.co/D8HSOd8G8S">pic.twitter.com/D8HSOd8G8S</a></p>&mdash; Leonid Volkov (@leonidvolkov) <a href="https://twitter.com/leonidvolkov/status/1205094298095673344?ref_src=twsrc%5Etfw">12 de diciembre de 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>

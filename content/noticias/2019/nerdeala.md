---
title: 'Nerdear.la'
date: '2019-10-03'
description: '¡se acerca la sexta edición de Nerdear.la! Un evento NERD realizado en Buenos Aires Argentina'
type: 'noticias'
tags: ["Nerdearla","EducacionIT","Linux"]
category: ["Noticias"]
img: 'https://ci5.googleusercontent.com/proxy/f5yAEKaq14WWeM65PBHOgmyp5wrvX9sqbc-vWQJKSobgmDyf9abp_FnzVR-68VoN5I4xexfqrr5na4mUrRPt1tiWnarGUrkNksk1Oq3hbO4Ox67WH-ZarJSUXrZPPOvJK5twtx2lDs_hLmA5HtgnKq6fRePjFHV1oUE=s0-d-e1-ft#https://gallery.mailchimp.com/d14a705d3d7f7bbfbb93d49e0/images/ace77f84-1e56-482d-be4b-9e9855dc8049.jpg'
authors: ["PatoJAD"]
---

{{< img src="https://ci5.googleusercontent.com/proxy/f5yAEKaq14WWeM65PBHOgmyp5wrvX9sqbc-vWQJKSobgmDyf9abp_FnzVR-68VoN5I4xexfqrr5na4mUrRPt1tiWnarGUrkNksk1Oq3hbO4Ox67WH-ZarJSUXrZPPOvJK5twtx2lDs_hLmA5HtgnKq6fRePjFHV1oUE=s0-d-e1-ft#https://gallery.mailchimp.com/d14a705d3d7f7bbfbb93d49e0/images/ace77f84-1e56-482d-be4b-9e9855dc8049.jpg" >}}


Nerdear.la es un evento en el que se llevarán a cabo 50 actividades relacionadas con charlas, talleres y coworkings, sobre Desarrollo, Infraestructura, Seguridad Informática, Robótica y más. La idea es aprender, divertirse y conocer gente como vos **¡y todo 100% gratis!**

El encuentro tendrá lugar del jueves 17 al sábado 19 de octubre, de 9 a 18 hs., en el Centro Cultural Kónex de la Ciudad de Buenos Aires (Sarmiento 3131).



> nerdear.la es un evento pensado principalmente para nerds, y gente de sistemas


{{< img src="https://nerdear.la/wp-content/uploads/2019/04/coworking5.jpg" >}}


## Agenda



#### **Día 1 (Jueves, 17 de octubre)**

**Charlas técnicas + coworking + actividades**

La idea es que vengas al evento y realices tus labores como lo harías cualquier día de trabajo, pero con la ventaja de contar con profesionales a tu alrededor, con quienes vas a poder interactuar y realizarles preguntas.



#### **Día 2 (Viernes, 18 de octubre)**

**Charlas técnicas + talleres**

En paralelo con el coworking, se darán charlas todo el día. El contenido está dedicado principalmente a tecnologías y productos en auge, además de mejores prácticas, implementación, casos de éxito e historias de horror. Buscamos que todas tengan un hilo conductor y que te dejen algo que puedas aplicar el lunes después del evento en tu trabajo.

**Lightning talks**

Cerramos la jornada invitando al público a pasar al frente a contar en 5 minutos o menos lo que quieran y consideren que puede ser de interés. ¡Animate!


 


#### **Día 3 (Sábado, 19 de octubre)**

**Charlas nerd + técnicas y talleres**

Sumadas a las charlas técnicas, se exponen temas no necesariamente ligados con nuestro trabajo pero sí de gran interés para el público nerd.



Tambien podes ver especificamente la agenda ingresando desde {{< textlink text="aquí" url="https://nerdear.la/agenda/" >}}



## Sobre el evento en general



No nos interesa que S.O. utilizás, si sabés programar o no, ni tampoco tu experiencia. Este es un evento en donde podés venir, sentarte y:

* Compartir lo que estás haciendo
* Buscar ayuda para tus issues
* Asistir a las charlas y talleres
* Conocer gente
* Jugar jueguitos


 


## ¿Qué necesitás traer?



* Tu laptop o dispositivo móvil
* El cargador (vas a estar en el evento por horas y no vas a querer irte)
* Lo que sea necesario para ejecutar tus cosas
* Recomendamos también: Candado para laptop, VPN, Multi-factor authentication, ganas de aprender y divertirte



{{< link url="https://nerdear.la/?utm_source=PatoJAD&utm_medium=web&utm_campaign=patojad" text="Sumate">}}

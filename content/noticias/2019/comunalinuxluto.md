---
title: 'La Comunidad Linuxera esta de luto'
date: '2018-12-02'
description: 'Comunidad de luto, no hay tiempo para bromas'
type: 'noticias'
tags: ["Linux","Comunidad","luto"]
category: ["noticias"]
img: 'https://i.postimg.cc/prmDD6jL/Capturefedora.png'
authors: ["PatoJAD"]
---

Más allá de cualquier noticia de broma que nos podamos encontrar hablando de que supuestamente Firefox se rinde ante Chromium (lo cual no es verdad) no tenemos que perder el foco de las cosas importantes y hoy lamentablemente lo importante es una noticia triste.

Como podemos ver en su Twitter del dia 24 de diciembre, Fedora nos trae una triste noticia.

{{< img src="https://i.postimg.cc/prmDD6jL/Capturefedora.png" >}}

> El Proyecto Fedora lamenta la pérdida del Líder del Proyecto Fedora @gafton. Todas nuestras simpatías y apoyo se extienden a su familia.

El Líder de proyecto Cristian Gafton nos ha dejado. Y esta noticia es muy triste para todos mas allá de que usen Fedora o no, dado que es un hombre que se fue respirando libertad y trabajando por la misma.

En lo personal me duele mucho que esto no salga entre los blogs o sitios mas populares que si se dedican a desinformar con noticias falsas.

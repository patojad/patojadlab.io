---
title: 'Latin Linux, una radio Libre'
date: '2019-02-13'
description: 'Ayer hablamos de un Dial sobre Software libre! Hoy hablamos de una Radio Libre'
type: 'noticias'
tags: ["Latin Linux","Libre","Radio"]
category: ["noticias"]
img: 'https://i.postimg.cc/Pr0CxJV8/latinlinux.png'
authors: ["PatoJAD"]
---

Ayer hablamos de {{< textlink url="https://patojad.com.ar/noticias/oyeven/" text="Oyeven" >}} ¡ Una radio que habla de software libre!.

### Radio Latin Linux

Las radios son un éxito como medio de difusión de contenidos, en este caso queremos traerles no solo un programa, sino ¡Una radio!.

Pero no es cualquier radio, es una radio completamente libre donde incluso la música que pasan las 24 hs. es COMPLETAMENTE LIBRE. ¡Y esto no es para menos, estamos ante la posibilidad de poder escuchar libertad las 24 hs. !.

Dejamos la descripción de la propia radio:

>En este sitio radial hablaremos de distros como Debian, Manjaro, Slackware, Deepin, Arch, Fedora, Antix, FSF Pure OS, Red Hat, Gentoo, Antergos, Parrot, Kali y sobre diversos temas, entrevistas, charlas con amigos, humor y sobre todo alma de comunidad.
Todos estan invitados a participar. Sumate.

Y hay un detalle ULTRA importante, ¡Vos podés sumarte a la radio!. ¡No esperes más y comenzá a ser parte de esta gran oportunidad!.




{{< link url="https://latinlinux.com/radio-latin-linux" text="ir a la radio">}}

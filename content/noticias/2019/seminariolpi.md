---
title: 'Seminario Exclusivo de Linux LPI'
date: '2019-10-15'
description: 'En un encuentro exclusivo, Hernán Pachas Magallanes, embajador oficial de la Linux Professional Institute en Latinoamérica, estará brindando un seminario gratuito acerca del estado de Linux, LPI y el Open Source en Latinoamérica.'
type: 'noticias'
tags: ["LPI","EducacionIT","Linux"]
category: ["Noticias"]
img: 'https://blog.educacionit.com/wp-content/uploads/2019/10/blog_educacionit_linux.jpg'
authors: ["PatoJAD"]
---

El próximo lunes 21 de octubre de 19:00 a 21:00 realizaremos un seminario presencial exclusivo para nuestros alumnos con la Linux Professional Institute. En esta oportunidad nos acompañará Hernán Pachas Magallanes Embajador Oficial de LPI en Latinoamérica, quien vendrá a conversar sobre el estado de Linux, LPI y el Open Source en nuestra región.

Este encuentro será una oportunidad única para escuchar y conversar con un profesional con más de 20 años de experiencia en el mundo de tecnologías Open Source sobre temas como Blockchain, Inteligencia Artificial, Internet of Things, SmartCitys y cómo la Linux LPI acompaña el desarrollo de estas áreas.

El Linux Professional Institute (LPI) es una de las organizaciones más grandes del mundo sin ánimo de lucro que otorga certificaciones profesionales de Linux a administradores de sistema, así como, programadores. Con sede en Canadá, cuenta con miembros activos en todo el mundo.


 


## Temarios



* El estado actual de las tecnologías : Blockchain, IA, IoT, SmartCity, etc
* El core de las denominadas «nuevas tecnologías»
* Demanda de la Industria (Estadísticas)
* Oferta de Profesionales/Técnicos en Tecnología (+gerentes vs -operadores)
* Oportunidades de Linux y el Open Source en Argentina
* Que tienen en común: Emprendedor, Promoción en el trabajo, Nuevos lugar de trabajo?
* Certificando las habilidades en Tecnologías Linux y Open Source
* LPI como engranaje del Ecosistema Linux y Open Source
* Presencia de LPI en el Mundo
* Cantidad de Certificados
* Exámenes Linux y Open Sources disponibles en LPI
* EducacionIT Centro oficial de Entrenamiento y Certificación LPI


 


## Modalidad



* Presencial
* Lavalle 648, Piso 8, CABA



## Información



* Lunes 21 de octubre
* 19 a 21 hs.



## Fromulario



<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScCEIL0l6sUQfmfE5ruJcmWgJh_sEOqokb7NYCVfpK9Evqmjg/viewform?embedded=true" width="100%" height="520" marginheight="0" marginwidth="0">Cargando…</iframe>

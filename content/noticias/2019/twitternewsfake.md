---
title: 'Twitter contra las Deepfakes'
date: '2019-11-15'
description: 'La red social elaboró un borrador que incluye las medidas que implementará para hacerle frente a las deepfakes y lo compartió con los usuarios para que puedan dar su opinión'
type: 'noticias'
tags: ["Twitter","deepfakes","publico"]
category: ["Noticias"]
img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpP9r43fyXcxuE8JlffZMU-xoZm4nUisFiJicZTkyhawHrLcc6&s'
authors: ["PatoJAD"]
---

Para quienes no están familiarizados con el término, se conoce como deepfake a la técnica de síntesis de imagen humana basada en inteligencia artificial que se utiliza para crear contenido falso, ya sea desde cero o utilizando videos existentes con el fin de imitar la apariencia y el sonido de una persona real. Este fenómeno se encuentra con un crecimiento.



## Twitter al Rescate



Dado los intentos deliberados de engañar y confundir a las personas mediante contenido multimedia manipulado y de atentar contra la integridad de las conversaciones, decidieron compartir con el público el borrador de lo que serían las medidas que implementarán para hacerle frente a las deepfakes.



<div class="text-center"><blockquote class="twitter-tweet" data-lang="es"><p lang="en" dir="ltr">We’re always updating our rules based on how online behaviors change. We&#39;re working on a new policy to address synthetic and manipulated media on Twitter – but first we want to hear from you.</p>&mdash; Twitter Safety (@TwitterSafety) <a href="https://twitter.com/TwitterSafety/status/1186403736995807232?ref_src=twsrc%5Etfw">21 de octubre de 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>



el fin es hacerle frente a cualquier foto, audio o video que fue creado o alterado de forma significativa con la intención de engañar a las personas o cambiar su sentido original. Algunas veces esto entra en la categoría de deepfakes y otras veces en la de shallowfakes, explican.



## El borrador



Como parte del borrador, la red social explica que ante estos casos hará lo siguiente:



* Colocará un aviso junto a los tweets que compartan contenidos manipulados;
* Advertirán a los usuarios antes de que compartan o pongan “me gusta” en tweets con contenidos manipulados;
* Añadirán un enlace, por ejemplo, a una noticia o a un momento de Twitter, que servirá a los usuarios para que puedan informarse acerca de por qué varias fuentes consideran que un contenido entra en la categoría de deepfake.



Adicionalmente, si un tweet incluye un contenido de esta naturaleza con el fin de realizar un engaño o puede representar una amenaza para la seguridad física o conducir a otro tipo de daño, el mismo será eliminado.



## El rol del usuario



Para conocer la opinión de los usuarios acerca de este tema, Twitter publicó una breve {{< textlink text="encuesta" url="https://survey.twitterfeedback.com/survey/selfserve/53b/191016?list=3&co=BLOG#?" >}} disponible en varios idiomas, aunque también existe la posibilidad de que los usuarios compartan su opinión a través de la red social utilizando el hashtag #TwitterPolicyFeedback. Hasta el 27 de noviembre hay tiempo para enviar opiniones sobre el borrador de la política.

Como ya hablamos anteriormente en {{< textlink text="el rol del usuario" url="https://patojad.com.ar/noticias/2019/07/el-rol-del-usuario-en-el-software-libre" >}} es importante colaborar activamente con la tecnología para llegar a buen puerto.

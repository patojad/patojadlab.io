---
title: '"La Consola" un software para los chicos con autismo'
date: '2019-10-06'
description: 'Hacer una aplicacion que detecte "imagenes reales" y las transforme a dibujo'
type: 'noticias'
tags: ["Tomas Basile","La Console","Hackaton"]
category: ["Noticias"]
img: 'https://scontent.faep8-1.fna.fbcdn.net/v/t1.0-9/71496454_2601548286570786_1833048349230497792_n.png?_nc_cat=106&_nc_oc=AQk_k0XzdjmuixMC_mxXt_jPpMnOJsU8kJWBE-5eHI4j4haLQTADDPy_a7NneA5zm_g&_nc_ht=scontent.faep8-1.fna&oh=6ea0848cb785f1bb19c729b5da8a0459&oe=5E3AD803'
authors: ["PatoJAD"]
---

Estos días me encontré con un proyecto de un compañero de añares y me sentí muy orgulloso al ver el sector que abarca, su impronta y obviamente que desarrollara software libre.


{{< img src="https://scontent.faep8-1.fna.fbcdn.net/v/t1.0-9/71496454_2601548286570786_1833048349230497792_n.png?_nc_cat=106&_nc_oc=AQk_k0XzdjmuixMC_mxXt_jPpMnOJsU8kJWBE-5eHI4j4haLQTADDPy_a7NneA5zm_g&_nc_ht=scontent.faep8-1.fna&oh=6ea0848cb785f1bb19c729b5da8a0459&oe=5E3AD803" >}}


> Hacer una aplicacion que detecte "imagenes reales" y las transforme a dibujo, ya que los chicos que sufren autismo tienen a aprender a reconocer objetos a través de los detalles, es una manera de aislar la imagen de todo lo externo para que puedan tomar y aprender el concepto
> {{< citaname name="Tomas Basile" >}}


 


Como acá nos describe nuestro compañero el proyecto se basa en una App (Mobile) que transforme imágenes en dibujos aislados para poder mejorar el aprendizaje de niños que sufren autismo.



Lo que les quiero pedir desde mi postura es apoyar este proyecto para que ellos lo puedan llevar a cabo al 100% esto solo requiere un MG en una publicación de Facebook

{{< link text="Darle MG" url="https://www.facebook.com/BelatrixSoftware/photos/pb.114123765313263.-2207520000.1570285184./2601548283237453/?type=3&theater">}}


Por nuestra parte comprometimos a {{< textlink text="Vasak Group" url="https://vasak.net.ar">}} a proporcionarle la cuenta de Google Play para subirlo

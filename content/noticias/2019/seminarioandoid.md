---
title: 'Seminario de Android Gratis'
date: '2019-11-08'
description: 'En este seminario explicaremos qué es Android, sus características, sus lenguajes para desarrollar y el entorno de desarrollo a utilizar.'
type: 'noticias'
tags: ["Seminario","EducacionIT","Andoid","Online","Gratis"]
category: ["Noticias"]
img: 'https://i.postimg.cc/Xv9yr0Qz/tablet-1442900-1280.jpg'
authors: ["PatoJAD"]
---

El próximo miercoles 13 de Noviembre de 19:00 a 21:00 hs la gente de EducacionIT realizaran un seminario online orientado a Android y el desarrollo de aplicaciones.


{{< img src="https://www.neostages.com/wp-content/uploads/2016/05/app-movil-380x380.png" >}}


> En este seminario explicaremos qué es Android, sus características, sus lenguajes para desarrollar y el entorno de desarrollo a utilizar. Abordaremos el mercado de tecnología de la información y su salida laboral, las empresas que solicitan empleados para trabajar en Android, la importancia de esta tecnología en el ambiente empresarial, y por qué es una de las tecnologías más solicitadas y utilizadas tanto en el mercado local como a nivel internacional.
>
> {{< citaname name="EducacionIT" >}}


 


## Temarios



* Qué es Android
* Cómo iniciar a desarrollar una App
* Distribución de una App
* Desafíos
* Puntos claves
* El día a día de un desarrollador Android
* Mercado laboral


{{< img src="http://www.movilzona.es//app/uploads/2016/01/AVD-manager-en-Android-Studio.png" >}}


## Modalidad



* Online
* 19:00 a 21:00 hs
* Miercoles 13 de Noviembre




## A quien va dirigido



Este seminario está orientado a todas aquellas personas que deseen introducirse en el mundo de Android, recomendablemente con conocimientos en programación, y estén interesados en convertirse en expertos en esta tecnología.



{{< link url="https://www.educacionit.com/seminario-de-desarrollo-mobile?cod=38597" text="Inscripcion" >}}

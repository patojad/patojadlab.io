---
title: 'Seminario gratuito Linux'
date: '2019-09-25'
description: 'Seminario gratuito sobre los aspectos clave para convertirte en un administrador Linux. ¿sabías que grandes empresas del mundo como Google, Wall Street, la NASA o los principales bancos del mundo utilizan servidores Linux? '
type: 'noticias'
tags: ["Seminario","gratuito","Linux"]
category: ["Noticias"]
img: 'https://ci5.googleusercontent.com/proxy/3oeib50SnBrpnjjJDmDylc8mclsHm_drKKWAEd8TuBa5u56LrZlchTft8DmbZ5sKc9GVViz38JxGIKyYQtYMxXkSQYMCYnQBbGlI9pDPR97hK32mMZ_0nhdjgpiaCkbA2lC-EmVcOWRX8bv0nDUvdg4SdzcDUyWgr64=s0-d-e1-ft#https://gallery.mailchimp.com/d14a705d3d7f7bbfbb93d49e0/images/5f8d017f-b15d-4cf9-9ebb-6a6195697dc3.jpg'
authors: ["PatoJAD"]
---

> Te invitamos a participar el próximo miércoles 2 de octubre de un Seminario Gratuito y Online sobre las principales bases de los servidores de Linux y su interacción con el ambiente empresarial. Además, cómo se configura, instala y mantiene este tipo de servidores.
>
> {{< citaname name="EducacionIT" >}}

¡La gente de EducacionIT nos están ofreciendo un seminario online gratuito sobre Administración Linux y nosotros te lo queremos acercar a vos!

El link de acceso será enviado a los inscritos 30 minutos antes del inicio del seminario.


 


## Contenido

* Introducción a Linux
* Linux como salida laboral
* Servidores Linux (Soluciones con Linux, Servidor Web, Servidor DNS, Servidor ftp, squid, firewall, mysql, etc., Servidor Mail)
* Virtualización con Linux
* Primer contacto con Linux
* Roles de un administrador
* LPI: la carrera hacia la certificación



## Modalidad
* Online
* Facebook Live & YouTube



## Información
* Miércoles 2 de octubre
* 19 a 21 hs. (ARG)

{{< link text="Inscribirse" url="https://www.educacionit.com/seminario-de-linux" >}}

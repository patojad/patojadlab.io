---
title: 'OpenBook: Un Paso mas'
date: '2019-06-14'
description: 'Ya sabiamos que OpenBook Nacio como iniciativa pero hoy sabemos un poco mas'
type: 'noticias'
tags: ["OpenBook","Facebook","OpenSource", "Libre"]
category: ["noticias"]
img: 'https://i.postimg.cc/g0VjRQrX/Capture.png'
authors: ["PatoJAD"]
---

Ya habíamos hablado antes de OpenBook, el "Facebook OpenSource" y muchos de nosotros nos inscribimos a la BETA esperando el día que nos informen que ya podríamos ingresar...

Mucho recibí desde la creación del {{< textlink link="https://patojad.com.ar/noticias/openbook/" text="primer post">}} como consultas de su avance y demás sin poder dar una respuesta certera a todo esto sin embargo hoy puedo notificar un gran Avance


{{< img src="https://i.postimg.cc/g0VjRQrX/Capture.png" >}}


Ya liberaron para los BETA la aplicación, si recibiste tu link ya podés ingresar en la nueva red social libre centralizada. Una nueva experiencia que promete cuidar tu privacidad intentando ganar todo el público que Facebook defraudó con sus alevosos movimientos de datos.

<div class="text-center">
  <a href="https://play.google.com/store/apps/details?id=social.openbook.app" target="_blank" rel="noopener">
    {{< img src="https://localixo.com/wp-content/uploads/getapp-btn-playstore-eS-300x88.png" >}}
  </a>
</div>

Espera tu Link de aprobación y contanos que te parece esta nueva app. Esperamos juntos muchas mas noticias

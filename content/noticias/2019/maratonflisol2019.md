---
title: 'El Maraton Linuxero nos adelanta el FLISOL 2019'
date: '2019-04-03'
description: 'Se nos acerca el FLISOL 2019 y La gente del maraton linuxero les dedica un espacio y nos trae todos los adelantos!'
type: 'noticias'
tags: ["Linux","Comunidad","luto"]
category: ["noticias"]
img: 'https://i.postimg.cc/1RW03Vjm/head-pato.png'
authors: ["PatoJAD"]
---

>Buenas amig@s Maratonistas!!! ¿Sabes que tienes un FLISOL muy cerca de tí? ¿Te gustaría disfrutarlo un poco antes? Maratón Linuxero vuelve con una edición especial del Festival de Software Libre con invitados y organizadores de varios países... te esperamos el día 20 de Abril... síguenos para más información!!!
>
>{{< citaname name="Iñaki P." >}}

Así inicia la invitación nuestro querido Iñaki a un evento que procura iniciar la emoción del Festival Latinoamericano de Instalación de Software Libre que se lleva acabo el día 27 de Abril (Una semana después del maratón). Como siempre podemos sintonizarlo desde {{< textlink url="https://maratonlinuxero.org/" text="la página oficial del maratón linuxero" >}} (Que también podemos encontrar en sitios amigos)

Esta edición especial tenemos la oportunidad de escuchar a muchos organizadores de diferentes lugares para conocer el detrás de escena de esta 15° edición del FLISoL

>El FLISoL es el evento de difusión de Software Libre más grande en Latinoamérica y está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas y aun personas que no poseen mucho conocimiento informático..
>
>El FLISoL se realiza desde el año 2005 y desde el 2008 se adoptó su realización el 4to sábado de abril de cada año. La entrada es gratuita y su principal objetivo es promover el uso del software libre, dando a conocer al público en general su filosofía, alcances, avances y desarrollo.
>
>El evento es organizado por las diversas comunidades locales de Software Libre y se desarrolla simultáneamente con eventos en los que se instala, de manera gratuita y totalmente legal, software libre en las computadoras que llevan los asistentes. Además, en forma paralela, se ofrecen charlas, ponencias y talleres, sobre temáticas locales, nacionales y latinoamericanas en torno al Software Libre, en toda su gama de expresiones: artística, académica, empresarial y social.
>
> {{< citaname name="flisol.info" >}}

{{< img src="https://flisol.info/FLISOL2015?action=AttachFile&do=get&target=QueEsSoftwareLibre.png" >}}

{{< img src="https://flisol.info/FLISOL2015?action=AttachFile&do=get&target=CodigoLibreComunidad.png" >}}

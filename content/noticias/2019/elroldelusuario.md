---
title: 'El Rol del usuario en el software Libre'
date: '2019-07-04'
description: 'El Software libre tiene una comunidad muy grande de desarrolladores, pero los usuarios tenemos un rol igual de importante'
type: 'noticias'
tags: ["Linux","Comunidad","Software Libre"]
category: ["noticias"]
img: 'https://www.revistamercado.do/wp-content/uploads/2019/04/4adad5d06af2bd2b43f0eae4a530713b.jpg'
authors: ["PatoJAD"]
---

Muchos hablamos de software Libre y de su comunidad de desarrolladores y los criticamos cuando los sistemas no funcionan como nosotros queremos. Esta actitud es muy común e incluso debo admitir que yo también soy muy crítico de proyectos como Gnome sin cumplir el rol que como usuario de Software Libre que me fue otorgado.

## ¿Qué es este ROL?

Antes que nada, tenemos que entender que esta cultura de “refunfuñar“ por el software que estamos utilizando viene heredado del uso de Software Privativo donde, si algo no nos gusta, solo debemos aceptarlo y seguir adelante con esa molestia.

 

Sin embargo, en el software libre somos críticos y actores del software que utilizamos. No necesariamente tenemos que ser programadores para poder tomar iniciativas para mejorar eso que nos genera alguna molestia. Todos los proyectos, y reitero, TODOS los proyectos tienen la posibilidad de reportar un error, un issus o contactarte con los desarrolladores para hacerles saber sobre eso que nos molesta.
Y este, queridos linuxnautas, es nuestra responsabilidad, ser actores del software libre y motivar el cambio y la evolución del software que utilizamos día a día. Contribuyendo con reportes, y participación activa en votaciones y demás.

---
title: 'OpenBook el Facebook Libre'
date: '2019-04-11'
description: 'Estos dias se abrió la inscripcion a la BETA de OpenBook la alternativa libre a Facebook y queremos contarte todo!'
type: 'noticias'
tags: ["OpenBook","Facebook","OpenSource", "Libre"]
category: ["noticias"]
img: 'https://i.postimg.cc/dtqkvD1m/portadaob.jpg'
authors: ["PatoJAD"]
---

OpenBook es una alternativa libre a FaceBook que nos promete cuidarnos y respetar nuestra privacidad.

{{< img src="https://i.postimg.cc/GhqBprT0/ob02.png" >}}

### Privacidad amigable
Navega con tranquilidad. Somos creyentes firmes, la privacidad es un derecho humano fundamental. No rastreamos ni monitoreamos su actividad.

---

### Personal
Con círculos sociales, listas, reacciones y temas personalizados, ¡haga de Open book su propia red personal!

---

### Transparente

Todo el código que alimenta la red es de código abierto, es decir, abierto para que todos lo vean. ¡No hay espacio para los algoritmos de abracadabra!

---

### Seguro por diseño

Venimos de un fondo de seguridad de la información. Estamos pensando en seguridad, desde el principio.

---

### Fácil de migrar

¿Viniendo de otra red social? ¡Estamos creando herramientas para que pueda importar sus datos en Openbook en unos simples pasos!

---

### Libre de Publicidad

Nuestro modelo de ingresos es una suscripción opcional llamada Openbook Gold que ofrece acceso a más reacciones, temas y opciones de personalización.

---

### Impulsado por la comunidad

Con más de 1900 patrocinadores y voluntarios, Openbook es más que una red social, es un movimiento.

---

### Bueno para el planeta

Creemos en un mundo mejor para todos. Es por eso que daremos el 30% de nuestras ganancias para hacer del mundo un lugar mejor, en asociación con FoundersPledge.

---
---

#### Estamos en una misión para construir tecnología centrada en el ser humano, responsable, sostenible y justa para un futuro más próspero.

Damos vida a nuestra misión diseñando nuestros productos y servicios con las mejores cualidades de la humanidad, como la bondad, la compasión, la tolerancia, la caridad, la empatía y la cooperación en su núcleo.

Openbook es nuestra primera creación. El proyecto se financió con éxito en septiembre de 2018 a través de una Campaña de Kickstarter.

Openbook se encuentra actualmente en pruebas alfa cerradas con más de 2000 usuarios y la versión beta estará disponible a mediados de 2019.

{{< img src="https://i.postimg.cc/05GML78y/ob01.png" >}}

Asi es como se presenta OpenBook, dice mucho y esperamos que cumpla, sin embargo ya nos incribimos para la BETA de esta red social que promete ser el futuro y lograr lo que G+ no pudo...

{{< link url="https://www.openbook.social/en/home" text="OpenBook BETA" >}}

---
title: 'Grupos de Telegram Recomendados I'
date: '2020-07-27 16:10:00'
description: 'En esta oportunidadviene a recomendarte una saga de grupos de Telegram donde puedes encontrar buena gente'
type: 'noticias'
tags: ["toxicidad", "telegram", "comunidad", "staff", "grupos"]
category: ["Noticias"]
img: 'https://cdn.shortpixel.ai/client/to_webp,q_lossless,ret_img,w_1024/https://martingalmarino.com.ar/wp-content/uploads/telegram-grupo-seo-1024x576.png'
authors: ["PatoJAD"]
---



Ya muchas veces hable de comunidades, de grupos de telegram y demás, pero siempre parece que todo está mal y que el mundo está lleno de toxicidad (y en parte lo está) Pero como siempre digo lo nuestro es ver lo bueno en lo malo, Para lo cual viene a recomendarte una saga de grupos de Telegram donde puedes encontrar buena gente (ojo, en algunos se escapa algún mala leche pero el staff mantiene bien controlado cada uno de los grupos)




## GNU/linux LATINOAMERICA


{{< img src="https://i.postimg.cc/y8W9j4ft/Screenshot-20200727-155553.png" >}}


Anteriormente Deepin Latinoamérica uno de los grupos de deepin más sanos que podrias encontrar, Como dije deepin tiene las comunidades más tóxicas y cerradas y en este caso nuestro amigo Federico Raika fundó una comunidad libre de esas cosas y apoyando siempre a los usuarios. Actualmente se extiende un poco más y no es solo deepin pero puedes encontrar mucha info sobre esta distro…


{{< link text="Unirsme al grupo" url="https://t.me/DeepinOSLatinoamerica" >}}



## Legion GNU/Linux


{{< img src="https://i.postimg.cc/RVgKwVTw/Screenshot-20200727-155508.png" >}}


Nuevamente seguimos con Fede, y en este caso es un grupo pensado con la consiga “compartir” acá no hay espacio para los problemas dado que siempre el staff los corta de raíz… En este grupo vamos a encontrar siempre una mano y mucha info de ayuda, todos los que estamos ahí lo hacemos por amor al prójimo…


{{< link text="Unirsme al grupo" url="https://t.me/Legiongnulinux" >}}



## VideoJuegos Open & Multiplataforma (y Offtopics)


{{< img src="https://i.postimg.cc/MHDRPWJ2/Screenshot-20200727-155411.png" >}}


Si los juegos son lo tuyo llegaste al lugar correcto, este grupo podemos encontrar gente muy amistosa que busca compartir videojuegos multiplataformas, no se cierran en linux, siempre hay espacio para todos los SO. Aquí no solo vemos novedades sino que siempre te invitaran a “echarte una partida entre amigos”.


{{< link text="Unirsme al grupo" url="https://t.me/JuegosOpen" >}}



## ~ LiNuXiToS ~


{{< img src="https://i.postimg.cc/CLhbZfbF/Screenshot-20200727-155449.png" >}}


Un poco de OffTopic y amistad, este canal vas a encontrar mucha gente que sabe mucho y en diferentes áreas, no es un canal para buscar disturbios ni nada por el estilo y eso es penalizado de inmediato. Aquí vas a poder encontrar charlas sobre lo que te imagines y siempre puedes aprender mucho en este pequeño gran grupo…


{{< link text="Unirsme al grupo" url="http://t.me/joinchat/EgyzlguOksnGTIHWKeJpDQ" >}}



## Karla's Project


{{< img src="https://i.postimg.cc/4dppc683/Screenshot-20200727-155327.png" >}}


Poco puedo hablar de este grupo dado que llevo muy poco tiempo, sin embargo note el amor que tienen por el grupo y obviamente por la más que popular estrella de youtube Karla. Al igual que su canal y su web este grupo es de tecnología y no repudian a nadie, incluso es muy común ver hilos sobre Windows


{{< link text="Unirsme al grupo" url="https://t.me/KarlasProject" >}}



## Proyecto Tic Tac (Grupo)


{{< img src="https://i.postimg.cc/Wp6Mb7FW/Screenshot-20200727-155430.png" >}}


Un grupo que busca compartir información, Todos conocemos el proyecto Tic Tac y sabemos el esfuerzo increíble que realiza su staff para mantener limpio el grupo y lleno de amor.


{{< link text="Unirsme al grupo" url="https://t.me/grupo_telegram_proyectotictac" >}}



## TRUE GNU/Linux sin systemd


{{< img src="https://i.postimg.cc/zGMhPt6B/Screenshot-20200727-155612.png" >}}


No puede faltar un grupo que toque la temática SystemD en este caso es un grupo puritano que nos ayuda a eliminar SystemD de nuestro sistema. Puede que sea un grupo extremista pero si estas en esta onda es realmente útil…


{{< link text="Unirsme al grupo" url="https://t.me/nosystemd" >}}



## Comunidad Latina TL


{{< img src="https://i.postimg.cc/KYMTX7h5/Screenshot-20200727-155637.png" >}}


Si te interesa ver el activismo por el Software Libre este es un gran ejemplo y latinoamericano, se realiza mucho aporte sobre el software libre y es una comunidad muy amistosa. Es muy útil para los que están iniciando.


{{< link text="Unirsme al grupo" url="https://t.me/comunidadlatinatl" >}}



## mint • compartimos y ayudamos


{{< img src="https://i.postimg.cc/6qrsFmMN/Screenshot-20200727-160235.png" >}}


Grupo que se centra en Linux Mint, lleva un nivel de amistad increíble… Tiene mucho poder de emprendimiento y organizan unas salas de jitsi realmente muy buenas y constructivas!


{{< link text="Unirsme al grupo" url="https://t.me/mcalinuxmint" >}}



## PatoJAD Community


{{< img src="https://i.postimg.cc/J0mZ5sg5/Screenshot-20200727-155350.png" >}}


Claramente este grupo no puede faltar porque el staff me mataría a golpes y enterraría mi cuerpo en el lugar más recóndito de este hermoso mundo. Ahora si hablando de verdad es un grupo que recomiendo y no porque lo haya fundado yo, sino porque es un grupo que demostró ir migrando a lo largo que los usuarios ingresaban y planteaban temáticas… Inicio hablando de Software Libre y hoy habla de tecnología en general…


{{< link text="Unirsme al grupo" url="https://t.me/PatoJADCommunity" >}}



No te olvides de dejarnos tu grupo...



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

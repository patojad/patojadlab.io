---
title: '¿SystemD no es el problema? Respuesta a Zatiel'
date: '2020-07-06 08:34:00'
description: 'Zatiel se tomo su tiempo para remarcar que SystemD no es le problema, yo me tomo el mio para responder'
type: 'noticias'
tags: ["systemd", "inits", "linux", "zatiel", "problema", "respuesta"]
category: ["Noticias"]
img: 'https://img.youtube.com/vi/iq1hyPOA0rA/maxresdefault.jpg'
authors: ["PatoJAD"]
---



Ya en este blog es un poco recurrente hablar de inits, en especial del tan aclamado SystemD un init tan popular que muchas personas dudan de que puedan existir otros en su lugar. Las miradas sobre este init suelen ser muy divididas. Mientras existe un grupo de personas que lo defienden a muerte otros intentan extinguir a como dé lugar… Después estamos lo que estamos en un limbo bajo la teoría de “vive y deja vivir” donde si bien no procedemos a estar de acuerdo con SystemD en general tampoco linchamos a quienes lo usan…




## El origen

En esta ocasión no vengo a hablar de init simplemente por generar polémica(¿o si?), o porque sea un tema que suele generar bastantes entradas. Simplemente lo hago para responder el punto de vista de una de las estrellas Linuxtubers más famosas de los últimos tiempo. Sí, estoy hablando del único e inigualable Zatiel, el cual publicó hace unos días el siguiente Video:


{{< youtube code="iq1hyPOA0rA" >}}


En el cual plantea su idea sobre SystemD y es ampliamente válida…




## Mi respuesta



Siempre desde la amistad me tomé el atrevimiento de responder, no porque creo que Zatiel este confundido ni mucho menos, simplemente porque la concepción de “lo que está mal” de systemD no es la realidad de SystemD. Para lo cual lo hice el siguiente video:


{{< youtube code="x4EUIrLeA_0" >}}


Antes de verlo recuerdo que estaría bueno ver el video de Zatiel si ya no lo viste…




## Antecedentes



Como dije antes es recurrente en este blog hablar de inits, tal vez a estas alturas muchos ya los tengan entendido (a su funcionalidad) pero otros no para lo cual estoy dejando abajo todo lo que nosotros ya hablamos anteriormente con el fin de obtener más información



* https://patojad.com.ar/linux/2020/06/systemd-solucion-o-problema/
* https://patojad.com.ar/linux/2019/04/systemd-al-volante-mata/
* https://patojad.com.ar/comunidad/2019/01/lo-que-la-gente-opina-sobre-los-inits/
* https://patojad.com.ar/linux/2019/01/lo-que-la-gente-opina-sobre-los-inits/



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

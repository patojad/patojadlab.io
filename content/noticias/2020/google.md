---
title: 'Google tiene una caida Global'
date: '2020-12-14 09:10:00'
description: 'Te traemos una de las mejores revistas de GNU/Linux en español en la cual tenemos el honor de ocupar un pequeño espacio'
type: 'noticias'
tags: ["noticia", "google", "youtube", "docs", "caido", "offline", "parcial", "global", "2020"]
category: ["Noticias"]
img: 'https://i.postimg.cc/dt3tnvFZ/image.png'
authors: ["PatoJAD"]
---


{{< img src="https://i.postimg.cc/dt3tnvFZ/image.png" >}}


El día de hoy pudimos encontrar (a partir de las 9 AM de Argentina) al servicio más monopólico de la internet con una caída parcial que está afectando múltiples servicios. Si, estamos hablando de nada más ni nada menos que el mismísimo google que al parecer comenzó con una caída parcial internacional donde se reportan caídas de múltiples servicios ofrecidos por esta empresa.


{{< img src="https://i.postimg.cc/prpbQWj2/image.png" >}}



## ¿Hasta donde se extiende?


{{< img src="https://i.postimg.cc/KYF6qrTp/image.png" >}}


El error se extiende por todas las plataformas como **Google**, **Youtube**, **Docs**, y **múltiples servicios**. De todas formas la caída no es total dado que puede que en algunas ocasiones podamos ingresar y utilizar estos servicios…


{{< img src="https://i.postimg.cc/j51kbzWb/image.png" >}}


Así mismo este error **se ve reflejado en todos los sitios o servicios que se proveen del gigante de la tecnología** por lo cual puede que el día de hoy sea uno de los más raros de la internet. Si bien estimamos que Google reaccionará rápido como siempre, no siempre vemos caídas globales, siempre suelen estar localizadas. Pero esto es una muestra mas de que este 2020 tiene ganas de aparecer en todos los libros...


{{< img src="https://i.postimg.cc/x1j7LTVB/image.png" >}}



## ¿Alguna informacion oficial?


{{< img src="https://i.postimg.cc/tCgCD6Pz/image.png" >}}


De momento Google Desconoce la causa por lo cual nos brinda su sitio oficial para conocer el status de sus servidores


{{< link url="https://www.google.com/appsstatus#hl=es&v=status" text="Revisar status">}}



## Alternativa



Es importante recalcar que no se depende exclusivamente de Google, existen otros servicios similares que hacen que Google deje de ser indispensable, a continuación dejo el enlace de disroot el servicio que desde mi punto de vista es el mejor y digno de compartir con amigos.


{{< link url="https://disroot.org/" text="Disroot" >}}


---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

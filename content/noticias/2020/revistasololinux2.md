---
title: 'Revista Solo Linux'
date: '2020-11-02 09:10:00'
description: 'Te traemos una de las mejores revistas de GNU/Linux en español en la cual tenemos el honor de ocupar un pequeño espacio'
type: 'noticias'
tags: ["sololinux", "linux", "revista", "gratis", "vps", "sorteo"]
category: ["Noticias"]
img: 'https://www.sololinux.es/wp-content/uploads/2020/11/REVISTA-SOLOLINUX-N21-OCTUBRE-2020-730x430.jpg'
authors: ["PatoJAD"]
---



No podemos arrancar este mes si no es de otra forma que con la tan esperada y afamada revista de Solo Linux que viene con una entrega que, como todas las anteriores, no te van a dejar despegarte de la revista hasta no leerla varias veces.



> El Covid-19 continúa azotando el mundo entero, aun así, nosotros seguimos esforzándonos para mantener vivo «sololinux.es« y su revista digital. No es fácil, eso seguro.




## Solo Linux (la revista)



La revista sololinux de este mes, viene cargada de manuales, noticias y presentaciones. Quiero hacer hincapié en las entrevistas realizadas, incluyendo la de Rachid, el desarrollador de la distribución LinuxFX (WindowsFX), que no deja indiferente a nadie.


{{< img src="https://www.sololinux.es/wp-content/uploads/2020/11/REVISTA-SOLOLINUX-N21-OCTUBRE-2020-730x430.jpg" >}}


En esta oportunidad la gente de Solo Linux nos permitio estar en una de sus entrevistas donde busca dar a conocer otros proyectos relacionados al mundo de GNU/Linux. Vas a poder encontrar la entrevista que se me realizo a mi hablando de la comunidad. Y adicionalmente recomiendo leer las publicaciones de esta revista que tanto tiene para darnos!


{{< link url="https://www.sololinux.es/revista/REVISTA_SOLOLINUX_N21_OCTUBRE_2020.pdf" text="Leer Revista" >}}



## La novedad del mes



> Actualmente estamos inmersos en un foro de linux, para que cualquier usuario pueda solventar sus dudas e interactuar con otros usuarios más experimentados. El desarrollo está bastante avanzado, pero se aceptan nuevos «linuxeros» que quieran colaborar con nosotros de cualquier forma. Si estás interesado o simplemente buscas nuevos retos no dudes en contactar con nosotros.



Como ellos mismo comentan estan planeando un foro nuevo para toda la comunida de linux de habla hispana, en lo personal tengo un especie de amor por los foros, y creo puede ser interesante. si vos pensas lo mismo te dejo toda la info para que te contactes con ellos!



{{< link url="https://forolinux.es/" text="Ver Foro" >}}

{{< link url="https://www.sololinux.es/contacto/" text="Ofrecer ayuda" >}}



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

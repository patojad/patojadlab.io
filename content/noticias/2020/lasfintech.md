---
title: 'Fintech ¿El terror de los Bancos?'
date: '2020-07-29 10:08:00'
description: 'En plena era digital los bancos no se adaptan y las fintech nacen en busca de un lugar que están obteniendo...'
type: 'noticias'
tags: ["fintech", "tecnologia", "bancos", "economia", "digital", "ventajas"]
category: ["Noticias"]
img: 'https://www.roadshow.com.ar/wp-content/uploads/ECOSISTEMA-DE-FINTECH.jpg'
authors: ["PatoJAD"]
---



Hace algunos años, apañados por la iniciativa de mercadopago, comenzaron a nacer muchas “fintech”. Determinado por muchos como la nueva era bancaria muchas fintech consiguieron adherir muchos clientes a  lo largo de los años, y la pandemia en la que vivimos fue el impulso que necesitaban para ser tomados como una opción antes que los bancos (lugar donde siempre se requiere contacto humano)




## Principales ventajas de las Fintech



Según algunos autores, las Fintech aportan su valor habiendo propuesto 4 soluciones principales:



* Han creado sistemas de pago online, agilizando y abaratando las operaciones de pago tradicionales.
* Mejoran la seguridad en términos de posesión y transferencia de bienes.
* Incrementan, mediante Big Data, la información sobre clientes, empresas y tendencias.


{{< img src="https://cdn2.hubspot.net/hubfs/2604929/iStock-974158260.jpg" >}}


Esto sumado al actual contexto mundial favoreció el crecimiento abismal de este modelo de negocio economico digital. Generando así un temor en los bancos, dado que estas fintech podrían mejorar al punto de desplazarlos (no hablamos de los principales bancos que dominan el mundo).




## Una negativa de las Fintech



Si bien las fintech parecen un sueño dorado de felicidad y amor tienen un sistema de reclamos extremadamente pobre (hablado en especial de Argentina - Mercadopago) y es una negativa que cuando hablamos de dinero es realmente importante. Imaginemos que nadie nos da respuesta de “supuestas transacciones”. Si bien es verdad que están encaminando soluciones pueden llegar a ser el punto más débil de las fintech.




## Bancos con miedo



Los bancos recibieron como golpe bajo el covid y demoraron muchísimo en actuar. Esto es algo realmente malo para ellos porque si bien no *pierden* usuarios están dejando que las fintechs crezcan de una forma abismal y esto puede volverse negativo en poco tiempo.

{{< img src="https://www.grou.com.mx/hs-fs/hubfs/Blog_Enero_19/Infografia_31_enero_grou.jpg?width=2084&name=Infografia_31_enero_grou.jpg">}}

Por eso mismo 3 grandes bancos, en Argentina, se unieron para crear una fintech que permita a los bancos posicionarse a nivel de competir con estas nuevas formas de llevar las finanzas un poco más amoldadas a la era digital. Si bien esta iniciativa intentara unir muchos bancos y volverlo una iniciativa internacional hoy estamos un pocos alejados de ver a los bancos amoldarse a la era digital.




## Las Fintech y el futuro



Segun el blog español eipe esta es la concepción que tienen del futuro económico...



> No podemos concebir el mundo de las finanzas actual sin tener en cuenta el sector Fintech; porque es la evolución natural, porque ya ha mostrado su potencial y porque promete acercar a los consumidores y a las empresas, el sector Fintech es uno de los más potentes del momento y de los que más crecimiento va a experimentar en los próximos años.




---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: 'ARM y NVIDIA ¿Hay Algo por atrás?'
date: '2020-09-16 09:42:00'
description: 'Me gustan mucho las teorias de conspiracion por lo cual voy a contar un poco lo que escuche estos dias'
type: 'noticias'
tags: ["nvidia", "arm", "intel", "amd", "miedo", "opinion", "logo"]
category: ["Noticias"]
img: 'https://www.profesionalreview.com/wp-content/uploads/2020/09/NVIDIA-y-ARM1-1024x576.jpg'
authors: ["PatoJAD"]
---



> Tras la adquisición de ARM Holdings por parte de NVIDIA, la cual ha trastocado por completo el mercado de las CPU pero no en el sector que mucha gente cree, el fabricante podría comenzar a fabricar sus propios procesadores. ¿Se convertirá NVIDIA en una amenaza para Intel y AMD?



Enunciados así podemos ver que están abatiendo en los blog de noticias estos días con la muy sorprendente compra que realizó NVIDIA con un tinte de expansión dentro del mercado.




## Contexto actual



Todos sabemos que, mal que pese, nvidia es quien domina el mercado de las placas de video. No es solo por su larga trayectoria, está impulsado por su auspicio en eventos y obviamente su innovación en esta área.


{{< img src="https://www.profesionalreview.com/wp-content/uploads/2020/09/NVIDIA-y-ARM1-1024x576.jpg" >}}


Pero el mundo se le pone gris a NVIDIA dado que su mano derecha está perdiendo terreno. Intel una empresa que lideró muchísimos años el mercado de los procesadores está perdiendo terreno abismalmente con AMD una empresa que venía acostumbrada al segundo lugar en procesadores y gráficos.




## Intel no sabe seguir



Intel fue atacado muchísimo en estos últimos tiempos debido a sus vulnerabilidades y a eso le sumaron muchísimos parches que reducen la performance de los equipos. Y por otro lado su digno oponente mejoraba exponencialmente su producto con la saga rayzen y mantenía el precio “popular” que todos queremos…


{{< img src="https://graffica.info/wp-content/uploads/2020/09/Intel-nuevo-logo-2.png" >}}


Recientemente Intel realizó un intento triste de “rejuvenecer” con un cambio, desde mi punto de vista paupérrimo, en su logo, poniendo una fuente que nos trae a nuestra mente el Internet Explorer… ¿Es posible que se estén dando por muertos como en IE? Realmente no lo creo pero con cada cosa que arman parece estar más lejos de su objetivo.




## NVIDIA podría tener miedo



Realmente no creo que esto sea así, sin embargo es una posibilidad, viendo como su mano derecha en este mundo tecnológico cayó en pocos años ante su mismo rival tiene que asegurarse su lugar en la cima y la mejor forma es atacar primero… ¿Es posible que al igual que AMD, NVIDIA saque sus propios procesadores?


{{< img src="https://hardzone.es/app/uploads-hardzone.es/2019/12/AMD-vs-NVIDIA.jpg" >}}


Realmente con esta compra tendrían que estar apostando a que los equipos de escritorio migrarán a esta esta arquitectura y poder, nuevamente, llevar la innovación. O por ahí es solo una teoría nuestra y solo quería mantener el mercado de los celulares… Esto lo veremos mejor en los siguientes días…




## Mi opinión



Toda esta teoría conspiranoica fortalece mi teoría de que AMD creció mucho y que dando por muerto a Intel quiere ir a comerse a NVIDIA, si bien tiene mucho trabajo por realizar cuenta con el apoyo de la comunidad y esto termina siendo mucho más fuerte que otras cosas… ¿Tendrá algo entre manos para combatir con RTX?



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: '¿Comunidades Toxicas? el caso de Mas GNU/Linux'
date: '2020-05-31 12:08:00'
description: 'Motivado por el post anterior intente forzar un caso de comunidades toxicas y me llevo una sorpresa...'
type: 'noticias'
tags: ["comunidades","toxicas","gnu","linux","mas"]
category: ["Noticias"]
img: 'https://live.mrf.io/statics/i/ps/www.ecestaticos.com/imagestatic/clipping/c45/53c/c4553c3c9a3495b1d01c3026868ffdc7/las-dudas-ortograficas-mas-frecuentes-de-los-espanoles-resueltas-por-la-rae.jpg?mtime=1579565836'
authors: ["PatoJAD"]
---



Como vimos en el caso anteior de {{< textlink url="/linux/2020/05/comunidades-toxicas/" text="Comunidades Toxicas" >}} me parecio interesante generar otra ocasión similar pero termino llevandome una grata sorpresa de la respuesta general de la comunidad




## El Video



A continuacion les dejo el video de lo ocurrido, y quiero mandar un fuerte abarazo a Pedro y a toda la comunidad de {{< textlink text="Mas GNU/Linux" url="https://masgnulinux.es/" >}} y agradecerle por toda la buena onda despues de lo sucedido desde ya sigan con su excelente trabajo.



{{< youtube code="8_4dW-NLEN0" >}}




---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: 'Ser pocas no da lo mismo'
date: '2020-11-18 09:24:00'
description: 'Por qué necesitamos más mujeres en Programación, una industria decisiva para la Economía Global'
type: 'noticias'
tags: ["ser", "pocas", "informatica", "mujeres", "online", "seminario", "genero", "programacion", "natalia", "rivera", "barrera"]
category: ["Noticias"]
img: 'https://i.postimg.cc/zBv83sTK/image.png'
authors: ["PatoJAD"]
---



> Sólo el 6% de las personas que desarrollan apps móviles son mujeres.
El resto son hombres. Es muy probable que todas las apps que tienes en tu celular hayan sido pensadas y diseñadas por hombres. Ellos dominan el mundo de la tecnología, esa industria que está cambiando el mundo.
>
> Pero los tiempos ya no son los de antes. Estamos nivelando la balanza. Ser pocas no da lo mismo.



Cuando mi mujer me leyó esto en su perfil de caralibro, me paré a pensar un poco. Esto es realmente real y absurdo. ¿Porque hay tan pocas mujeres en la tecnología, si suelen ser ellas las que dan los pasos más importantes?



> Natalia Rivera Barrera, Business Analyst en ThoughtWorks, nos contará su historia y cómo se abrió paso en el mundo de la Programación.
>
> Asiste al webinar con algo para tomar notas. Vamos a contar tips interesantes para ganar terreno en esta industria: cómo empezar, dónde estudiar y qué camino seguir.



Tras esto vi muchos mensajes diciendo que no había problema si eran pocas, pero creo que la realidad si hay problema. El problema se genera porque muchas veces no se da la posibilidad. Yo creo que cada persona debería poder elegir su trabajo sin importar el género y creo que decir eso debería ser absurdo dado que es lógico. Pero en pleno siglo XXI existe gente que no logra entender esto.




## El estudio crece



Hace dos años cuando comencé a ser colaborador de la universidad, me di cuenta que había pocas estudiantes. En general había muchos hombres, no era algo que me moleste pero si era algo que me llamaba la atención.

La directora de la carrera y muchos profesores tomaron la partido con este tema, notaron que muchas mujeres dejaban al sentirse excluidas en TP grupales y demas, y conjunto a chicas que ya estaban más avanzadas en la carrera poblaron nuevamente de mujeres la carrera




## El evento


{{< img src="https://i.postimg.cc/zBv83sTK/image.png" >}}


En particular me gusta fomentar estos eventos para poder nivelar la balanza y poder crecer, comprobé empíricamente que las mujeres son mucho mas constructivas y menos egocéntricas que los hombres, y como tal necesitamos mas gente asi para mejorar la calidad del software (Julio, imaginate si gnome fuera desarrollado por mujeres seguro no consumiria tanto ram)



* **Fecha**: mié., 18 de noviembre de 2020
* **Hora**: 20:00 – 21:00
* {{< textlink url="https://www.eventbrite.com.ar/e/ser-pocas-no-da-lo-mismo-tickets-128922220833#add-to-calendar-modal" text="Agregar al calendario" >}}



{{< link url="https://www.eventbrite.com.ar/e/ser-pocas-no-da-lo-mismo-tickets-128922220833" text="inscribirse" >}}



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: 'Los usuarios de Linux'
date: '2020-09-08 13:15:00'
description: 'Ya hablamos de las comunidades con anterioridad y ahora nos toca hablar de los usuarios que lo conforman'
type: 'noticias'
tags: ["comunidad", "linux", "usuarios", "tipos", "virus"]
category: ["Noticias"]
img: 'https://itmastersmag.com/wp-content/uploads/2019/05/shutterstock_622733918_Internet-users-e1557852460431.jpg'
authors: ["PatoJAD"]
---



Para los que me leen o me siguen a menudo saben que soy muy sincero dando mi opinión y en esta oportunidad quiero hablar sobre los diferentes usuarios que están en linux.

Con anterioridad hablamos ya de las {{< textlink text="comunidades y sus falencias" url="https://patojad.com.ar/linux/2020/05/comunidades-toxicas/" >}}, ahora nos toca hablar de los usuarios que las componen, diferencias los tipos que existen y como pueden ser más o menos dañinos




## El video



En esta oportunidad me exprese por video y espero que sea de su agrado


{{< youtube code="fjjX_aT8eXU" >}}



## Tu opinión



Como siempre digo todos somos libres de opinar y muchas veces esto nos ayuda a aprender entre todos, por lo cual te pido que dejes tu opinión sobre los usuarios...

---
title: 'El camino del Blog'
date: '2020-09-11 13:14:00'
description: 'Cuando inicie con el blog tenia un camino, pero no un norte, y a estas alturas es hora de que juntos lo definamos'
type: 'noticias'
tags: ["blog", "patojad", "camino", "destino", "comunidad"]
category: ["Noticias"]
img: 'https://biblioteca.acropolis.org/wp-content/uploads/2016/11/destino.jpg'
authors: ["PatoJAD"]
---



Hace tiempo el blog nació, probablemente no tenía razón de ser el blog, o incluso no tenía norte. Pero el blog avanzaba, paso por muchos cambios y llegó hasta donde está ahora, sin embargo siento que aun no marque el norte.




## Algo que parece claro y no lo es



Yo no entiendo porque publico estas cosas como si alguien le importara. Este blog parece estar orientado a GNU/Linux, digo el 90% de las publicaciones hablan de eso, sin embargo, esto va a ir cambiando con el tiempo… La idea es la tecnología y sobre eso quiero girar. Claramente voy a seguir hablando sobre GNU/Linux, pero no va a ser el único tema y pese a quien le pese voy a hablar de la tecnología en general…




## ¿Como va a ser el cambio?



Esto va a ser paulatino y con el tiempo iremos incluyendo más temas con un poco más de seriedad y si es posible con más participación de la comunidad o usuarios. La idea del blog es tener un lugar para hablar de todos los temas que creamos acordes y si bien no está bueno que las cosas no sean muy específicas esta bueno tener un poco de libertad y esto puede ser la receta de la muerte del blog, pero aun asi nunca estuvo en la sima por lo cual nadie pierde nada




## ¿Vamos a perder los chistes de Julio y Pipe?



Claramente no, ponerle seriedad implica en fundar más las cosas o intentar ser más explícito. Creo que es parte de la mística de este blog incluir los chistes internos del canal, es nuestra marca y el dia de mañana pueden ser más que solo julio y pipe, todos tienen su lugar y pueden formar parte del chiste cotidiano (Y quien dice el dia que escriban podré ser yo el chiste)




## ¿Cómo va participar la comunidad?



Esta pregunta no me deja conciliar el sueño, mi idea es intentar responder a las demandas de la misma o incitar a que ellos las compartan. En criollo me gustaría poder suplir más los pedidos de los usuarios o que ellos mismos escriban en el blog…




## Tu Opinión



Claramente como este blog es más de los que opinan que mio, me gustaria saber tu opinion, ¿Cual es el camino a tomar? ¿Sobre qué queres saber? ¿Qué temas son interesantes?




---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

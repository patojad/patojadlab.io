---
title: 'Revista Solo Linux'
date: '2020-08-03 09:25:00'
description: 'Te traemos una de las mejores revistas de GNU/Linux en español en la cual tenemos el honor de ocupar un pequeño espacio'
type: 'noticias'
tags: ["sololinux", "linux", "revista", "gratis", "vps", "sorteo"]
category: ["Noticias"]
img: 'https://www.sololinux.es/wp-content/uploads/2020/08/REVISTA-SOLOLINUX-N18-JULIO-2020-730x430.png'
authors: ["PatoJAD"]
---



Si ya me conoces hace un tiempito sabrás que siempre me gusta colaborar con todos los proyectos. Siempre y cuando los proyectos sean con amor a la causa yo siempre ofrezco mi apoyo. Sin embargo no suelo publicar muchos otros proyectos porque me preguntan por algunos que “faltan” y es difícil tener que decir que algunos proyectos están lejos de los ideales que la comunidad que se fue formando acompañan…

Sin embargo de alguna forma llegue a cansarme de esto, si hay algo que es bueno lo comentare y si hay algo que sea dañino simplemente guardare silencio y listo.




## Solo Linux (la revista)



Yo siempre digo que los buenos proyectos tienen que ser reconocidos y si bien esto tendría que hacerse esto mucho antes aprovecho esta oportunidad que nos hicieron un pequeño mimo…


{{< img src="https://www.sololinux.es/wp-content/uploads/2020/08/REVISTA-SOLOLINUX-N18-JULIO-2020-730x430.png" >}}


En esta oportunidad la gente de Solo Linux nos permitio estar en una de sus entrevistas donde busca dar a conocer otros proyectos relacionados al mundo de GNU/Linux. Vas a poder encontrar la entrevista que se me realizo a mi hablando de la comunidad. Y adicionalmente recomiendo leer las publicaciones de esta revista que tanto tiene para darnos!




## Leela y de paso llevate un VPS



La gente de Solo Linux aprecia a sus lectores y sabe que de estos no le faltan, por lo cual decidió darle un mimo a sus lectores con un sorteo increíble.


{{< img src="https://www.sobotzo.com/wp-content/uploads/2018/06/vps-servers-inamsay-800x400.png" >}}


Para participar en el sorteo tan solo debes {{< textlink text="dejar un comentario en este artículo" url="https://www.sololinux.es/revista-sololinux-n18-julio-2020-sorteo-de-un-vps/" >}}. El ganador se publicará en la revista del mes que viene, pero nos pondremos en contacto con el ganador un día antes. El sorteo se cerrará el día 29 de agosto (si no respondes en 12 horas se sorteará de nuevo).

El afortunado podrá elegir entre estas dos opciones de VPS. Uno durante un mes, otro durante 3 meses. Aclaro que este sorteo no está patrocinado por ninguna empresa, es particular de Revista Sololinux N18. Vemos las dos opciones a elegir por el futuro ganador.




#### Opción 1 (1 mes)



* CPU: 1 Core
* RAM: 2 Gb
* Disco: 20 Gb SSD
* Bandwidth: 100 Mbit/s
* Sistema: KVM
* Duración: 1 mes




#### Opción 2 (3 meses)



* CPU: 1 Core
* RAM: 512 Mb + 512 Mb de swap
* Disco: 10 Gb SSD
* Bandwidth: 250 Mbit/s
* Sistema: Open VZ
* Duración: 3 meses




Claramente esta oportunidad es única y sirve para impulsar el proyecto que estemos pensando! No te lo pierdas!


{{< link url="https://es.calameo.com/read/00585139616dce8759793" text="Leer Revista" >}}


---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

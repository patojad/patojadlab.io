---
title: '24H24L un nuevo evento se acerca'
date: '2020-09-29 10:02:00'
description: 'Cuando inicie con el blog tenia un camino, pero no un norte, y a estas alturas es hora de que juntos lo definamos'
type: 'noticias'
tags: ["24h24l", "evento", "linux", "usuarios", "comunidad", "software", "libre"]
category: ["Noticias"]
img: 'https://i.postimg.cc/RFvWPTLC/photo-2020-09-29-10-36-31.jpg'
authors: ["PatoJAD"]
---

Para la comunidad de linux hay pocas cosas más emocionantes que un evento, en esta oportunidad te traemos lo que se viene… Si estamos hablando de un nuevo evento, que llega para impulsar a los usuarios a usar linux y sentirse cómodos en este sistema operativo que algunos tanto amamos...




## **24H24L Evento creado por usuarios de GNU/Linux para futuros usuarios de GNU/Linux**




##### **Evento orientado hacia aquellos curiosos de GNU/Linux que no se han atrevido a probarlo e incluso para los que están empezando en el apasionante mundo del software libre.**




En el primer cuarto del siglo XXI todavía existe mucha gente que no se atreve a usar un sistema operativo y las herramientas que provee de manera directa desde la comunidad de desarrolladores.

Hemos preparado un evento online en formato podcast en la que los ponentes y moderadores explicarán conceptos básicos de este sistema operativo basado en la colaboración de voluntarios.

Consideramos que ya que GNU/Linux se encuentra en la inmensa mayoría de servidores profesionales y que dispone de distribuciones o sabores adaptadas o adaptables a cualquier entorno personal o profesional sin duda es la oportunidad de mostrar sus ventajas.


{{< img src="https://pbs.twimg.com/media/EhYNCLmX0AASM2V.jpg" >}}


En el evento existen seis categorías(Redes, Programación, Empresas, Hardware,GNU/Linux, Multimedia) en las podremos encontrar la solución que precisamos tanto como negocio o empresa hasta el usuario final.

No solo se trata de programación de aplicaciones en docenas de lenguajes, el despliegue o la creación de scripts se limita este sistema operativo.

Por donde muchos empiezan es por la puesta en marcha de hardware anticuado que puede renacer gracias a la adaptabilidad de este entorno. No olvidemos que la domótica o la IoT (el Internet de las cosas) usa estos lenguajes para ser completamente funcionales.


{{< audio url="https://24h24l.org/audios/promo01-24h24l.ogg" >}}


Grabar podcast, producción musical, edición de video o realizar tareas de diseño gráfico, todo esto y mas se puede realizar en GNU/Linux.

Como servicio esencial debemos tener en cuenta que los grandes proveedores de alojamiento web o de aplicaciones usan GNU/Linux para servir a sus usuarios, siendo éste el más eficiente para estas tareas.

Las más grandes super-computadoras usan distribuciones de Linux para poder ofrecer esa potencia de cálculo sin importar con qué estación de trabajo va a ser controlada.

Podemos usar diferentes distribuciones o “sabores” en los que cada máquina aunque parta de una base similar puede personalizarse completamente al gusto del usuario, no solo en cuanto a colores o iconos, si no que no partimos de una base estricta de aplicaciones, podemos disponer de lo que necesitemos en cualquier momento y cambiarlo cuando sea necesario con apenas unos pocos clicks.


{{< img src="https://24h24l.org/images/banner.png" >}}


Cabe destacar el conocimiento y la experiencia en entorno GNU/Linux tanto de los ponentes como de los moderadores, muchos de ellos con una dilatada experiencia en el podasting, por lo que encontraremos debates interesantes en cada una de las secciones.

Este evento estará compuesto por la emisión , el próximo **12 de Diciembre** , de los 24 audios grabados en un formato de podcast. Para encontrar mucha más información en nuestra web https://24h24l.org/ donde ya se explica todos los contenidos de los podcast y otra información relacionada.



Pueden encontrarnos en redes sociales:



- Mail: {{< textlink text="24h24L@mailo.com" url="mailto:24h24L@mailo.com?subject=Nota_de_prensa" >}}
- Twitter: {{< textlink text="@24H24L1" url="https://twitter.com/24H24L1" >}}
- Canal de Telegram: {{< textlink text="Evento 24H24L" url="https://t.me/evento24h24l" >}}
- Mastodon: {{< textlink text="@24h24l@mastodon.online" url="https://mastodon.online/@24h24l" >}}
- Youtube: {{< textlink text="24H24L" url="https://www.youtube.com/channel/UCxUKfsev_8aJEQHFKVMk0Kw" >}}



José Jiménez (Organización)

\#24H24L  



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

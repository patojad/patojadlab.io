---
title: 'La importancia del Staff'
date: '2020-07-21 08:58:00'
description: 'La importancia de un staff en una comunidad es muy elevada y mas cuando de toxicidad hablamos...'
type: 'noticias'
tags: ["toxicidad", "admins", "comunidad", "staff", "pedro", "crespo"]
category: ["Noticias"]
img: 'https://www.3gb.com.mx/wp-content/uploads/2015/10/Toxic.jpg'
authors: ["PatoJAD"]
---



ya anteriormente hablamos sobre comunidades, toxicidad, y muchas cosas junto a videos que desde mi punto de vista son divertidos de ver… Sin embargo creemos que todo esto tiene un origen y una forma de llevarse a cabo




## ¿Cuán importante es el Staff?



A decir verdad el staff de una comunidad es realmente importante, porque siempre, aunque sea inconsciente, los miembros toman una admiración y/o respeto por estos que los lleva a adoptar su comportamiento como correcto.


{{< img src="https://yorcyrivera.files.wordpress.com/2018/11/concurso_cas05_2018.jpg">}}


Esta pequeña construcción social hace que si nuestro staff está compuesto por gente tóxica el grupo tiende a ser tóxico en sí mismo… Y no hablo de todo el grupo, sino de los más influenciables pero esto termina marcando a un grupo en sí (pues siempre lo malo resalta más)




## ¿Qué podemos hacer?



Esta pregunta es difícil que se la haga el staff, dado que la gente tóxica no lo admite, (pocos somos los que admitimos que somos tóxicos con x temas) por lo cual los consejos que voy a ir marcando a continuación se pueden aplicar a cualquier tipo de grupo.



* **Staff Amplio y con carácter**: El staff está siempre para colaborar por el bien del grupo, por lo cual un staff amplio que no sea sumiso ante el creador del grupo es importante para **corregir errores del mismo staff** (es fácil advertir a un usuario, pero no tan fácil advertir a un admin o un creador)

* **Ideas diferentes, mismo objetivo**: El hecho de que los miembros del staff tengan un objetivo claro y piensen diferente contribuye a evitar la exclusividad de los grupos. Por ejemplo: Todo el que me conoce sabe que yo **amo KDE** y *repudio GNOME* sin embargo en mi staff hay gente que **ama GNOME** por lo cual evitamos que *GNOME* sea  mala palabra en el grupo. (Este ejemplo es absurdo, pero es perfectamente escalable)

* **Sin piramides, todo una llanura**: Si hay algo de lo que estoy orgulloso es que en mi grupo el usuario nuevo y el owner (o sea yo) tenemos el mismo peso de la palabra y el mismo derecho en todo. Cualquier miembro tiene el grupo y sus herramientas a disposición para lo que necesite y eso hace que la voz de todos los miembros valga. Ya varias veces fueron usuarios los que acomodaron cosas sobre el canal de YTbe o el mismo blog.




## La toxicidad trasladada


{{< img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSb1oi8-SwW_PINKcpD1_1XNo6k7uPSvBKxkQ&usqp=CAU" >}}


Este es el tema más difícil que nos toca, cuando la toxicidad viene por fuera del grupo, en ese caso debemos poner paños de agua fría o invitarlos al club de la pelea, un grupo pensado para el forobardo y las peleas a fin de calmar y no llenar otros grupos con cosas que no vienen al caso.


{{< link url="https://t.me/fightclub202X" text="El club de la lucha" >}}


Y ya que estás uniéndote por ahí no te olvides de entrar a la comunidad


{{< link url="https://t.me/PatoJADCommunity" text="Comunidad" >}}



## A tomarlo con humor



Como siempre digo el humor nos mantiene vivos por lo cual en esta oportunidad les dejo un poco de mi toxicidad y mi “cariñoso” hater. Para que nos riamos, vos, yo y por supuesto el que nunca viene a mal reirse un poco…


{{< youtube code="xeNhOjqSRzE" >}}


Espero que te haya gustado, espero que te sirvan los consejos, no te olvides de dejar tu consejo en los comentarios para tomar mejores medidas en los grupos o tu opinión…



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

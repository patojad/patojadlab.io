---
title: 'Todas las novedades en un solo lugar'
date: '2020-06-12 13:34:00'
description: 'Feed un lugar donde están todos tus sitios favoritos, y si no están… Agregalo mandonos su RSS...'
type: 'noticias'
tags: ["rss", "feed", "linux", "novedades", "news", "planeta", "libre"]
category: ["Noticias"]
img: 'https://i.postimg.cc/YSnhfWqd/image.png'
authors: ["PatoJAD"]
---



Estos días recurro mucho a {{< textlink text="Planeta Libre" url="https://victorhck.gitlab.io/planetalibre/">}} un proyecto que junta múltiples RSS para *mantenernos informados* y note que se actualizaba de una forma un tanto manual, si bien lo sigo usando decidí crear una especie de **V2** de este proyecto que se actualiza cada vez que alguien ingrese al sitio web y este es el resultado…




## Las webs


{{< img src="https://i.postimg.cc/g0NYbFCj/image.png" >}}

En particular puse *algunos sitios web* que suelo utilizar a menudo, sin embargo la idea sería que cada administrador o seguidor de un sitio nos envíe su **RSS** para poderlos agregarlos y así mejorar la cantidad de sitios que mostramos… Siempre teniendo en cuenta que los sitios **tienen que estar orientados a la tecnología** (*a diferencia de Planeta Libre creo que debería ser un poco mas generico*)




## Las vistas


{{< img src="https://i.postimg.cc/C1rLkT7X/image.png" >}}


Actualmente mostramos **las últimas 5 publicaciones** de cada blog, en formato de tarjetas, sin embargo esperamos mejorar un poco las vistas con el tiempo, incluso queremos tener un mix de todos los contenidos ordenados por fechas pero en este primer release buscamos presentar el proyecto y unir a más sitios webs!




## Feed


{{< img src="https://i.postimg.cc/YSnhfWqd/image.png" >}}


Así es el nombre que tiene este pequeño proyecto que pretende darnos un solo lugar donde tener todos los datos accesibles. Como siempre todos nuestros sitios son libres y se encuentran disponibles en **gitlab** para quien quiera replicarlo, mejorarlo, o realizar cualquier modificación.


{{< link url="https://feed.patojad.com.ar/" text="Ir a Feed" >}}


---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

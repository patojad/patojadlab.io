---
title: 'Legión Linux Gamers'
date: '2021-06-30 09:10:00'
description: 'Hablamos de LLG, una Legión que llego para quedarse...'
type: 'noticias'
tags: ["comunidad", "juegos", "llg", "legion", "gamers"]
category: ["Noticias"]
img: 'https://i.postimg.cc/MH5cN5vx/llg.png'
authors: ["PatoJAD"]
---

Hoy vamos a hablar de un gran proyecto que está naciendo como la unión de muchas comunidades que buscan cambiar la forma de ver el mundo gaming de linux. Y hablo de proyecto y no de comunidad porque esto excede a las comunidades en sí, esto trata de unión entre todos…

## Video

Les dejo un video en el que vamos a estar hablando sobre este tema, lo que la comunidad significa para nosotros y obviamente como puedes unirte a nuestra comunidad… Y como podes unir a tu comunidad… Así mismo también tenes toda la información de cómo se puede ayudar con esto.

{{< youtube code="xnp4jLxUAKA" >}}

{{< link url="https://llg.patojad.com.ar/#/" text="Ver Sitio de LLG" >}}

Si queres un poco más de información, o este video no satisface tus necesidades porque es de un "youtuber" de poca monta, te dejo aca un video de verdad de un YOUTUBER con todas la letras que te invita a unirte a esta Legión

{{< youtube code="Jxi2SwwO5sk" >}}

## Te facilito las Redes

Tenes ganas de unirte a esta comunidad o queres hablar con el staff para proyectar algo te dejo todas las redes por las cuales vas a poder hablar y comunicarte con todos los usuarios y staff de este hermoso proyecto que va creciendo dia a dia.

* {{< textlink text="Discord" url="https://discord.com/invite/n8XtgN3">}}
* {{< textlink text="Steam" url="https://steamcommunity.com/groups/LegionLinuxGamers" >}}

---
title: 'Argentina y su soberanía Digital'
date: '2021-07-12 09:10:00'
description: 'Juana Manso y las nuevas notebooks para los estudiantes de escuelas publicas'
type: 'noticias'
tags: ["huayra", "windows", "conectar", "igualdad", "notebooks", "juana", "manso", "educacion", "argentina", "ubuntu"]
category: ["Noticias"]
img: 'https://i.postimg.cc/3NvwHKtv/image.png'
authors: ["PatoJAD"]
---

{{< img src="https://i.postimg.cc/6qky778c/image.png" >}}

No pretendo politizar este blog ni esta publicación, creo que todo en la política tiene cosas buenas y malas, sin embargo quiero hablar sobre algo que inició en argentina hace ya muchos años (gracias a esto llegue a linux) y después de ser fuertemente masacrado el proyecto renace y con algunos detalles que me dan una sana envidia con un gran orgullo.

## Del conectar igualdad a Juana Manso

> “Los equipos de Juana Manso son la evolución tecnológica de lo que fue Conectar Igualdad. Es un modelo creado de forma conjunta con proveedores pioneros y expertos en el desarrollo de computadoras educativas”

No conozco a detalle cada una de las especificaciones de estos planes, pero a fines prácticos simplemente se cambió el nombre. La idea básicamente es entregar a todos los jóvenes estudiantes una notebook con la cual puedan realizar sus tareas, y aprender jugando. A decir verdad en mi experiencia estas notebooks son realmente útiles aunque no sean muy potentes cumplen su función y lo hacen muy bien.

## Tierra del Fuego a todo vapor

Ya se comenzo la produccion de maquinas en Tierra del fuego para ser entregadas en todas las escuelas públicas del país, a diferencia de los años anteriores en que esto fue aplicado, ahora las notebooks contarán con un diseño más cuidado, un ssd M2 de 240GB, 4 GB de ram, y un Celeron N4020 con una placa integrada UHD 600. Un equipo bastante interesante para las tareas que realizará.

{{< img src="https://i.postimg.cc/wxGJ8Mj7/image.png" >}}

## Tecla super

Este pequeño detalle me llena de envidia, las teclas super suelen venir con el logo de Windows, algo que a los linuxeros no nos suma ni nos resta (Como al resto del mundo porque solo es un logo) Pero los afortunados jóvenes que reciban esta PC van a contar con el “hermoso” logo de Ubuntu en su lugar

{{< img src="https://i.postimg.cc/Dzrd7TYR/image.png" >}}

(Sabemos que no soy fan de ubuntu pero lo prefiero...)

## ¿Ubuntu? ¿Y dónde está Huayra?

Las pruebas preliminares de la PC se realizaron con Windows 10 Sin embargo se había anunciado que la máquina vendría con Huayra 5 (Amaría ver a la vaca en la tecla Super) Sin embargo ahora se confirmó que vendrán con Ubuntu. Esto es un gran golpe a la soberanía digital, pero tenemos que entender que a Huayra 5 le falta camino… Después de años parados es difícil retomar el proyecto, y actualmente Huayra 5 tiene muchas falencias que dificultan su uso para novatos. Sin mencionar que no dispone del hermoso acabado final que tenía…

{{< img src="https://www.sololinux.es/wp-content/uploads/2021/04/Huayra-5.0-Listo-para-su-descarga-730x430.jpg" >}}

Espero que Huayra siga mejorando para volverse la Distro por defecto en la siguiente camada y, porque no, cambiar el logo de Ubuntu por la amistosa vaca…

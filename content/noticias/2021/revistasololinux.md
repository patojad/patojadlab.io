---
title: 'Revista Solo Linux 31'
date: '2021-09-02 08:10:00'
description: 'Te traemos una de las mejores revistas de GNU/Linux en español y una gran yapa incluida que trae a colación las críticas innecesarias realizadas a la revista.'
type: 'noticias'
tags: ["sololinux", "linux", "revista", "critica", "respuesta", "agosto", "2021", "magazine", "digital"]
category: ["Noticias"]
img: 'https://i.postimg.cc/WtLfBLTN/Revista-Digital-Magazine-N31.webp'
authors: ["PatoJAD"]
---

> Como es habitual desde hace dos años y medio, cada día uno del mes entrante lanzamos la revista **Magazine SoloLinux**. Nuestro **magazine digital** nació con una visión muy clara y concisa, ayudar a los que no tienen la posibilidad de disponer de una conexión a internet decente, así… pueden descargar todos los artículos del mes con una operación centralizada y rápida.

Así se presenta, mes a mes, la gran revista de linux que se mantiene como una de las mejores revistas del ámbito. Más allá de las críticas de algunas personas se mantienen firme y mes a mes nos traen nuevas versiones llenas de artículos interesantes.

> (...)dado el revuelo causado en nuestro chat de Telegram cuando un usuario crítico abiertamente y de manera efusiva, que se enumere la revista con mes pasado. NO vamos a cambiar por ti amigo, primero son nuestros lectores, solo me faltaba eso ya.

De esta forma, responden políticamente a las quejas que, desde mi perspectiva, no tienen mucho sentido (En el peor de los casos que importa el mes que pongan si lo importante es el contenido) Pero es válido, más allá de traer la increíble revista traer las novedades que la acompañan.

{{< img src="https://i.postimg.cc/WtLfBLTN/Revista-Digital-Magazine-N31.webp" >}}

Nosotros compartimos la visión de **SoloLinux** de que es una buena manera descargar la revista para evitar problemas con internet, más que nada orientado a esas personas que no disponen de buenos recursos online o que pueden mostrarse inestables.

> Como es habitual, desde **sololinux** te recomendamos descargar la revista sololinux en pdf(...), así la puedes leer en cualquier momento sin tener que estar conectado a Internet. También te brindamos la opción de poder leerla online.

{{< link url="https://revistalinux.es/revista/REVISTA_SOLOLINUX_N31_AGOSTO_2021.pdf" text="Descargar Revista" >}}

Pero también entendemos que muchos preferimos verlo online sin descargar nada y sin acumular archivos en nuestro disco, o simplemente leerlo desde algún dispositivo de un amigo:

> Si prefieres ver el **Magazine SoloLinux** de forma online, en modo revista interactiva 3D (a través de Calameo), puedes verlo desde aqui.

{{< link url="https://www.calameo.com/books/00585139681cbdc74769f" text="Ver Online" >}}

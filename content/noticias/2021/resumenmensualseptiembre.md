---
title: 'Septiembre en pocos minutos'
date: '2021-10-01 09:15:00'
description: 'Seguimos con el nuevo formato de “resumen mensual” donde vamos a ver un pequeño resumen de lo que pasó en el mes de Septiembre '
type: 'noticias'
tags: ["ubuntu", "windows", "debian", "fuchsia", "apps", "septiembre", "resumen", "2021", "deepin", "manjaro"]
category: ["Noticias"]
img: 'https://i.postimg.cc/XJSjcjxC/tecnoticias.png'
authors: ["PatoJAD"]
---

En esta segunda edición recorremos el mes de **Septiembre** con las novedades que trajo en el mundo linux-tecno que no podes perderte para arrancar el mes completamente informado

{{< youtube code="u9mG4CIT-7Y" >}}

* Seguimos de cumpleaños con los nuevos 30 años del *kernel* de *linux* desde el día que se subió a un servidor.
* **OpenSuse** cumple 29 años, el camaleón está terminando la facultad y yo sigo en tratativas…
* Ya puedes ver el informe del año fiscal de **Apache**
* **Apple** pierde el caso contra **Epic**, s*i bien el fallo es a favor de apple ahora sí se podrán hacer compras por fuera del appstore*.

## Aplicaciones

* **GitHub** le hace un homenaje a Linux con una selección de juegos OpenSource, aunque en realidad no lo entendi.
* Por decimonovena vez anuncian que **Xorg** está *muriendo* aunque sigue actualizándose.
* **Microsoft** liberó el código de GCToolKit
* **Firefox** parece que vendrá como flatpack en ubuntu 21.10 y no como deb.
* **PeaZip** lanza la versión 8.2 con mejoras para la terminal
* **LibreOffice** publica la primera corrección menor de la 7.2 corrigiendo 87 bugs.
* **Peertube** lanza la versión 3.4 con más mejoras que los fanáticos admiran…
* **OBS** llega al versión 27.1 con muchas correcciones de errores, soporte mejorado para Wayland y ahora con vista múltiple de hasta 18 escenas.
* **OpenShoot** vuelve a aparecer con la versión 2.6.1 con nuevas funcionalidades y grandes mejoras en el rendimiento.

## Juegos

* En Steam Linux *vuelve a copar el 1%* de los jugadores… solo nos falta un 40% para ser tomados en cuenta.
* Si te gustan los juegos retro no te olvides que este mes se volvieron locos con **Antstream** que es una especie de Steam pero retro con más de 1000 juegos gratis.
* **Blizzard** sacó unos cuadros del wow porque supuestamente sexualizaban el juego.
* **RetroArch** consigue un lanzamiento oficial en Steam
* **Cathedral 3-D** llega a linux. Otro shooter que no quiero jugar con gráficos en retro y algo falopa.
* **Easy Anti-Cheat** se vuelve nativo para Linux y agrega soporte nativo para Wine y Proton.
* Esto no es nuevo pero lo descubrí este mes, ¿Sabias que existe **oneko** un tamagotchi para linux?

## Escritorios

* **KDE** muestra su BETA 5.23 más amigable con Wayland y con mejoras varias para todo el entorno, es común que de KDE no pueda decir nada porque leer los changelog de esto me llevaría todo el día…
* *** migró de GTK a EFL el proyecto de **Enlightenment**, *cuando leí esto me di cuenta que tengo que cambiar el escritorio que estoy usando*.
* **Gnome 41** Llega con cambios en la tienda y opciones de energía como novedad

## Hardware

* Amazon lanza **FireTV** metiéndose en el mercado de los Smartizadores de TV. Y puede que su OS venga con algunas marcas de TV.
* Los drivers de **Nvidia** ya funcionan con Sway el compositor de Wayland
* El controlador **Zink** para OpenGL agrega mejoras para los últimos juegos.


## Distribuciones

* **Manjaro** *cambió Firefox por Vivaldi* y muchos aprovecharon para ponerse apocalípticos con este tema…
* Se anunció **LFS 11** el manual para crear desde 0 tu distro de linux.
* **Nitrux 1.6** viene con un nuevo gestor de software para AppImage
* Salió **Linux Lite 5.6** con retoques varios y con un modelo de “paga lo que quieras” pero si viste el video de principio de mes te darás cuenta que es porque muchos no colaboran nada…
* **Chrome OS** sin esperar nada salió con v93 con mejoras en rendimientos, pantallas y más…
* **Manjaro** migró ahora a *pipeWare*

---
title: '¿Una computadora cuántica en casa?'
date: '2021-02-24 10:33:00'
description: 'Ayer por la noche en el grupo de Locos por Linux debatimos acerca de si era útil o no tener un ordenador cuántico de sobremesa en casa'
type: 'noticias'
tags: ["computadora", "cuantica", "fisica", "convencional", "sobremesa", "euros", "ordenador"]
category: ["Noticias"]
img: 'https://i.postimg.cc/rpsZgXCK/image.png'
authors: ["PatoJAD"]
---


Ayer por la noche en el grupo de **Locos por Linux** (Canal de youtube que recomiendo fuertemente al que este suscritos y pienso dejarles el link por aca para que no se olviden {fin del chivo}) debatimos acerca de si era útil o no tener un ordenador cuántico de sobremesa en casa (Aunque valiendo 50.000 euros claramente no puedo tener uno). Y eso me inspiró a explicar bien qué es esto y porque no tendría uno en casa (Y no sería solo por los 50.000)

{{< link text="Canal de Locos Por Linux" url="https://www.youtube.com/channel/UCl8XUDjAOLc7GNKcDp9Nepg" >}}

## ¿Qué es un ordenador cuántico y cómo funciona?

Explicar física cuántica se excede de este blog por un solo motivo, no soy experto en la materia. Por lo cual para entender el orden de magnitud del cambio de un ordenador normal a un ordenador cuántico solo nos queda entender que: mientras un ordenador normal utiliza los bits (que llevados a los discos duros convencionales son pequeños imanes bipolares, como los de la heladera de casa, que dependiendo el polo que expongan significan un 1 o un 0) las cuánticas utilizan los qubits (donde los elementos pueden estar en dos estados a la vez y esto ocurre gracias a que deja de regirse por la física convencional y entra en el mundo de la física cuántica. Estos estados podemos encontrarlos en los átomos, que son millones de orden de magnitud más pequeños que un imán.)

{{< img src="https://www.elespectador.com/resizer/3JXavo4jsW9K1doiHoGlgYXfuMw=/657x0/cloudfront-us-east-1.images.arcpublishing.com/elespectador/QW5BSPLLXRHRREMU3MTBM442JA.jpg" >}}

Si bien esto parece genial, no es todo color de rosas. De momento las máquinas desarrolladas por Google e IBM no utilizan Átomos naturales sino que utilizan átomos artificiales que son mucho más grandes, pero tienen propiedades similares.

## Software

El software claramente no es el mismo y teniendo tu ordenador de sobremesa no podrás darle el uso convencional, aún estos equipos sólo están hechos para investigación y no para un uso cotidiano, por lo cual si no eres científico no te será muy útil este tipo de ordenadores.

## Caros

Todavía falta mucho para estos equipos, algunos pueden pensar en minar criptos, otros en hackear el wifi del vecino. Sin embargo, lo que permitiran hacer en algunos campos sigue siendo inimaginables. Y un usuario normal que utiliza el FB para hablar con la tía Elisa no se imagina las dimensiones del equipo ni lo que podría hacer, claro está que tampoco podría hablar con la tía Elisa desde esta máquina.

{{< img src="https://larepublica.pe/resizer/FhVxsJ0_2A5EihREwcV3F89KpIk=/1200x660/top/cloudfront-us-east-1.images.arcpublishing.com/gruporepublica/STOVKQQKZNHYJDP27N2BC5XWAA.jpg" >}}

El precio del primer ordenador de sobremesa cuántico está rondando los 50.000 euros. Si realmente tienes el dinero (Y no quieres donarlo al blog) y eres curioso y quieres aprender programación cuántica es una buena oportunidad poder tener uno en casa. Sin embargo, realmente el precio no es accesible para cualquiera.

---
title: 'Revista Solo Linux 25'
date: '2021-03-01 09:10:00'
description: 'Te traemos una de las mejores revistas de GNU/Linux en español y una gran yapa incluida'
type: 'noticias'
tags: ["sololinux", "linux", "revista", "sorpresa", "vps", "sorteo"]
category: ["Noticias"]
img: 'https://www.sololinux.es/wp-content/uploads/2021/03/REVISTA-SOLOLINUX-N25-FEBRERO-2021-730x430.jpg'
authors: ["PatoJAD"]
---

Como todos los meses (Cuando lo recuerdo) Da placer recordar que es el lanzamiento de una de las mejores revistas del ámbito, si no es la única. Siempre viene plagada de novedades que nos permitirán hablar de esta revista por mucho tiempo, pero la idea es ir a leerla por uno mismo y valorar y agradecer el trabajo de sololinux.

{{< img src="https://www.sololinux.es/wp-content/uploads/2021/03/Revista-Digital-Magazine-N25.jpg" >}}

## Algo más, se viene…

Las comunidades siempre estamos intentado ofrecer más en la medida de lo que podemos, siempre intentamos crecer y favorecer a todos nuestros usuarios y a toda la comunidad. En esta oportunidad además de su presentación deja un párrafo que nos da a pensar que están trabajando en algo más…

> Cada vez somos más conocidos y, son miles de usuarios los que nos siguen a diario. Después de darle vueltas y vueltas… considero que llego el momento de diversificar. Aunque «sololinux.es» sea el núcleo, debemos crear satélites que giren a su alrededor. Este mes entrante veremos un aperitivo de los nuevos cambios, que facilitaran el acceso a ciertos contenidos de forma directa. De momento es secreto, jejeje.

Lo cual nos deja a la expectativa a todos los usuarios y obviamente a todas las comunidades que siempre queremos ver a nuestros compañeros.

{{< link text="Ver Revista Online" url="https://www.sololinux.es/revista-sololinux-n25-febrero-2021/" >}}

---
title: 'Los Blogs ¿Un esfuerzo en vano?'
date: '2021-02-23 09:53:00'
description: 'La pasada charla en el 24H24L con profesionales del ámbito de los blogs me dejó con una duda'
type: 'noticias'
tags: ["noticia", "blogs", "24h24l", "esfuerzo", "youtube", "bloggers", "vloggers"]
category: ["Noticias"]
img: 'https://neilpatel.com/wp-content/uploads/2017/08/blog.jpg'
authors: ["PatoJAD"]
---

La pasada {{< textlink url="/noticias/2021/02/estaremos-junto-al-24h24l-hoy/" text="charla en el 24H24L" >}} con profesionales del ámbito de los blogs me dejó con una duda, que no solo fue introspectiva sino también me llevó a hablar con más bloggers para entender si esto era solo una sensación o nos estaba pasando a todos en realidad.

## ¿Por qué  si lo puedo ver en Youtube?

Esta pregunta es la que destruye la integridad de muchos blogs. Realmente muchos internautas están cambiando las costumbres. No es solo el hecho de que no buscan información sino que solo esperan ver videos que resuelvan problemas. Ya la lectura se vuelve un último recurso.

{{< img src="https://bloggingx.com/wp-content/uploads/2020/03/blogging-vs-youtube-new.png" >}}

Con este cambio de costumbre y/o de búsqueda los que escribimos nos quedamos cada vez más atrás con la creciente ola de vloggers en los diferentes ámbitos.

## El tiempo es dinero… ¿Y el dinero?

Esto impacta a todos los creadores de contenido más allá de las plataformas en las que trabajamos. Eventualmente todos iniciamos esto como un hobby pero cada vez la misma comunidad nos exige de alguna forma u otra que mejoremos el contenido, la calidad, etc. Y esto se convierte en tiempo adicional que tenemos que dedicar, y si bien realizamos esto por amor a lo que hacemos se termina cruzando con nuestras responsabilidades (trabajo, familia, etc)

{{< img src="https://teraweb.net/wp-content/uploads/Los-5-M%C3%A9todos-m%C3%A1s-efectivos-para-monetizar-tu-blog-800x445.jpg" >}}

Así mismo la comunidad perdió en gran parte la costumbre de apoyar, y no, no estamos hablando de dinero, porque todos sabemos que no siempre uno puede donar pero eso no implica que no se pueda apoyar. Apoyar implica cosas más simples como compartir el contenido, dejar un comentario, desactivar los bloqueadores de anuncios. Nosotros, generalmente, procuramos que al poner anuncios los mismos no impidan leer el contenido solo intentamos cubrir las pérdidas de generar contenido gratuito.

## ¿Cómo sigue esto?

Bueno, a lo largo de estos años vi muchos blogs que murieron, otros que redujeron sus publicaciones. En lo personal esperaría que esto deje de suceder pero es real que cada vez se pone más difícil mantener el ritmo. Muchos blogs cayeron en subir contenido repetido o publicaciones con vasto contenido solo para mantener actividad.

{{< img src="https://blogz.org/wp-content/uploads/2019/06/cropped-Blogs-de-M%C3%A9xico-blogz.org-Ser-Blogger.jpg" >}}

Sin embargo, existen muchos blogs que se mantienen fuertes más allá de los vientos desfavorables y trataremos de seguir ese ejemplo siempre construyendo dinámicas para nuestra comunidad.

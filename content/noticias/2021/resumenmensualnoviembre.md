---
title: 'Noviembre en pocos minutos'
date: '2021-11-30 11:15:00'
description: 'Seguimos con el nuevo formato de “resumen mensual” donde vamos a ver un pequeño resumen de lo que pasó en el mes de Noviembre'
type: 'noticias'
tags: ["endless", "manjaro", "debian 11", "alemania", "apps", "noviembre", "resumen", "2021", "deepin"]
category: ["Noticias"]
img: 'https://i.postimg.cc/gkDC0Z9K/noviembre.webp'
authors: ["PatoJAD"]
---

Estamos llegando a fin de año y este nuevo formato que implemente no fue muy aceptado pero lo sigo haciendo porque me gusta. En fin les dejo las novedades que no pueden perderse para arrancar el mes de las fiestas y de la alegría…

{{< youtube code="XgkVzON2Ac8" >}}

## Aplicaciones

* **Steam** soporta *pipeware*.
* **APT** recibe update y no permite desinstalar aplicaciones esenciales.
* **Godot** sigue creciendo y lanza la versión 3.4 con novedades que vas a tener que buscar por internet
* **LibreOffice** lanza 7.2.3 con soporte para los *M1 de Apple* y la corrección de más de 100 fallos.
* **TuxPaint** libera la versión 0.9.27 y es muy melancólico para los que pintábamos en paint de niños.


## Escritorios

* **LXQt** lanzó la primera versión “redonda” la 1.0 y suponemos que esto habla de una gran evolución.
* **Cinnamon** 5.2 llegó para el asomo de la siguiente versión de Mint.
* **System76** la empresa de PopOS! parece que está *desarrollando de 0 un nuevo desktop en Rust*.


## Distribuciones

* Aparece **fedora 35** con GNOME 41 y mejoras en pipeware.
* **EndlessOS 4** arriba con su nueva base de *Debian 11*
* **Amazon linux** migra a Fedora y entra en nuevo ciclo de actualizaciones (*Fedora* está siendo muy presente)
* **Deepin 20.3** viene con mejoras en la gestión de imágenes y la captura de pantalla.
* Tarde pero seguro, **Manjaro** ya tiene *Gnome 41* y *KDE 5.23*, Ahora no entiendo yo a Manjaro es como ser mas actualizado que ubuntu pero menos que cualquier otra distro.
* Se dio a conocer **Gabee**, una distro que usa como base **Void**.


## Hardware

* Ya tenemos soporte para **DLSS** de **Nvidia** con *Proton 6.3-8*.
* **MESA 21.3** Liberado con buenas novedades para las gráficas.


## Otras

* **Alemania** inicia la migración al *Software Libre / Código Abierto*.
* **Microsoft** metió una supercomputadora con *GNU/Linux* y todos creen que ahora va a colaborar.
* Nace **KDE Eco** que busca que su código sea más ecológico, la pregunta que nos hacemos todos es: *¿Hay algo que KDE haga mal?*

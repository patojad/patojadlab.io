---
title: 'Minds la red social blockchain'
date: '2021-12-17 12:01:00'
description: 'Tuvimos la oportunidad de probar una MacBook Pro 16 y te contamos qué tan especial es el supuesto mejor sistema operativo del mundo.'
type: 'noticias'
tags: ["blockchain", "red social", "instagram", "red", "social", "criptomonedas", "facebook", "twitter", "mastodon", "odysee", "lbry"]
category: ["Noticias"]
img: 'https://i.postimg.cc/mZKMN84b/image.png'
authors: ["PatoJAD"]
---
Desde los juegos hasta las plataformas de videos, ya están implementando las criptomonedas en redes blockchain. Y esto es una forma de obtener un ingreso, aunque sea mínimo, por el paveo cotidiano del día a día.

Hace ya mucho tiempo hablé de {{< textlink url="/aplicaciones/2020/06/lbry-en-busca-de-la-libertad/" text="LBRY" >}}, una alternativa a Youtube que nos permite ganar algunos centavos con mirar videos o tener varias visitas. Esta moneda después podíamos o retirarla o usarla para apoyar a los creadores de contenido.

{{< info text="¿Sabías que…? Todo el contenido que encuentras en mi canal de youtube está también en LBRY para todos aquellos que deseen desgooglearse. Esto permite que siga compartiendo contenido sin necesidad de que la gente acceda a servicios de Google" >}}

## Minds

En esta oportunidad llegan las redes sociales a la blockchain y esta nos pagará por nuestras interacciones y la generación de publicaciones. Es decir, que nos va a ir dando algunos céntimos, por publicar las mismas pavadas que publicamos en Facebook y Twitter, siempre y cuando tengamos interacción. Lo mejor de todo es que es totalmente OpenSource

{{< img src="https://www.minds.com/static/en/assets/logos/logo-dark-mode.svg" >}}

Claramente las ganancias no serán significativas a no ser que seamos grandes influencers, sin embargo al generar una red con usuarios que interactuemos o que leamos podemos ir ganando dinero y, obviamente mantenernos comunicados. Actualmente no encontré mucha comunidad hispana pero realmente la propuesta es interesante.

{{< img src="https://i.postimg.cc/kGL5Q1RL/image.png" >}}

Algo que me llamó la atención fuertemente es que podemos generar contenido, como videos o imagenes que sean de pago, por lo cual los usuarios que deseen verlo Tendrán que pagar por eso (Ojo que es mejor propuesta que only fans) Y esto permite a los creadores de contenido asegurarse del rédito de algunas cosas, en mi caso no me imagino el uso, pero si es una propuesta interesante para aquellos que den cursos o cosas de esta índole.

## Iniciar en Mind

Les voy a dejar el link abajo donde vamos a entrar, más allá de que podemos poner la interfaz en español, el setup y algunas pantallas aún no están traducidas, por lo cual puede que tengamos que traducir a mano algunas preguntas que nos realice.

{{< img src="https://i.postimg.cc/mZKMN84b/image.png" >}}

Lo que vamos a tener que hacer es registrarnos poner una fotito de perfil y chequear nuestro número, aparte de esto vamos a responder unas preguntas sobre qué queremos que nos muestre para que su algoritmos nos ofrezca un poco de contenido, después de eso vamos a tener que relacionarlo con una billetera virtual de etherium y {{< textlink url="https://www.minds.com/patojad/" text="seguir a PatoJAD">}} (Paso mucho muy importante) Y ya vamos a ver arriba que podemos estar generando ganancia con todas las interacciones (No se esperen ganar 10000 dólares al día claramente)

{{< link url="https://www.minds.com/register?referrer=patojad" text="Registrarse en Minds" >}}

##

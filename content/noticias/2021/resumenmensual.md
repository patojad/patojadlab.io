---
title: 'Agosto en pocos minutos'
date: '2021-08-31 10:10:00'
description: 'Te presento mi nuevo formato de “resumen mensual” donde vamos a ver un pequeño resumen de lo que pasó en el mes de Agosto '
type: 'noticias'
tags: ["ubuntu", "windows", "debian", "fuchsia", "apps", "agosto", "resumen", "2021", "deepin", "manjaro", "pine", "onlyfans", "plutotv"]
category: ["Noticias"]
img: 'https://i.postimg.cc/QtJ6b978/tecnonews.webp'
authors: ["PatoJAD"]
---

Un resumen de lo que paso en este mes de agosto de una forma breve y rápida que está interpretada por mi y con la idea de mantenernos actualizados con las novedades más relevantes de la tecnología de este mes para que arranques *con de todo* el mes que viene…

{{< youtube code="IIh8NmS08lo" >}}

## Juegos

* Se publicó el **0 AD** Alpha 25 que está lleno de novedades, tantas que no me dieron ganas de leerlas todas. Sin embargo los usuarios comentan que es mucho más estable que el lanzamiento del **CyberPunk**
* **Steam Deck** marca el futuro de Valve, ahora se refactoriza big picture para darle la nueva interfaz de los Steam Deck.
* **Netflix** siente que dominó el Streaming y ahora apuesta a los videojuegos, incorporando a su aplicación juegos de celular. Y ya podes descargar el de Stranger things
* La **Legión Linux Gamers** decidió incorporar un nuevo juego llamado ET Legacy que consiste en hacer enojar al streamer y está relacionado con los nazis y los yankees.
* **LLG** también está estrenando {{< textlink url="https://legionlinuxgamers.ga/" text="nueva web" >}} y una nueva comunidad que se incorpora activamente al Satff.
* **Thrive**, el spore open source, actualiza a su versión 0.5.5 con muchas mejoras gráficas y correcciones de errores.


## Escritorios

* **Mate** lanzó la versión 1.26 con soporte mejorado para Wayland y un sin fin de mejoras en sus aplicaciones donde la más beneficiada es Pluma, el mítico editor de textos.
* **KDE** lanzó múltiples actualizaciones para sus aplicaciones pero las más visibles van a poder verlas en Okular, Konsole y Dolphin
* **Wayland** contento con que mejora el soporte para realidad virtual, pero los usuarios esperamos solo que sea performante con juegos.
* **Gnome** lanza la beta 41 con más mejoras para Wayland y una nueva interfaz para las apps de llamadas.


## Aplicaciones

* **LibreOffice** llegó con su versión 7.2 con un HUD propio y algo sobre la interpolaridad que puedes leer en muchos portales de noticias.
* **Thunderbrid** 19 llegó con soporte para los M1 y entre sus tantas novedades nos avisa que si el email dice noreply no tenemos que contestar.
* Nueva versión de **Polychromatic**, la 0.7 se independiza del daemon de OpenRazer, noticia que solo importa si usas periféricos de **Rayzen**.
* **Latte Dock** el mejor dock que existe en linux lanzó la versión 0.10 donde mejora la integración con KDE, si es que esto era posible y además suma soporte para Gnome y XFCE.
* **CrossOver** lanza la versión 21 donde lo único interesante parece ser que actualizaron Wine y el soporte para el mando de play 5 pero esto solo es válido en Mac
* **Kitra** alcanza la versión 4.4.8 corrigiendo un fallo de guardado y algunos cambios más para Windows.
* **Kdenlive** también se actualiza y con la 21.08 llegan grandes mejoras que los editores de videos y creadores de contenido esperaban.
* **Ventoy** saca una GUI para linux para que dejen de excusarse los que no la usaban.
* **WhatsApp** agrega la función de escuchar audios antes de enviarlos para que la gente deje de enviar audios y borrarlos
* **Wine** 6.15 trae una cantidad de mejoras y cambios increíbles que desmienten a los 3 desubicados que decían que wine no se desarrollaba más.
* **Nmap* va por la versión 7.92 y si sos de los que usa esta herramienta deberías leer los cambios porque son muy interesantes.
* **GameMaker 2** ahora esta disponible para GNU/Linux. Ahora podes hacer juegos 2D con esta Plataforma directamente en tu distro favorita.
* **WhatsApp** llega a tablets y Ipad pero es completamente inutil porque no permite tener la misma cuenta en 2 dispositivos.
* **GitHub Copilot** generó un código más inseguro que el de estudiante de primer año de programación. Ahí se va el “reemplazo de los devs”
* **OpenShot** 2.0.6 llegó demostrando ser uno de los editores más completos de videos con funciones que buscan destronar a kdenlive


## Entretenimiento

* **PlutoTV** incorpora 5 canales nuevos y en uno de esos podes encotrar las 9 temporadas de “Doctor Who”
* En **Netflix** está disponible la peli animada de “Whitcher” pero no la vi todavia porque mi mujer sigue con finales y la estoy esperando
* **OnlyFans** penalizó el contenido sexualmente explícito, pero antes de que esto se volviera real se retractó al ver que el sitio no facturará nada.


## Hardware

* **Pine64** dio a conocer su nueva tablet que en realidad es un ebook con funciones de tablet pero con tinta electrónica.
* Una persona aburrida armó un modelo miniatura de la TV de los simpson que reproduce capítulos al azar y todo el mundo está fanatizado con eso.
* **Nvidia** anunció que el DLSS para proton soportará Directx 12.


## Distribuciones

* Llegó **Zorin 16** con más widgets para parecerse a otros sistemas operativos y como sabía a qué sistema de paquetería apostar metió a todos juntos y ya está.
* **Elementary** anunció su versión 6 donde, otra vez, afirma que apuesta por flatpack y le mete ganas a la app de cuentas en línea.
* **Ubuntu** da muestras de un nuevo instalador que nadie pidió ni que se entiende porque lo están haciendo.
* **Majaro 21.1** está disponible con gnome 40, los fanáticos de arch se preguntan porque esta distro demora cada dia mas.
* **Deepin** lanzó la versión 20.2.3 con la herramienta OCR, basado en Debian 10 y mejoras en DDE, que por si no lo sabías es su escritorio.
* **FuchsiaOS** el sistema de Google que planea dominar el mundo volvió a salir en boca de muchos debido a que Chrome está siendo portado a esta OS.
* **ChromeOS 92** mejoró al fin las videollamadas y le mete mas ganas a los escritorios virtuales.
* **Lakka 3.3** la nueva versión de este OS para juegos llegó lleno de actualizaciones como el kernel, retroarch y los emuladores entre otras cosas.
* **Open SUSE** recibe la certificación EAL que significa que es uno de los Sistemas operativos más seguros y que más respeta la privacidad.
* Otras que tambien se actualizaron pero no tengo nada para decir:
    * Debian 11
    * DebianEdu 11
    * Sparky 6
    * Pardus 21
    * IPFare 2.27
    * KaOS 2021.08
    * Voyager 11
    * Elive 3.8.22
    * 4MLinux

No te olvides que este mes cumplio años Linux y digo linux porque es el kernel así que ya tiene 30 años edad para los que celebran la presentación en el foro, si no sos de esos el mes que viene es el cumpleaños para los que festejan la primer publicación..

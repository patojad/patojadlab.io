---
title: 'CentOS: Una mala decisión'
date: '2021-01-07 09:10:00'
description: Red Hat decidió transformar CentOS Stream en su rama principal dando muerte al proyecto CentOS como se lo conocía.'
type: 'noticias'
tags: ["noticia", "centos", "redhat", "rhel", "rocky", "linux", "youtube"]
category: ["Noticias"]
img: 'https://rockylinux.org/img/cover.png'
authors: ["PatoJAD"]
---



En esta oportunidad, y como ya de costumbre llegando tarde, vamos a comentar un poco lo ocurrido con CentOS. Como sabemos Red Hat decidió migrar su rama principal de CentOS a su versión Stream mandando un aspecto más similar a Debian SID.




## Video



Como escribir mis pensamiento no me resulta muy fácil decidí, como siempre que llego tarde, hacer un video en el cual pueda contar mi **OPINIÓN** de las decisiones que tomó red hat y de lo que para mi es una buena salida de este “entierro”


{{< youtube code="SwloHc1xsa4" >}}



## Rocky Linux



Tal como nos dicen los de MuyLinux:



> El algo más definitivo es lo que muchos esperaban, **un fork de Red Hat Enterprise Linux (RHEL) con el que cubrir el hueco que deja CentOS**, auspiciado nada más y nada menos que por Gregory Kurtzer, cofundador de CentOS. La iniciativa se dio a conocer muy poco después del anuncio de Red Hat y aunque en un principio parecía más una tentativa que una propuesta en firme, ha tardado poco en conformarse como lo que a todas luces será una nueva continuación comunitaria de CentOS.



Existe una alternativa que está en desarrollo y podés obtener toda la información e incluso colaborar con el proyecto desde su sitio oficial donde puedes encontrar los contactos.


{{< link text="Sitio Oficial Rocky" url="https://rockylinux.org/es/" >}}



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: 'Conociendo la Feria de Solano Argentina'
date: '2021-09-22 15:01:00'
description: 'Recorrimos la feria de solano, te mostramos algunas tomas y te contamos la verdad sobre el tabu de "un lugar peligroso"'
type: 'noticias'
tags: ["solano", "feria", "reportage", "gastronomia", "shopping", "argentina", "2021"]
category: ["Noticias"]
img: 'https://i.postimg.cc/nzgSdtCL/solano.png'
authors: ["PatoJAD"]
---

En un apartado completamente diferente en este blog decidí no solo hacer lo que me gusta, sino también un poco cosas variadas sobre todo lo que rodea al personaje PatoJAD.

> "Bah! Esto no es RAP,¿Qué está intentando,innovar? Al Chojin se le va"
>
> {{< citaname name="El chojin" >}}

Con esta frase se puede explicar lo que van a sentir muchos al ver que este video no está orientado a Linux ni a la tecnología en general, es algo diferente, pero que según algunas ideas que tenemos pueden estar íntimamente relacionados en algún futuro.

## El video

Insisto el video no es a lo que venimos acostumbrados pero es realmente interesante o al menos desde mi punto de vista.

{{< youtube code="txvaWQe34Ck" >}}

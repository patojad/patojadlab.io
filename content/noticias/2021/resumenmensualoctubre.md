---
title: 'Octubre en pocos minutos'
date: '2021-10-29 15:25:00'
description: 'Seguimos con el nuevo formato de “resumen mensual” donde vamos a ver un pequeño resumen de lo que pasó en el mes de Octubre'
type: 'noticias'
tags: ["ubuntu", "debian", "pine", "phone", "apps", "octubre", "resumen", "2021", "noticias", "juegos"]
category: ["Noticias"]
img: 'https://i.postimg.cc/6qFJkMyg/tecnoticias.png'
authors: ["PatoJAD"]
---

En esta segunda edición recorremos el mes de **Octubre** con las novedades que trajo en el mundo linux-tecno que no podes perderte para arrancar el mes completamente informado

{{< youtube code="G-DMm7N0HZY" >}}

Ya estamos terminando el mes de octubre y no pueden faltarte las noticias. Si sos de esos que festejan halloween tenes como compromiso mandarme unos caramelos o un adorno para el canal y sin más rodeos vamos a las noticias de este hermoso mes.

## Aplicaciones

* Si sos usuario de **Gnome** podes *estar feliz* porque ya **DashToDock** ya trae soporte para Gnome 40.
* **FreeOffice** lanza la versión 2021. Los alemanes están con de todo.
* **KDE festeja sus 25 años** lanzando la *versión 5.23*, podrían haber sido más marketineros y ponerle 5.25 total nadie iba a preguntar donde estaban las dos versiones que faltaban.
* **Flatpack** se actualiza para amigarse con Steam y corregir un super error de vulnerabilidad que lleva mucho tiempo por ahí.
* **Kodi 19.3** vino arreglando varios errores y mucho antes de lo esperado.
* Si algún pesado no entendió que **WINE** sigue actualizando este mes metieron *más de 390 cambios en version 6.20*.
* **Kdenlive** llegó finalmente a MacOS y se declara multiplataforma, lo cual viene bien justo que tengo una Mac por aca.


## Juegos

* Una empresa comentó como el 5.8% de sus ventas de una alpha fueron para usuarios de linux y según ellos *“cubre los costos de desarrollar para linux y deja ganancias”*
* **Super Tux Kart** lanzó la versión 1.3 con mejoras de rendimiento Por si la tostadora te quemaba el pan, y mejoras en la GUI. Esto fue el mes anterior pero no lo dije


## Distribuciones

* **Canonical** busca feedback de su nuevo instalador. Si me está viendo alguien de canonical el feedback es que no era necesario pero esta lindo.
* Llegó Debian 11 con otro nombre sacado ToyStory. Si no sabías esto te quedaste con cara de que falopa nunca me di cuenta.
* **Mx Linux**, lanzó una nueva versión basada sobre Debian 11 y es una de las más finas que existen. Dudo que otras distros base *Antix* le lleguen a los talones.
* Salió **Ubuntu 21.10** pero si queres saber las novedades busca video sobre eso que sacaron 500 en todas las plataformas.


## Hardware

* Ya está disponible la **Raspberry PI Zero 2 W** a *tan solo 15 dólares*.
* **Nvidia** está cada día más cerca de ser compatible con Wayland.
* Demandan a **Vizio** el fabricante de TVs por no respetar la licencia GPL.
* **AMD** libera el código de **GPUFORT** en busca de volverse el preferido por todos.
* Se anunció el **PinePhone Pro** otro celular muy copado que no voy a tener en mi vida.

Por cierto, si estás intentando entrar en **Solo Linux** y no te funciona *deja de reiniciar el modem* que el problema es de Solo Linux. Y si alguien de Solo Linux ve esto por favor diganme como ayudarlos que su blog me encanta.

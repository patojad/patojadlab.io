---
title: "Consegui el Endless Space Free"
date: "2019-09-12"
description: "Nuevamente les traemos un juego Completamente Gratis y obviamente sin caer en piratería."
type: "juegos"
tags: ["Steam","AirStyle","Themes"]
category: ["Juegos"]
img: "https://i.postimg.cc/fLPhQVF8/Capture.png"
authors: ["PatoJAD"]
---
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/392110/ss_5e59d20c1b6e0313e6f2d8dff89e720769a122f6.600x338.jpg?t=1567783068" >}}



El juego traemos hoy es para Steam y si bien, no se encuentra disponible para Linux se puede probar con proton.  Es un juego de Estrategia que viene de la mano de la empresa SEGA. En Steam cuenta con críticas muy variadas por lo cual podemos decir que la comunidad no llego aun a calificarlo como bueno o malo. Lo cual lo deja enteramente a tu criterio.

El juego podemos jugarlo en español (Su interfaze y sus subtitulos) aunque los audios solo se encuentran en inglés. En Stem se pueden conseguir 115 logros con este juego



{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/392110/ss_92993a0d6df637d2bf516d78c98d124a752f3870.600x338.jpg?t=1567783068" >}}
 


## Requerimientos



### Minimos:

* Requiere un procesador y un sistema operativo de 64 bits
* SO: Windows (64bits only) 7 / 8 / 8.1 / 10
* Procesador: i3 4th generation / i5 2nd generation / A6 series
* Memoria: 4 GB de RAM
* Gráficos: Intel HD 4000 / AMD Radeon 5800 series / NVidia 550Ti
* DirectX: Versión 11
* Almacenamiento: 8 GB de espacio disponible
* Tarjeta de sonido: DX11 compatible
* Notas adicionales: Minimum Resolution: 1280 x 720

### Recomendados:

* Requiere un procesador y un sistema operativo de 64 bits
* SO: Windows (64bits only) 7 / 8 / 8.1 / 10
* Procesador: i3 5th generation (or newer) / i5 3rd generation (or newer) / FX4170 (or newer)
* Memoria: 8 GB de RAM
* Gráficos: AMD Radeon 8000 series or newer / NVidia GTX 660 or newer
* DirectX: Versión 11
* Almacenamiento: 8 GB de espacio disponible
* Tarjeta de sonido: DX11 compatible
* Notas adicionales: Recommended Resolution: 1920 x 1080

 

{{< link url="https://www.humblebundle.com/store/endless-space-collection-free-game?linkID=&mcID=102:5d789b1627c89c5ddd2823a2:ot:56cfa14f733462ca894cced9:1&utm_source=Humble+Bundle+Newsletter&utm_medium=email&utm_campaign=2019_09_12_endlessspace_collection_storefront&utm_content=Banner" text="Obtener FREE" >}}

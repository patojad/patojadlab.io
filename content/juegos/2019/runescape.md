---
title: "RunScape"
date: "2018-12-11"
description: "MMORPG Online"
type: "juegos"
tags: ["RunScape","MMORPG","Online"]
category: ["Juegos"]
img:  "https://i.postimg.cc/TwC9jbHC/Old-School-Rune-Scape-810x400.png"
authors: ["PatoJAD"]
---

### Elige tu Lucha
El combate de RuneScape es emocionante y desafiante, con muchas opciones y capacidades diferentes para elegir. Hay disponibles miles de combinaciones de armaduras y armas, y los jugadores más fuertes son aquellos que se preparan bien para la batalla. Los jugadores pueden tratar de derrotar a cientos de NPC diferentes a medida que avanzan en su aventura o pueden participar en combates con otros jugadores en áreas específicas de PvP. Sigue leyendo para descubrir por qué Combat es una parte tan importante del juego de RuneScape.

{{< img src="https://i.postimg.cc/KcpfRnTY/image.png" >}}

### Evolución del combate
Un sistema de combate dinámico y moderno basado en las habilidades de hacer clic. Se puede jugar completamente de forma manual o semiautomatizada, y permite batallas emocionantes y visualmente impresionantes. Es una excelente forma de jugar en todos los niveles, y se recomienda encarecidamente para los desafiantes encuentros de jefes finales del juego. Hay tres tipos de habilidades:

* Básico: estas habilidades son rápidas, generalmente basadas en daños, que pueden generar adrenalina. La adrenalina se usa para realizar habilidades más avanzadas y más fuertes, como el umbral y el final.
* Umbral: las habilidades de umbral hacen más daño que las habilidades básicas, pero también agotan el 15% de tu adrenalina cuando se lanzan. Las habilidades de umbral requieren 50% de adrenalina para usar.
* Ultimate: estas son las habilidades más poderosas. La mayoría de estas habilidades borran completamente tu barra de adrenalina y no generan adrenalina.

La mayoría de las habilidades se desbloquean a medida que entrenas tus habilidades de combate, pero algunas se pueden obtener de otras maneras: como gotas de criaturas poderosas o al completar misiones.

También puedes elegir automatizar tus habilidades seleccionando Revolution como tu modo de combate

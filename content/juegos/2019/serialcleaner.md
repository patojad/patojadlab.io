---
title: "Serial Cleaner Free"
date: "2019-11-22"
description: "Por un tiempo limitado, obtenga una copia gratuita de Serial Cleaner cuando se suscriba al boletín Humble Bundle."
type: "juegos"
tags: ["Steam","Serial Cleaner","FREE"]
category: ["Juegos"]
img: "https://hb.imgix.net/ee7ad4c8f6b5c148d057a0bc0a9852dfbdc3e360.png?auto=compress,format&s=602b439f1f5f8c387bc1d702f4ad23ae"
authors: ["PatoJAD"]
---

{{< img src="https://hb.imgix.net/32ebe17a897687e2d57c1ca905160bb05ebf770f.png?auto=compress,format&h=252&w=503&s=739bee12f30fde70bab73df55c48fb68" >}}



> Eres un limpiador profesional. Su trabajo es limpiar las escenas de asesinato eliminando cuerpos, limpiando manchas de sangre y ocultando evidencia incriminatoria. Un buen limpiador nunca queda atrapado, por lo que debe asegurarse de entrar, limpiar y salir sin dejar rastros de evidencia para que la policía los encuentre.




## CARACTERÍSTICAS DEL JUEGO:



* Ambiente inspirado en los años 70
* Uso de datos del mundo real para modificar los niveles de acuerdo con la hora actual del día en el paradero del jugador
* Múltiples tipos de enemigos ofrecen un nivel variable de desafío.
* La amplia variedad de modos de juego garantiza una gran capacidad de reproducción.
* los niveles se inspiran en escenas reales de asesinatos de los años 70
* humor y ambiente inspirados en la serie Fargo y la película Pulp Fiction
* enfoque lúdico para resolver cada nivel, que recompensa la exploración y la experimentación



{{< youtube code="fRboxdCs3zQ" >}}



## MÍNIMO:



* **SO:** Ubuntu 12.04+, SteamOS+
* **Procesador:** Intel Core Duo 2 2,5GHz
* **Memoria:** 2 GB de RAM
* **Gráficos:** Nvidia GeForce 9600 GT 512MB
* **Almacenamiento:** 2 GB de espacio disponible



{{< link url="https://www.humblebundle.com/store/serial-cleaner-free-game?linkID=&mcID=102:5dd490c523c7a14816277552:ot:56cfa14f733462ca894cced9:1&utm_source=Humble+Bundle+Newsletter&utm_medium=web&utm_campaign=PatoJAD&utm_content=Post" text="Obtener Ahora" >}}

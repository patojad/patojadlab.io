---
title: "Postal 4 proximamente llegara a linux"
date: "2019-12-26"
description: "Un usuario de Reedit indago, pregunto y nos trae su respuesta y reflexión a todos nosotros."
type: "juegos"
tags: ["Steam","Postal"]
category: ["Juegos"]
img: "https://steamcdn-a.akamaihd.net/steam/apps/707030/header.jpg?t=1571599564"
authors: ["PatoJAD"]
---

Esta información la compartió un usuario de Reedit, el cual con todas ansias fue directamente a preguntar, el contenido es de su autoría y solo traducimos (no literalmente) lo que el con su esfuerzo consiguió para todos nosotros. En mi opinión mucha razón tiene al apreciar a los desarrolladores que invierten tiempo en portar a Linux sus juegos.




## Lo que nos dice Jianmin_Tao




### El echo



Recientemente Postal 4: No Regerts se lanzó en Steam y GoG como un título de "acceso anticipado". Siendo fanático de Running With Scissors, les envié un correo electrónico y les pregunté sobre el soporte de Linux, viendo cómo todos sus juegos anteriores estaban disponibles para Linux (con la excepción de Postal III, que no es del todo, y oficialmente lo rechazan y si me preguntan les diria "no lo compren") Postal 4 actualmente no tiene una compilación de Linux, así que pensé en enviarles un correo electrónico y preguntar. Esta fue la respuesta que obtuve



> Hola Jianmin_Tao,
>
> ¡Seguro que admitiremos Linux con una compilación nativa ya que tenemos todos nuestros juegos son migrados! Sin embargo, para ser claros, no es solo una casilla simple de realizar, muchas cosas se rompen al exportar a Linux (lo intentamos ya) y necesitarán una consideración especial, y por esta razón, estaremos esperando hasta que lleguemos a la versión final 1.0 hasta Abordamos a esa bestia. Por el momento, estamos escuchando informes de que P4 funciona bastante bien con proton.
>
> ¡Gracias por el apoyo!
>
> Jon




### La Opinion



También me gustaría tomarme un momento y hablar sobre Running With Scissors como desarrolladores; porque creo que a menudo se pasan por alto.



Para aquellos que no lo saben, Running With Scissors es el desarrollador detrás de la serie de juegos Postal. Postal es una serie conocida por su violencia exagerada y su humor grosero y ofensivo. Por ejemplo, en Postal 2, si tienes una escopeta y necesitas un silenciador, puedes levantar un gato callejero y... ponerlo en el extremo de la escopeta. A los policías no les gusta que dispares a la gente, no dudes en orinar sobre ellos. Estos son algunos de los ejemplos más menores del humor crudo y ofensivo en estos juegos. Di lo que quieras sobre eso, no estoy aquí para juzgar eso, ni de eso se trata este hilo (Referente a Reedit).


{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/707030/ss_fb9d09af4af0e737ecc577c5127b8d06e1ff9412.600x338.jpg?t=1571599564" >}}


De lo que me gustaría hablar es del hecho de que Running With Scissors ha sido siempre un desarrollador increíble, y un desarrollador increíble para la comunidad Linux. El primer juego Postal salió en 1997, también fue lanzado para Linux (en 2001), todavía es compatible hoy, Y fue de código abierto en 2016. Postal 2 salió en 2003, fue lanzado para Linux en 2005, todavía es compatible, es seguía recibiendo actualizaciones e incluso tenía un paquete de expansión lanzado en 2015. Postal 3 de la que no hablamos. Luego llegamos a Postal Redux, una nueva versión del primer juego Postal; Fue lanzado en 2016, tanto para Windows como para Linux, y nuevamente, todavía es compatible y todavía recibe actualizaciones. La existencia de Postal Redux no ha negado en absoluto el lanzamiento original o el soporte del lanzamiento original, como lo hacen muchos remakes/remasters. Además de todo esto, Running With Scissors tiene una larga historia de estar muy cerca de la comunidad en torno a sus juegos. Por ejemplo, en 2005, hubo un mod de horror de supervivencia lanzado para Postal 2 con el nombre de Eternal Damnation. Menos de un año después, los desarrolladores incluyeron como parte del lanzamiento oficial como parte del "Postal Fudge Pack", una compilación de todos los juegos y expansiones de Postal lanzados hasta ese momento. El punto es que, a pesar de lo objetable que pueda ser la calidad y el contenido de sus juegos, son desarrolladores increíbles que merecen reconocimiento y amor por parte de la comunidad de Linux, y quiero llamar la atención de cualquiera que los haya pasado por alto; e imploro a todos ustedes que compren Postal 4, y denles el apoyo que realmente merecen.

{{< link text="Hilo original" url="https://www.reddit.com/r/linux_gaming/comments/efs1p1/postal_4_will_eventually_be_coming_to_linux/" >}}
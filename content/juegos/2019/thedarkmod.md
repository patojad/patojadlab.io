---
title: "The Dark MOD"
date: "2039-03-18"
description: "The Dark Mod es un exelente juego inspirado en la seria Thief del Looking Glass Studios. Es OpenSource y tiene una gran trayectoria"
type: "juegos"
tags: ["OpenSource","MMORPG","Free"]
category: ["Juegos"]
img:  "https://wiki.spiralknights.com/es/images/b/bd/SpiralKnights_News_44-big.png"
authors: ["PatoJAD"]
---

{{< youtube code="brJqHnXmpgE" >}}

>El mundo del Dark Mod es oscuro y anacrónico de forma que tiene elementos medievales al igual que elementos de la era victoriana. Aunque el Dark Mod fue inspirado en la seria Thief del Looking Glass Studios, no tiene ni materiales ni nombres de ella a causa del Copyright.
>
>El jugador es un ladrón que tiene que ir tirando en un mundo hostil y siniestro. Por eso él roba a la gente o escala en las casas de la gente rica por la noche. Chantajes y asesinatos también pueden ser una opción. Como el jugador tiene las habilidades de lucha capadas, debe evitar a sus enemigos, escondiéndose en las sombras y evitando hacer algún ruido. Para lograr su objetivo el jugador puede utilizar un equipamiento especial como ganzúas, varios tipos de flechas, pociones, explosivas y más.
>
>{{< citaname name="wikipedia" >}}

{{< img src="https://www.thedarkmod.com/wp-content/themes/thedarkmod/images/tdm_title.png" >}}

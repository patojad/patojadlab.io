---
title: "AirStyle para Steam"
date: "2019-01-19"
description: "Un theme que nos permite renovar un poco nuestro Steam"
type: "juegos"
tags: ["Steam","AirStyle","Themes"]
category: ["Juegos"]
img: "https://i.postimg.cc/DzPcG0ZB/Deepin-Screenshot-20181205105759.png"
authors: ["PatoJAD"]
---

Muchas personas jugamos en GNU/Linux, no es irreal ni algo imposible. Y tambien a muchos nos importa la estética de nuestro sistema o nos aburre la monotonía. Para ambos casos en esta oportunidad traigo una solución para cambiarle un poco la cara a nuestro cliente de _Steam_ para que ¡ No sea mas aburrido!.

En esta oportunidad subí personalmente la versión que mejor me funcionó y con la cual no tuve ningún problema. Sabiendo que todo funciona bien les dejo el link para que lo descarguen y luego lo instalen en unos simples pasos:



{{< link url="https://www.mediafire.com/file/2z2bnn223yy2vv6/Air-for-Steam-LatinLinux.zip/file" text="Desacargar" >}}



Una vez descargado nos dirigimos a .steam/skins y pegamos la carpeta descomprimida.

{{< img src="https://i.postimg.cc/HWXQK3XF/1544019059933.png" >}}

Luego iniciamos _Steam_ y nos dirigimos a párametros -> interfaz y ahi seleccionamos el nuevo theme.

{{< img src="https://i.postimg.cc/2ScvvtNL/1544019146365.png" >}}

Una vez seleccionado, guardamos, reiniciamos _Steam_ y ¡Podemos verlo de la siguiente forma!:

{{< img src="https://i.postimg.cc/bvZnRRqC/Deepin-Screenshot-20181205105846.png" >}}

---
title: "Grid 2 Free"
date: "2019-03-14"
description: "Un exelente juego de Carreras para windows"
type: "juegos"
tags: ["Steam","GRID2","Juego"]
category: ["Juegos"]
img: "https://i.postimg.cc/SsJ2LrZy/grid2-patojad.png"
authors: ["PatoJAD"]
---

>Ponte a los mandos, pisa a fondo y persigue la fama en GRID 2, la nueva entrega de Race Driver: GRID, el juego que ganó un premio BAFTA y vendió millones de copias.

Así se presenta uno de los mejores juegos de carreras, este juego se encuentra bien logrado y en esta ocasión podemos obtener keys gratis para steam gracias a humble bundle.

### ACERCA DE ESTE JUEGO

Ponte a los mandos, pisa a fondo y persigue la fama en GRID 2, la nueva entrega de Race Driver: GRID, el juego que ganó un premio BAFTA y vendió millones de copias. Sumérgete en agresivas carreras rueda con rueda contra una IA avanzada y vive a fondo la competición con el nuevo sistema de control TrueFeel™, que transmite al detalle la emoción de ponerse al volante de unos coches legendarios. La nueva generación de la plataforma tecnológica para juegos EGO te ofrece unos gráficos únicos en este género y un sistema de daños sensacional para que te pongas a prueba en tres continentes distintos con competiciones de otro nivel. Saborea la fama y amasa una fortuna a un ritmo vertiginoso con carreras intensas e implacables dentro de circuitos oficiales, calles perfectamente recreadas y peligrosas carreteras de montaña. GRID 2 también establecerá un nuevo estándar en cuanto a carreras multijugador gracias a modos innovadores, un sistema de progresión totalmente independiente y una total integración en RaceNet, la extensión gratuita en línea para los juegos de Codemasters Racing.

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/44350/header.jpg?t=1546282708" >}}

Grid 2 Es Competición Como Nunca.
* Las carreras cobran vida con el alucinante sistema de control TrueFeel™ de GRID 2.
* Mídete a una IA avanzada en carreras agresivas y demoledoras repletas de momentos impresionantes.
* Ábrete paso hasta la cima de este nuevo universo de los deportes de motor.
* El extenso modo en línea es independiente y redefine las carreras multijugador, además de estar totalmente integrado en RaceNet.
* La plataforma tecnológica para juegos EGO de Codemasters te ofrece un sistema de daños sensacional y unos gráficos espectaculares, de modo que GRID 2 fija un nuevo estándar de excelencia en su género.
* Pilota una cuidada selección de coches legendarios que representan lo mejor de la automoción de los últimos 40 años.
* Compite en circuitos oficiales, calles perfectamente recreadas y peligrosas carreteras de montaña.
* Ponte a prueba: participa y gana carreras en tres continentes distintos.
* La tecnología de inmersión te sumerge en las carreras como nunca antes.
* Por fin, la ansiada continuación de Race Driver: GRID, que ganó un premio BAFTA y vendió millones de copias.

### REQUISITOS MINIMOS DEL SISTEMA

* OS: Windows Vista, Windows 7, Windows 8
* Processor: Intel Core 2 Duo @ 2.4Ghz or AMD Athlon X2 5400+
* Memory: 2 GB RAM
* Hard Disk Space: 15 GB HD space
* Video Card: Intel HD Graphics 3000 / AMD HD2600 / NVIDIA Geforce 8600
* DirectX®: 11
* Sound: Direct X compatible soundcard
* Additional: Broadband Internet connection
* Additional Supported Graphics Cards: Intel HD Graphics 3000, Intel HD Graphics 4000, Intel Graphics 5200.
* AMD Radeon HD2600 or higher, HD3650 or higher, HD4550 or higher, HD5000 Series, HD6000 Series, HD7000 Series.
* NVIDIA GeForce 8600 or higher, 9500 or higher, GTX220 or higher, GTX400 Series, GTX500 Series, GTX600 Series: AMD Fusion A8 or higher.




Si bien solo es nativo para windows nos conviene tener en nuestra biblioteca el juego para poder jugarlo con protón o desde otro Sistema libre como ReactOS. También podemos aventurarnos a instalarlo en wine, pero no podemos perdernos esta oportunidad de tener GRID 2 con sus DLC




{{< link url="https://www.humblebundle.com/store/grid2-spa-bathurst" text="Pedir Key" >}}

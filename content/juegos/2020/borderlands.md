---
title: "Epic Games hoy nos ofrece: Borderlands"
date: "2020-05-28 12:00:00"
description: "Epic Games viene dándonos muchos juegos increíbles, como sabemos este gran esfuerzo por ganar leales..."
type: "juegos"
tags: ["epic","games","juego","gratis","free","borderlands"]
category: ["Juegos"]
img: "https://steamcdn-a.akamaihd.net/steam/bundles/8133/gzgratfuwmpa1072/header_586x192.jpg"
authors: ["PatoJAD"]
---

Epic Games viene dándonos muchos juegos increíbles, como sabemos este gran esfuerzo por ganar leales, tarea muy difícil debido a los años que tiene Steam ofreciendo buen servicio, pero sí debemos admitir que muchos ya lo tienen instalado en sus PC junto al mítico Steam.

Si bien No es nativo en GNU/Linux muchos juegos pueden instalarse y esperemos que este no sea una excepción, aun no lo pude probar por lo que no podemos asegurar su funcionamiento.




## Borderlands: The Handsome Collection



Dispara y consigue botín mientras exploras el caótico mundo de Pandora en Borderlands 2 y la locura lunar de Borderlands: La Pre-Secuela en Borderlands: Una colección muy guapa. Disfruta de la premiada serie FPS-RPG con tropecientas armas, juego cooperativo marca de la casa y todo el contenido adicional.




### Borderlands: Una colección muy guapa incluye:



* Los aclamados BORDERLANDS 2 y BORDERLANDS: LA PRE-SECUELA en un solo pack
* Todo el contenido adicional de ambos juegos, que añade horas y horas de diversión*
* Juego cooperativo para hasta cuatro jugadores en línea



*Incluye todo el contenido descargable lanzado para Borderlands 2 y Borderlands: La Pre-Secuela hasta el 31 de diciembre de 2017.*


{{< link url="https://www.epicgames.com/store/es-ES/product/borderlands-the-handsome-collection/home" text="Obtener Gratis" >}}


Puede que este caido el sitio pero tenes tiempo hasta el 4 de Junio para reclamarlo

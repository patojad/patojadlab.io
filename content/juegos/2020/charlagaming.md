---
title: "Gaming en GNU/Linux por Diego Pardo"
date: "2020-07-23 09:48:00"
description: "Ya hablamos nosotros de nuestro punto de vista, ahora te invitamos a la charla de Diego Pardo en el SFD"
type: "juegos"
tags: ["juegos", "linux", "puede", "charla", "sfd", "ecuadortl", "diego", "pardo"]
category: ["Juegos"]
img: "https://hardzone.es/app/uploads-hardzone.es/2018/03/linux-jugar-ordenador.jpg"
authors: ["PatoJAD"]
---



Ya con anterioridad hable del gaming en GNU/Linux, y mucho intente explicar desde mi punto de vista. Pero siempre digo que este blog es de la comunidad y como tal es importante saber lo que piensan ellos también. En esta oportunidad quiero recomendar la asistencia a una charla organizada por EcuadorTL en el SFD




## Sobre la charla



En esta oportunidad es **Diego Pardo** el orador de esta charla que recibe el nombre de *“Gaming en GNU/Linux”*. Ingeniero Electricista, amante de GNU Linux y el gaming en PC, creador del canal en YouTube Linux Gaming Español y creador de contenido en Twitch de juegos ejecutados en GNU Linux y nativos.


{{< youtube code="Fzma_kQdIGA" >}}
Video al azar de nuestro compañero


El gaming en Pc, ha visto una gran demanda en los últimos años y GNU/Linux no ha quedado atrás en el avance de la la creación y ejecución de juegos en entornos Linux. En esta charla veremos este avance, el estado actual y al importancion del gaming en GNU.




## Información práctica



* **Fecha** 19/09/2020 12:30 (America/Guayaquil)
* **Duración** 30 minutos
* **Locación** Software Freedom Day 2020 - Online


{{< link url="https://www.ecuadortl.club/event/software-freedom-day-2020-online-2020-09-19-1/track/gaming-en-gnu-linux-9#scrollTop=0" text="Mas Información" >}}



## Adicional



También te recomiendo como calentamiento para este dia donde vamos a escuchar a una persona que sabe del tema, leer un poco artículos relacionados sobre el gaming en linux que te voy a ir dejando a continuación...



* {{< textlink url="/juegos/2020/07/se-puede-jugar-en-linux/" text="¿Se puede jugar en linux?" >}}
* {{< textlink url="https://www.youtube.com/watch?v=i3cmsvrOI2w" text="¿No se puede o no se hace? La actualidad de Linux" >}}



Y si te gusta jugar no te podes perder el {{< textlink url="/juegos/2020/07/el-primer-torneo-de-supertuxkart/" text="primer torneo de Super Mario Kart" >}} que cada día procura más convertirse en el inicio de grandes cosas...



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: "Sega 60th Aniversario"
date: "2020-10-15 09:43:00"
description: "Sega festeja su aniversario 60 y nos regala muchos juegos para que lo disfrutemos con ellos"
type: "juegos"
tags: ["sega", "NiGHTS", "sonic", "steam", "free", "gratis", "company of heroes", "windows", "two point hospital", "valve"]
category: ["Juegos"]
img: "https://60th.sega.com/message/assets/images/msg/user_img/5547.png"
authors: ["PatoJAD"]
---



Sega está celebrando su aniversario número 60 y esto para nosotros se traduce en poder obtener juegos gratis en nuestra cuenta de steam, claramente, para los que tenemos steam. Así que vamos adelante con todos los juegos que podemos reclamar hoy!




## NiGHTS into Dreams


{{< img src="https://cdn.cloudflare.steamstatic.com/steam/apps/219950/header.jpg" >}}


Sumérgete en el mundo de los sueños y emprende una aventura aérea como NiGHTS en este clásico remake de Saturno. Recoge Ideya (orbes de colores), acumula puntos y lucha contra jefes para ayudar a Elliot y Claris a salvar Nightopia de Wizeman the Wicked.


{{< steamgame code="219950" >}}


Para reclamar este juego debemos registrarnos en la web de los 60th de Sega y darle acceso a nuestra cuenta de Steam para que nos dé el juego en nuestra biblioteca y así poder descargarlo. También puedes usar otra plataforma para reclamarlo


{{< link text="Registrate y Reclama el juego" url="https://www.sega60th.com/" >}}



## Sonic 2


{{< img src="https://cdn.cloudflare.steamstatic.com/steam/apps/71163/header_alt_assets_0.jpg" >}}


El Dr. Eggman (también conocido como el Dr. Robotnik) ha vuelto, convirtiendo a los indefensos animales en robots y obligándolos a construir su arma definitiva, ¡el Death Egg! Pero esta vez, Sonic tiene un amigo que puede ayudarle.


{{< steamgame code="71163" >}}


Este juego puedes reclamarlo ahora y quedará en tu cuenta de **Steam** para siempre y podrás jugarlo cuando quieras.




## Company of Heroes 2: Southern Fronts (DLC)


{{< img src="https://cdn.cloudflare.steamstatic.com/steam/apps/260290/header.jpg" >}}


En este caso hablamos de un DLC lo cual requiere el juego original para poder habilitarlo, si no lo tienes deberías aprovechar la oferta del 95% que tenemos el dia de hoy en el juego base


{{< steamgame code="231430" >}}


Una vez que lo tengamos podemos habilitar el DLC simplemente registrándose en la web del juego y enlazando con nuestra cuenta de Steam.


{{< steamgame code="260290" >}}

{{< link text="Registrate y Reclama" url="https://giveaway.companyofheroes.com/" >}}



## Two Point Hospital: SEGA 60th Items (DLC)


{{< img src="https://cdn.cloudflare.steamstatic.com/steam/apps/1424940/header.jpg" >}}


Nuevamente hablamos de un DLC que requiere su juego base y el mismo está en oferta y podemos aprovecharlo con un 70% menos.


{{< steamgame code="535930" >}}


Y ya luego podemos reclamar directamente desde Steam el DLC desde su página o desde la siguiente info.


{{< steamgame code="1424940" >}}



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

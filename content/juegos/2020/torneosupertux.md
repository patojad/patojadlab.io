---
title: "El primer torneo de SuperTuxKart"
date: "2020-07-20 08:58:00"
description: "Quienes obtengan los tres primeros puestos de la gran final, ganarán un curso de su elección, gracias a la gentiliza de nuestros amigos JuncoTIC."
type: "juegos"
tags: ["juegos", "linux", "tux", "kart", "supertuxkart", "tuxkart", "torneo"]
category: ["Juegos"]
img: "https://www.ecuadortl.club/web/image/588/TorneoSuperTuxKart.png?access_token=ecf91674-3e56-4c6c-98c4-0ac6897a9597"
authors: ["PatoJAD"]
---

El primer torneo de SuperTuxKart organizado por la Comunidad Latina de Tecnologías Libres.


{{< img src="https://www.ecuadortl.club/web/image/588/TorneoSuperTuxKart.png?access_token=ecf91674-3e56-4c6c-98c4-0ac6897a9597" >}}


**Premios:** Quienes obtengan los tres primeros puestos de la gran final, ganarán un curso de su elección, gracias a la gentiliza de nuestros amigos JuncoTIC.




## Reglas del Torneo:



* Para participar debes registrarte, dejanos tus nombres, correo electrónico y elije cual es el curso de tu interés por si llegas a ganar.
* Formaremos de forma aleatoria grupos de 4 jugadores (dependiendo de la cantidad de jugadores inscritos esto puede variar)
* La gran final está pensada para que corran 4 jugadores de los cuales los tres primeros puestos reciben el de su elección como premio.



**Estas reglas están en construcción y se actualizarán...**


{{< link text="Inscribirse" url="https://www.ecuadortl.club/event/1er-torneo-supertuxkart-online-2020-08-15-2/register" >}}



## Sobre el Juego



**SuperTuxKart** es un juego libre y gratuito de carreras de coches tipo arcade en 3D, cuyo protagonista es Tux, la mascota del kernel Linux. La idea de su creación surgió como una mejora alternativa del juego **TuxKart**.


{{< youtube code="Lm1TFDBiIIg" >}}


Se puede elegir entre un jugador, con ventana completa, dos, tres y cuatro jugadores con pantalla partida. Hay dos opciones de juego: carrera simple y Gran Premio. En el primer caso, se elige un circuito de los múltiples disponibles y gana el que complete antes el número de vueltas. En el segundo, se hace un recorrido por 4 circuitos y gana el que más puntos sume.

A lo largo del recorrido se encuentran nitros y cajas sorpresa, que incluyen desde elementos de ayuda hasta trampas.



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: "Ultimos dias para obtener Among US Completamente GRATIS"
date: "2021-05-29 10:30:00.00"
description: "Hasta el 3/6 Podes reclamar el popular juego de impostores completamente gratis"
type: "juegos"
tags: ["epic","store","juego","gratis","free","games","windows","linux","amongus"]
category: ["Juegos"]
img:  "https://www.lavanguardia.com/files/og_thumbnail/uploads/2020/09/09/5faa7208ed0b1.jpeg"
authors: ["PatoJAD"]
---

Ya habíamos hablado anteriormente en este Blog de {{< textlink url="/juegos/2020/09/como-jugar-among-us-en-linux/" text="como instalar Among US en Linux" >}}. Pero mucha gente no tenía el juego, así que hoy no puedes perderte la oportunidad de obtener este juego que corre tranquilamente con Wine en cualquier PC de Bajo recursos!!

{{< img src="https://generacionxbox.com/wp-content/uploads/2021/05/epic-games-store-juegos-gratis-free.jpg.webp" >}}

La gran tienda de Epic Games que intenta revocar a Steam de su puesto nos regala este popular juego para todos los que tengamos la cuenta verificada. Si sos un hábil usuario de Linux ya sabrás que con Wine puedes correr tanto Epic como la mayoría de sus juegos

{{< img src="https://i.blogs.es/7836f8/at-cm_449030120-preview/450_1000.jpeg" >}}

> Un juego para 4 a 10 jugadores en línea o en modo local mediante wifi en el que debéis preparar vuestra nave espacial para el despegue. Pero, cuidado: ¡uno o más jugadores elegidos al azar entre la tripulación son impostores dispuestos a matar al resto!

## El Juego

{{< img src="https://www.lavanguardia.com/files/og_thumbnail/uploads/2020/09/09/5faa7208ed0b1.jpeg" >}}

* **Vida de tripulante:** Completa las tareas para ganar, pero ¡cuidado con los impostores! Informa de cadáveres y convoca reuniones urgentes para expulsar al impostor por votación. ¡Ojalá no te equivoques!
* **Juega como impostor:** Siembra el caos, acecha sigilosamente e incrimina a inocentes. Para ganar, tendrás que matar a todos los tripulantes. ¿Se te da bien sabotear?
* **Personalización:** ¡Elige cómo quieres jugar! Añade más impostores o tareas, reduce la visibilidad ¡y mucho más! Juega con estilo eligiendo tu color, disfraz y gorro favoritos.
* **Multiplataforma:** ¡Juega con tus amigos entre PC, Android y iOS!

{{< link url="https://www.epicgames.com/store/es-ES/p/among-us" text="OBTENER AHORA" >}}

---
title: "Frostpunk GRATIS por tiempo limitado"
date: "2021-06-04 09:40:00.00"
description: "No pierdas mas tiempo y obtene este increible juego completamente GRATIS"
type: "juegos"
tags: ["epic","games","juego","gratis","free","store","windows","frostpunk"]
category: ["Juegos"]
img:  "https://cdn1.epicgames.com/salesEvent/salesEvent/EGS_FrostpunkGameofTheYearEdition_11bitstudios_Bundles_S1_2560x1440-904a427fe3afc998cd7cb8b96d304fa0?h=270&resize=1&w=480"
authors: ["PatoJAD"]
---

Como ya estamos acostumbrados todas las semanas **Epic Games** libera algún juego completamente gratis para todos nosotros. Y en esta oportunidad le tocó a **Frostpunk** que desde ayer podemos guardar en nuestra cuenta de **Epic Store**

## Frostpunk

{{< img src="https://media.vandal.net/i/1200x630/6-2021/20216317493078_1.jpg" >}}

Frostpunk es el primer juego de supervivencia de una sociedad. Como dirigente de la última ciudad del mundo, tu deber será el de lidiar tanto con los ciudadanos como con la infraestructura. ¿Qué harás cuando estés entre la espada y la pared? ¿En quién te convertirás para conseguirlo?

### Acerca del juego

{{< img src="https://cdn.cloudflare.steamstatic.com/steam/apps/323190/ss_5f9c9d5944a98b68b3b57c418f4267a459c757f8.1920x1080.jpg?t=1622541593" >}}

Frostpunk, el título más reciente de los creadores de This War of Mine, es un juego de supervivencia en sociedad donde el calor es fuente de vida y cada decisión tiene un precio. En un mundo totalmente congelado, la humanidad ha desarrollado una tecnología basada en el vapor para hacer frente al frío implacable. Tu tarea consiste en construir la última ciudad de la Tierra y conseguir los recursos necesarios para que tu comunidad sobreviva.

## Requerimientos

* **Procesador:** Procesador Dual Core a 3,2 GHz
* **Memoria:** 4 GB de RAM
* **Almacenamiento:** 8 GB de espacio disponible
* **Tarjeta gráfica:** GeForce GTX 660, Radeon R7 370 o equivalente con 2 GB de VRAM

{{< link text="Obtener Juego Gratis" url="https://www.epicgames.com/store/es-ES/p/frostpunk" >}}

---
title: "Ya estan los 2 Nuevos juegos GRATIS de Epic"
date: "2021-06-17 11:50:00.00"
description: "No pierdas mas tiempo y obtene estos increibles juegos completamente GRATIS"
type: "juegos"
tags: ["epic","games","juego","gratis","free","store","windows","frostpunk"]
category: ["Juegos"]
img:  "https://cdn1.epicgames.com/salesEvent/salesEvent/egs-overcooked2-Wide_2560x1440-808fbab09ed3ab49d6d0107683cbba8b"
authors: ["PatoJAD"]
---

Estamos acostumbrándonos a que Epic Regale juegos, y hoy no es la excepción, se habilitaron ya los siguientes 2 juegos que podemos reclamar. Y vamos a hablar un poco de estos juegos que ya podes agregar a tu lista de juegos.

## Overcooked! 2

{{< img src="https://i.postimg.cc/PqgfdD4y/image.png" >}}

¡Vuelve Overcooked con una buena y caótica ración de acción culinaria! Adéntrate de nuevo en el Reino de la Cebolla (Onion Kingdom) y reúne a tu equipo de chefs en clásicas partidas multijugador en modo cooperativo local o en línea para hasta cuatro jugadores. No tires el delantal… ¡Es hora de volver a salvar el mundo!

### ¡DE LA SARTÉN A LAS BRASAS!

Has salvado el mundo del Glotón Eterno (Ever Peckish). Ahora, una nueva amenaza ha emergido. ¡Ha llegado el momento de volver a los hornos y saciar el apetito del Pan Demonium (Unbread)!
CAOS MULTIJUGADOR EN LÍNEA Y LOCAL
Trabaja con otros jugadores (o contra ellos) para alcanzar la máxima puntuación en caóticas partidas multijugador locales y en línea.

### UNA DELICIA VISUAL

Recorre un mapa nuevo por tierra, mar y aire. Ponte a cocinar en nuevos escenarios, como restaurantes de sushi, escuelas de magia, minas y hasta planetas alienígenas.

### ¡ABRE EL APETITO!

Viaja por el mundo y cocina una serie de nuevas recetas para todos los gustos, como sushi, tartas, hamburguesas y pizzas.

### ¡NO METAS LA GAMBA!

Viaja con teletransportadores, cruza plataformas móviles y ahorra tiempo lanzando ingredientes por cocinas dinámicas que cambian y van evolucionando. Algunas cocinas hasta lanzarán a tus chefs a zonas nuevas.

### Requisitos

* **Procesador** Intel i3-2100, AMD A8-5600k
* **Memoria** 4 GB
* **DirectX** 11
* **Tarjeta gráfica** GeForce GTX 630, Radeon HD 6570

{{< link text="Obtener Overcooked! 2" url="https://www.epicgames.com/store/es-ES/p/overcooked-2" >}}


---

## Hell is Other Demons

{{< img src="https://i.postimg.cc/8PbzSsxQ/image.png" >}}

Hell is Other Demons es un juego de disparos y acción de plataformas con elementos roguelite. Explora un vasto mundo dibujado a mano, repleto de demonios y memorables batallas contra jefes, con una banda sonora de marcado estilo synthwave.

### Requisitos

* **Sistema operativo** Windows 7 o superior
* **CPU** Intel i3 o superior
* **GPU** Nvidia 450 GTS, Radeon HD 5750 o superior
* **Memoria** 4 GB de RAM
* **Almacenamiento** 500 MB
* **Inicios de sesión** Se necesita una cuenta de Epic Games

{{< link text="Obtener Hell is Other Demons" url="https://www.epicgames.com/store/es-ES/p/hell-is-other-demons" >}}

---
title: "Steam Deck Analisis"
date: "2021-07-27 10:30:00.00"
description: "Hoy voy a analizar y dar mi opinión sobre SteamDeck y lo que valve quiere hacer, tambien recordemos el fracaso de las Steam Machines"
type: "juegos"
tags: ["steam","deck","arch","switch","steamos"]
category: ["Juegos"]
img: "https://i.postimg.cc/GtJxbPmK/steamdeck-1080x609.webp"
authors: ["PatoJAD"]
---

{{< img src="https://i.postimg.cc/GtJxbPmK/steamdeck-1080x609.webp" >}}

Ya se anunció Steam Deck y mucha revolucion género, hoy vamos a hacer un análisis de este lado del mundo (el de GNU/Linux) sobre todo esto que vamos a vivir desde el año que viene que se entreguen estos dispositivos...

{{< youtube code="QHtIKBF39sA" >}}

## La portabilidad se alía con la potencia

{{< img src="https://i.postimg.cc/0NTYmX6Z/1601762882-AMD-Ryzen-ahora-representa-mas-del-25-de-las-CPU-1.webp" >}}

Steam se ha asociado con AMD para crear la APU personalizada de Steam Deck, optimizada para el juego en dispositivos portátiles. Se trata de una potente combinación de las arquitecturas Zen 2 y RDNA 2 que ofrece un rendimiento más que suficiente para ejecutar los últimos juegos AAA en una envolvente de potencia muy eficiente.

## Hardware

* **Procesador**
  * APU AMD
  * **CPU:** Zen 2 de 4 núcleos/8 hilos, 2.4-3.5 GHz (hasta 448 GFLOPS FP32)
  * **GPU:** RDNA 2 con 8 unidades de cálculo, 1.0-1.6 GHz (hasta 1.6 TFLOPS FP32)
  * Consumo de la APU: 4-15 W
* **RAM**
  * 16 GB de RAM LPDDR5 integrada (cuatro canales de 32 bits, 5500 MT/s)
  * Almacenamiento
  * eMMC de 64 GB (1 PCIe Gen 2)
  * NVMe SSD de 256 GB (4 PCIe Gen 3)
  * NVMe SSD de 512 GB de alta velocidad (4 PCIe Gen 3)
*Todos los modelos utilizan módulos 2230 m.2 (no diseñados para ser reemplazados por el usuario final)*
*Todos los modelos incluyen una ranura para tarjetas microSD de alta velocidad*
* **Resolución:** 1280 x 800 píxeles (relación de aspecto 16:10)
* **Tipo Pantalla:** LCD IPS con laminación óptica para mejorar la legibilidad
* **Tamaño de pantalla:** 7" de diagonal
* **Brillo:** 400 nits (típico)
* **Frecuencia de actualización:** 60 Hz
* **Táctil:** Sí
* **Sensores:** Sensor de luz ambiental
* **Bluetooth:** Bluetooth 5.0 (compatible con mandos, accesorios y audio)
* **Wi-Fi:** Radio wifi de doble banda, 2.4 GHz y 5 GHz, 2 x 2 MIMO, IEEE 802.11a/b/g/n/ac


## Software

| | |
|---|---|
| Sistema operativo | SteamOS 3.0 (basado en Arch) |
| Escritorio | KDE Plasma |

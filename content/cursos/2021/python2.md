---
title: "Universidad Python 2021 - POO, Django, Flask y Postgresql"
date: "2021-06-16 17:16:00"
description: "Nuestros amigos de Udemy tienen disponible por 18HS este curso para aprender Programacion Python"
type: "cursos"
tags: ["curso", "programacion", "web", "gratis", "udemy", "limitado", "python", "django", "flask", "postgresql", "developer"]
category: ["Cursos"]
img: "https://cdn-thumbs.comidoc.net/750/2867812_4227_12.jpg"
authors: ["PatoJAD"]
---

Antes que nada debemos todos agradercer a **Federico J. Przemyslanski** que nos informo de esta gran oportunidad!!!

> De Cero a Experto en **Python**: **POO**, Web con **Django**, **Flask**, **Jinja**, **SQL Alchemy**, **Postgresql**, **PyCharm**!

## Requisitos

* No se requiere ningún conocimiento previo de programación
* Sólo se requiere una computadora, cualquier sistema operativo funciona con Python

## Lo que aprenderás

* Aprenderán a programar con Python desde cero hasta experto!
* Desde los temas más básicos, hasta intermedios y avanzados!
* Programación Orientada a Objetos con Python
* Conexión a Bases de Datos con Python y Postgresql
* Creación de aplicaciones Web con Django y Python
* Creación de aplicaciones Web con Flask y Python
* Todo dentro de un mismo curso, Universidad Python

## Este curso incluye:

* 23,5 horas de vídeo bajo demanda
* 3 artículos
* 188 recursos descargables
* Acceso de por vida
* Acceso en dispositivos móviles y TV
* Tareas
* Certificado de finalización

## Descripción

Universidad Python 2021 [Actualizado 2021]: Este es el mejor curso para aprender Python desde Cero hasta Experto. Somos más de +67,000 alumnos inscritos al curso y creciendo rápidamente, con una calificación promedio de 4.7 (de 5 estrellas), la calificación más alta de los cursos de Python.

Aprenderás Python desde las bases, NO necesitas ningún tipo de experiencia programando, iremos avanzando desde lo más básico hasta llegar a niveles intermedios y avanzados, todo en este mismo curso, la Universidad Python.

Estudiaremos desde los Fundamentos de Python utilizando PyCharm como Herramienta para desarrollar tu código (Funciona en Windows, Mac o Linux). Además aprenderás la POO (Programación Orientada a Objetos) en Python, Manejo de Archivos con Python, Conexión a Base de Datos con Postgresql y el conector de psycopg2, el desarrollo de aplicaciones Web con Django utilizando el IDE de PyCharm, aprenderemos el ORM de Django para la creación de clases de modelo que mapean a tablas de base de datos, el concepto de Migraciones con Django, Panel de Administración de Django, etc. También aprenderás el Microframework de Flask para el desarrollo de aplicaciones Web incluyendo Jinja para la creación de Templates HTML con Flask, el uso de SQL Alchemy para el mapeo ORM de tus clases de modelo, Flask Migrate con Alembic, entre muchísimos temas más.

## Tiempo para Reclamar

APURATE QUE QUEDA POCO TIEMPO!!

{{< countdown anio="2021" mes="06" dia="17" hora="14" minuto="0" >}}

{{< link url="https://www.udemy.com/course/universidad-python-desde-cero-hasta-experto-django-flask-rest-web/?ranMID=39197&ranEAID=d2gvurItCFk&ranSiteID=d2gvurItCFk-zd1MAunr6ILjziqCuJ88gQ&LSNPUBID=d2gvurItCFk&utm_source=aff-campaign&utm_medium=udemyads&fbclid=IwAR18olM7THKqr8tMPRxr6YZdEOtUYWvZCS34UVOgjQAWa0X6T7umfgyV-00&couponCode=GM_ONCE" text="Inscribite al Curso" >}}

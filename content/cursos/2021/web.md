---
title: "Programacion FrontEnd Web - GRATIS por tiempo limitado"
date: "2021-06-15 09:56:00"
description: "Nuestros amigos de Udemy tienen disponible por 48HS este curso para aprender Programacion FrontEnd Web"
type: "cursos"
tags: ["curso", "programacion", "web", "gratis", "udemy", "limitado", "css", "html", "js", "front", "developer"]
category: ["Cursos"]
img: "https://blog.facialix.com/wp-content/uploads/2021/06/3895716_00dc_7.jpg"
authors: ["PatoJAD"]
---

> El mejor curso para aprender a crear aplicaciones Web Modernas con **HTML**, **CSS** y **JavaScript**. Serás un *FrontEnd Developer*!

## Requisitos

* Uso básico de PC y conexión a Internet
* No se requiere ningún conocimiento de programación, comenzamos desde cero y llegamos a niveles avanzados!

## Lo que aprenderás

* Crear aplicaciones Web de Cero a Experto, utilizando **HTML**, **CSS** y **JavaScript** con las últimas versiones al día de hoy!
* Aprenderán absolutamente desde cero **HTML**, **CSS** y **JavaScript** con el objetivo de crear increíbles aplicaciones Web
* Conseguir empleo como *FrontEnd Web Developer*

## Este curso incluye:

* 30,5 horas de vídeo bajo demanda
* 2 artículos
* 312 recursos descargables
* Acceso de por vida
* Acceso en dispositivos móviles y TV
* Tareas

## Descripción

En este curso *Universidad Desarrollo Web - FrontEnd* aprenderás las tecnologías **HTML**, **CSS** y **JavaScript** con las últimas tecnologías y herramientas que te permitirán dominar y crear aplicaciones Web modernas, convirtiéndote en el *FronEnd Developer* que las empresas están buscando.

Aprenderás **HTML**, **CSS** y **JavaScript** a profundidad, la mano del Ing. Ubaldo Acosta, experto en desarrollo Web y aplicaciones Empresariales, con más de 20 años de experiencia laboral y más de 10 años enseñando vía online. Fundador de Global Mentoring y  del exitoso curso Universidad Java.

Este curso de *Universidad Desarrollo Web* te permitirá aprender en tiempo record todo lo necesario para crear tus propios proyectos como Freelance o conseguir el empleo de tus sueños como *FrontEnd Developer*.

Los temas que veremos en este curso son:

* **Universidad HTML:**
    * Introducción a HTML
    * Instalación de Herramientas (Visual Studio Code)
    * Elementos Básicos de HTML
    * Introducción a CSS
    * Links en HTML y CSS
    * Tablas en HTML
    * Listas en HTML
    * Elementos inline y block en HTML
    * Formularios en HTML
    * Elementos Semánticos en HTML
* **Universidad CSS:**
    * Introducción a CSS
    * Selectores en CSS
    * Manejo de Colores en CSS
    * Manejo de Borders en CSS
    * Manejo de Margenes en CSS
    * Manejo de Padding en CSS
    * Manejo de Box Model
    * Manejo de Fuentes en CSS
    * Float en CSS
    * Íconos en CSS
    * Propiedad Display en CSS
    * Posición de Elementos en CSS
    * Selectores en CSS
    * Gradientes en CSS
    * Box Shadow en CSS
    * Flexbox en CSS
* **Universidad JavaScript:**
    * Introducción a JavaScript
    * Tipos de Datos en JavaScript
    * Operadores en JavaScript
    * Sentencias de Control en JavaScript
    * Ciclos en JavaScript
    * Arreglos en JavaScript
    * Funciones en JavaScript
    * Objetos en JavaScript
    * Clases en JavaScript
    * Palabra static en JavaScript
    * Manejo de Herencia en JavaScript
    * Sistema de Ventas con JavaScript
    * Proyecto Mundo PC con JavaScript
    * POO (Programación Orientada a Objetos) en JavaScript
    * Manejo de Errores en JavaScript
    * Funciones Flecha (Arrow Functions) en JavaScript
    * Funciones Callback en JavaScript
    * Promesas en JavaScript
    * Manejo del DOM HTML con JavaScript
    * Manejo de Eventos con DOM HTML y JavaScript
    * Aplicación Calculadora con HTML, CSS, JavaScript y Bootstrap
    * Aplicación SAP (Sistema de Administración de Personas) con HTML, CSS y JavaScript
    * Aplicación Reloj Digital con HTML, CSS y JavaScript
    * Aplicación Presupuesto (Ingresos-Egresos) en HTML, CSS y JavaScript

## Tiempo para Reclamar

{{< countdown anio="2021" mes="06" dia="17" hora="0" minuto="0" >}}

{{< link url="https://www.udemy.com/course/universidad-desarrollo-web-moderno-html-css-javascript-html5-css3/?ranMID=39197&ranEAID=sIt9MIeGnaM&ranSiteID=sIt9MIeGnaM-yB9f7ZnRLrP8438x45K6vg&LSNPUBID=sIt9MIeGnaM&utm_source=aff-campaign&utm_medium=udemyads&couponCode=GM_ONCE" text="Inscribite al Curso" >}}

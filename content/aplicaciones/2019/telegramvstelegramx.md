---
title: 'Telegram vs Telegram X'
date: '2019-08-13'
description: 'Telegram X el cliente ganador del desafio Telegram'
type: 'aplicaciones'
tags: ["Telegram"]
category: ["Aplicaciones"]
img: 'https://luvcrypto.com/wp-content/uploads/2018/04/Telegram-versus-Telegramx.png'
authors: ["PatoJAD"]
---

## Telegram Vs Telegram X



Telegram X fue el proyecto ganador de un concurso que invitaba a crear una nueva aplicación Telegram usando su nueva librería experimental. Si bien en su presentación recibió otro nombre, tras la victoria en el concurso los desarrolladores fueron contratados y se pusieron al frente del proyecto Telegram X que es la versión BETA/Experimental del cliente oficial.

{{< img src="https://androidayuda.com/app/uploads-androidayuda.com/2018/01/telegram-x-muestra-de-chats-768x545.png" >}}
<div class="text-center">Izquierda: modo burbujas. Derecha: modo plano.</div>

## Velocidad



En este factor tenemos un único ganador. Telegram X gano supuesto debido a su velocidad y sus animaciones. Es muy fluido y consume muy pocos recursos lo cual lo pone como primera elección para dispositivos más antiguos. Tiene un nivel de personalización mucho más elevado, aunque debe pulir mucho todavía.

{{< img src="https://i2.wp.com/www.dignited.com/wp-content/uploads/2018/02/photo_2018-02-07_07-45-10.jpg" >}}



 



## ¿La velocidad define todo?



La respuesta corta es NO y la respuesta larga Por supuesto que no. Si bien es muy importante la velocidad no es un factor decisivo. Es como comprar un auto por su velocidad pero que venga sin carrocería. Telegram tiene más tiempo y más experiencia, se encuentra más pulido y con menos fallos, lo cual es una base sólida para nuevas implementaciones o incluso para optimizaciones (En las últimas Versiones bajo su consumo de memoria) y se jacta de no tener errores como los comúnmente reportados en Telegram X con las notificaciones.

{{< img src="https://lh3.googleusercontent.com/proxy/fo8bGvjdd3G0qINH9_VvrvkHcq3MiPVnT6iAD_a8sFm6XsSa-mZXsS2OWt1pl-Gj848HhnecEXE-W1TuTBFGh-b9Wiq-3YIbKTg4RgdA7wDP=w506-h910" >}}



## ¿Qué debo elegir?



Claramente siempre debemos elegir lo que nos guste, El nivel de personalización y las animaciones son zona de Telegram X sin embargo lo que buscamos es estabilidad y un buen theme oscuro para ser feliz Telegram es nuestro mejor candidato

¿Y Vos que pensas?

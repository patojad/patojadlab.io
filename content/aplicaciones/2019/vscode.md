---
title: 'VS Code 1.30 "Ayuda al Programador"'
date: '2019-01-12'
description: 'Editor de Código'
type: 'aplicaciones'
tags: ["","","",""]
category: ["Aplicaciones"]
img: 'https://i.postimg.cc/g0vBxwYS/Visual-Studio-Code.png'
authors: ["PatoJAD"]
---

Si bien no esta en mi stock de aplicaciones para programar no podemos dejar de lado el trabajo que esta realizando y el esfuerzo que realiza MS Microsoft para participar del Open Source.

En esta oportunidad en su actualización, ¡Nos trae herramientas que están pensadas para simplificarnos el día a día de los Programadores!

Lo primero que se ha destacado de Visual Studio Code 1.30 es la mejora del soporte de la búsqueda multilínea que fue introducido en el anterior lanzamiento de la aplicación. Haciendo que ahora no sea imprescindible la introducción de una expresión regular. El usuario puede recurrir a la combinación de teclas _“Shift + Intro”_ para introducir varias líneas en la barra de búsqueda o bien copiar y pegar un texto de varias líneas procedente de un fichero.

{{< img src="https://i.postimg.cc/jqQcy0Y5/Mejora-en-el-soporte-de-la-busqueda-multilinea-en-Visual-Studio.pngg" >}}

Se incrusta la barra de menú en la barra de título, haciendo que se ahorre espacio en pantalla, pero posiblemente dificulte la gestión de la propia ventana en monitores que no tengan mucha resolución.

<div style="text-align: center"><p> Podés seguir viendo la info desde {{< textlink url="https://code.visualstudio.com/updates/v1_30" text="aqui" >}} </p></div>

---
title: 'eDEX-UI'
date: '2018-12-11'
description: 'Una terminal futurista que dejara anonados a nuestros colegas'
type: 'aplicaciones'
tags: ["Terminal","edex-ui","console","futurista"]
category: ["Aplicaciones"]
img:  'https://i.postimg.cc/YSxfkgkM/edex-ui-scifi-computer-interface.jpg'
authors: ["PatoJAD"]
---

eDEX-UI es una aplicación de escritorio de pantalla completa que se asemeja a una interfaz de computadora de ciencia ficción, inspirada en gran medida en DEX-UI y los efectos de película TRON Legacy. Ejecuta el shell de su elección en un terminal real y muestra información en vivo sobre su sistema. Fue diseñado para ser usado en pantallas táctiles grandes, pero funcionará bien en una computadora de escritorio normal o tal vez en una tableta o en una de esas computadoras portátiles de 360 ° con pantalla táctil.

{{< img src="https://i.postimg.cc/YSxfkgkM/edex-ui-scifi-computer-interface.jpg" >}}





{{< link url="https://www.mediafire.com/file/b52tmfjif1dimj2/eDEX-UI+Linux+%28x86_64%29.AppImage" text="Descargar" >}}

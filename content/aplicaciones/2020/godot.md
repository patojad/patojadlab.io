---
title: 'AQUÍ VIENE GODOT 3.2, CON LA CALIDAD COMO PRIORIDAD'
date: '2020-01-29 10:41:00'
description: 'GODOT 3.2 ya es oficial y aqui te traemos sus novedades! Mira todo lo que la nueva version trae para vos!'
type: 'aplicaciones'
tags: ["godot","juegos"]
category: ["Aplicaciones"]
img: 'https://godotengine.org/storage/app/uploads/public/5e3/15f/979/5e315f979aa78053750274.jpg'
authors: ["PatoJAD"]
---


Los colaboradores de Godot están encantados y encantados de lanzar nuestra última actualización importante, Godot 3.2 . ¡Es el resultado de más de 10 meses de trabajo de cerca de 450 contribuyentes (300 de ellos contribuyeron a Godot por primera vez) que crearon más de 6000 compromisos!

Godot 3.2 es una mejora importante con respecto a nuestra instalación 3.1 anterior, que ofrece docenas de características principales y cientos de correcciones de errores y mejoras para brindar a nuestros desarrolladores de juegos un conjunto de características cada vez mejor con un fuerte enfoque en la usabilidad.




## LANZAMIENTO PEQUEÑO CRECIDO GRANDE


{{< img src="https://user-images.githubusercontent.com/6093119/55293018-88de3b00-53f1-11e9-99b3-cf63f1ae0094.gif" >}}


Inicialmente, se planeó que Godot 3.2 tuviera un ciclo de lanzamiento corto (de 4 a 6 meses), con pocas características nuevas y en su mayoría soluciones para algunos de los problemas principales de nuestro lanzamiento principal anterior, Godot 3.1 (marzo de 2019). Y, de hecho, poco después de lanzar 3.1 y haber escrito un puñado de nuevas características destinadas a 3.2, nuestro desarrollador principal, Juan Linietsky, pasó a desarrollar el próximo procesador Vulkan para Godot 4.0 en una rama separada .

Pero el resto de nosotros, los contribuyentes del motor, no permanecimos inactivos mientras tanto, y se enfocó en solucionar tantos problemas como pudimos para hacer de Godot 3.2 un lanzamiento duradero. Muchas características nuevas introducidas en Godot 3.0 (enero de 2018) y 3.1 aún necesitaban refinamiento, por lo que se invirtió mucho trabajo en esas áreas para mejorar la usabilidad, implementar componentes faltantes y corregir errores reportados por nuestra creciente base de usuarios.

Si bien la rama de desarrollo estaba congelada de funciones (es decir, no se aceptaron grandes funciones nuevas) a fines de agosto de 2019 , tomó varios meses de trabajo pulir las nuevas funciones y corregir muchos de los errores de larga data que se posponían desde el lanzamiento hasta lanzamiento. Para Godot 3.2, nuestros colaboradores profundizaron en las profundidades del rastreador de problemas y resolvieron cerca de 2000 problemas reportados a lo largo de los años, ¡ya en 2015 !

Y por lo tanto, sin previo aviso, la versión 3.2 "pequeña" se convirtió orgánicamente en algo casi tan grande como su predecesora 3.1, lo que llevó a la versión ETA a noviembre de 2019, y después de tener en cuenta el retraso habitual de 2-3 meses más allá de nuestra mejor estimación , ¡aquí estamos!

De hecho, el lanzamiento de hoy marca el segundo aniversario de Godot 3.0, que se lanzó el 29 de enero de 2018 . ¡Es una forma inesperada pero agradable de iniciar el viaje de desarrollo hacia nuestro próximo gran logro, Godot 4.0!

¡Esperamos que todos disfruten la versión 3.2 tanto como nosotros disfrutamos al desarrollarla!




## COMPATIBILIDAD


{{< img src="https://user-images.githubusercontent.com/6964556/59157685-7ebb7580-8ae1-11e9-95c0-2b2179985fa8.gif" >}}


Casi todas las áreas del motor han visto algún grado de mejora, y alentamos a los usuarios a mover sus proyectos activos a Godot 3.2 si pueden. Hicimos todo lo posible para preservar la compatibilidad entre los proyectos 3.1 y 3.2, pero todavía se han realizado una baja cantidad de cambios de compatibilidad y podría requerir un trabajo de portabilidad ligero para grandes proyectos.

Para los usuarios que elijan permanecer en la rama 3.1, seguiremos manteniéndolo con correcciones de errores relevantes y cambios específicos de la plataforma en los próximos meses (especialmente con una versión de mantenimiento 3.1.3). Sin embargo, el foco principal de la actualización estará en la rama 3.2.




## NUEVAS CARACTERÍSTICAS



Ahora a las cosas buenas: ¿qué hay de nuevo en Godot 3.2?

Ha habido miles de cambios, grandes y pequeños, por lo que enumerar todo sería imposible. Sin embargo, puede consultar el registro de cambios detallado , donde intentamos enumerar los cambios más relevantes, separados por categoría: adiciones , cambios , eliminaciones y correcciones .

En el resto de esta publicación, nuestro objetivo es ofrecer una visión general de las características y cambios más notables en Godot 3.2. Puede leer en orden o usar el índice a continuación para saltar a sus áreas de interés. Las áreas del motor se enumeran sin ningún orden en particular.

* Documentación: más contenido, mejor tema
* Mono / C #: compatibilidad con Android y WebAssembly
* AR / VR: compatibilidad con Oculus Quest y ARKit
* Revisión de sombreadores visuales
* Mejoras de gráficos / renderizado
* Canal de activos 3D: glTF 2.0 y FBX
* Redes: WebRTC y WebSocket
* Sistemas de compilación y complementos de Android
* Nuevas funciones del editor
* Herramientas de codificación
* 2D: Pseudo 3D, Atlas de texturas, AStar2D
* GUI: flujo de trabajo de ancla / márgenes, efectos RichTextLabel
* Generadores de audio y analizador de espectro.
* Descomposición convexa mejorada

{{< link url="https://godotengine.org/article/here-comes-godot-3-2" text="Mas Informacion" >}}

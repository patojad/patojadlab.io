---
title: 'Cerebro'
date: '2020-05-22 10:30:00'
description: 'Hoy gracias Humberto Blanqueto Melche traemos otro hermoso launcher basado en el de MacOS y muy interesante...'
type: 'aplicaciones'
tags: ["ulauncher","launcher","krunner","cerebro","apps"]
category: ["Aplicaciones"]
img: 'https://d2.alternativeto.net/dist/s/cerebroapp_412593_full.png?format=jpg&width=1600&height=1600&mode=min&upscale=false'
authors: ["PatoJAD"]
---

Seguimos trayendote los mejores launchers para tu distro de linux. En esta ocasión le toca cerebro, pero antes de ponernos a hablar de él tengo que agradecerle enormemente a **Humberto Blanqueto Melche** (el creador de {{< textlink url="/estilo/2020/05/plasmapple/" text="PlasmApple" >}} ) por darme a conocer esta hermosa app que está inspirada en el launcher de MacOS.



Si bien no terminé de acostumbrarme a usarlo (Realmente soy muy fiel a {{< textlink url="/aplicaciones/2020/05/ulauncher/" text="Ulauncher" >}}) me sorprendió que si bien su último release es de Diciembre del 2017 es una aplicación que tiene todo lo que uno necesita y más. Pensé que me iba a encontrar con algo viejo e inútil. Pero la verdad me sorprendio las **comodidades y facilidades** que tiene. Lo mejor de todo es que tiene una API muy simple que nos permite extenderlo como nos plazca…




## Busca todo


{{< img src="https://cerebroapp.com/images/search.png" >}}


Busca todo en unos pocos clics. En su máquina o en Internet. Interactúe con todo: abra el archivo en el programa predeterminado o revele en el buscador, copie los detalles de contacto en el portapapeles, vea las sugerencias de Google.




## Ver todo


{{< img src="https://cerebroapp.com/images/previews.png" >}}


Mapas, traducciones, archivos. Ahora no tiene que abrir otra aplicación para ver lo que necesita.Todo está en un solo lugar.




## Hacer todo


{{< img src="https://cerebroapp.com/images/plugins.png" >}}


Con el administrador de complementos incluido, siempre puede encontrar y usar lo que desee. ¿No hay un complemento que estás buscando? Utilice una {{< textlink url="https://github.com/KELiON/cerebro/blob/master/docs/plugins.md" text="API" >}} simple pero potente para crear sus propios complementos y compartirlos con la comunidad.




## Codigo Abierto


{{< img src="https://cerebroapp.com/images/open-source.png" >}}


Cerebro es gratuito y de código abierto. Sea parte de la comunidad : contribuya con sus mejoras, sugerencias y soluciones.




## Instalacion



Como siempre **nuestra comunidad** busca que todos los linuxnautas tengan las apps de una forma simple y segura por lo cual te ofrecemos desde el **repositorio de la comunidad** una instalación fácil y sencilla.

Una vez que tengamos instalado el repositorio de la comunidad que si aun no lo tienes instalado te recomiendo ingresar {{< textlink url="/repositorio/" text="aquí" >}} aquí para ver cómo se hace e instalarlo. basta con ejecutar la instalación desde la terminal:



    sudo apt install cerebro



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

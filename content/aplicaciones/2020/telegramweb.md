---
title: 'Telegram Web en tu sitio web'
date: '2020-12-16 09:55:00'
description: 'Hablemos la nueva versión de Telegram Web de la Comunidad y como instalarlo en tu sitio'
type: 'aplicaciones'
tags: ["telegram", "web", "online", "sitio", "mi", "tu", "instalar" , "gratis"]
category: ["Aplicaciones"]
img: 'https://i.postimg.cc/TYsyTXLM/image.png'
authors: ["PatoJAD"]
---



Hace ya un tiempo lanzamos la primera versión de Telegram Web de la comunidad, esta versión disponía ser muy minimalista y simple de utilizar con un diseño un poco más personalizado y unos temas muy simples. Siendo la opción rápida  de uso de telegram para todos los usuarios…

Lamentablemente esto durante el tiempo no recibió muchas actualizaciones volviéndose un sitio un poco… anticuado…




## Nueva versión


{{< img src="https://i.postimg.cc/qMvnkL1n/image.png" >}}


El proyecto original avanzó a pasos agigantados y teníamos que ponernos en línea, por lo cual decidimos rebasar el proyecto y agregarle todo nuestro desarrollo a posteriori… Esto llevo bastante tiempo pero hoy podemos ver una nueva versión de Telegram Web en nuestra comunidad disponible para todos los usuarios…




## ¿Que trae?



En esta nueva actualización podemos ver la corrección de varios errores como botón de iconos que no permitía deslizar a stickers o gifs. Y suma algunas cosas como:  


{{< img src="https://i.postimg.cc/TYsyTXLM/image.png" >}}


* Soporte para Carpetas
* Restricción de administración
* Nuevo diseño
* Nuevo LogIn mas estetico
* Soporte multi idiomas
* Acceso directo a Guardados
* Mucho más…




## Yo quiero el mio!



Como siempre digo en este mundo lo importante es compartir… Por lo cual esto es totalmente replicable para quienes así lo deseen y es un proceso tan fácil que cualquiera puede hacerlo para eso dejo toda la información necesaria a continuación.


{{< img src="https://i.postimg.cc/BncGMTGY/image.png" >}}


* {{< textlink url="https://patojad.com.ar/programacion/2019/11/configurar-gitlab-pages-con-cloudflare/" text="Configurar la URL">}}
* {{< textlink url="https://patojad.com.ar/programacion/2019/10/gitlab-pages/" text="Informacion sobre GitLab Pages" >}}
* {{< textlink url="https://gitlab.com/patojad/telegram" text="El codigo de Telegram con el CI para subir a GitLAB (Se puede forkear)" >}}



Si necesitas ayuda puedes contar con nosotros en nuestro grupo de telegram para poder levantar tu instancia




---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

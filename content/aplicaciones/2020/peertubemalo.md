---
title: 'PeerTube no es para creadores de contenido'
date: '2020-11-20 09:45:00'
description: 'Hoy usaremos nuestro movil desde '
type: 'aplicaciones'
tags: ["peertube", "youtube", "twitch", "lbry", "lbry.tv", "creadores", "contenido"]
category: ["Aplicaciones"]
img: 'https://i.postimg.cc/65Bwqc4W/image.png'
authors: ["PatoJAD"]
---


{{< img src="https://i.postimg.cc/65Bwqc4W/image.png" >}}


Desde que comencé a intentar crear contenido en la comunidad de linux, por diversos factores, me incentivo a moverme a plataformas libres. Si bien **apoyó rotundamente el Software Libre** tenemos que entender algunas cosas y una es que PeerTube es un *GRAN PROYECTO*, tiene muy buen desarrollo, e incluso es muy funcional. Pero no está pensado para creadores de contenidos, solo está pensado para los usuarios y eso no motiva a nadie.




## Google no es Dios


{{< img src="https://i.postimg.cc/t4CPH83F/image.png" >}}


Como siempre digo, si bien estoy en **Youtube** no creo que **Google** sea un buen ejemplo a seguir. Creo que hay muchas cosas que no nos dicen y eso está mal. Aun así soy yo el que decide darle acceso a toda mi vida. Pero por eso existen otras alternativas privativas como **Twitch** que es por donde realizo los directos o libres como **LBRY.TV** que si está pensada para creadores.




## Mucho pedimos y poco damos


{{< img src="https://i.postimg.cc/t4NjFmwv/image.png" >}}


Eventualmente a los creadores (No es mi caso porque no me conoce ni Dios) se les exige realmente mucho. Ya sea con ideas o demandando contenido. Muchos de los creadores realizamos esto **porque lo amamos** y nos gusta compartir u opinar. Sin embargo, más allá de la pantalla hay una vida, un trabajo, una familia y en muchos casos estudios. ¿Saben lo difícil que es equilibrar todo esto? Muchos quisiéramos generar mucho contenido y de calidad, pero el día solo tiene 24 Hs y no nos da el tiempo. Afortunados son quienes pueden vivir de su contenido y mejorar la calidad y cantidad de contenido creado.




## ¿Todo para el creado es dinero?


{{< img src="https://i.postimg.cc/WbMLPHNp/image.png" >}}


**NO**, y a los creadores que yo conozco no les interesa el dinero en sí. Pero hay un momento en el que te das cuenta que no podes comer con amor. Que los remedios no se compran con amor… El dinero es lamentablemente lo que necesitamos para vivir y conforme a eso dedicamos horas de nuestra vida para conseguirlo.




## ¿Qué tiene que ver esto con PeerTube?


{{< img src="https://i.postimg.cc/65Bwqc4W/image.png" >}}


Claramente el hecho es que las instancias de PeerTube no nos permiten *monetizar* nuestro contenido, por lo cual no podemos conseguir rédito por nuestro contenido. Cosa que de alguna forma nos ayuda a mejorar el contenido con herramientas o tiempo… Cosas que los usuarios de los creadores de contenidos deberíamos valorar de aquellos que generan el contenido con pequeñas acciones que son gratis para uno pero llenan al creador…




## Entonces… ¿Siempre hay que ir a lo privativo?


{{< img src="https://i.postimg.cc/nz7TQNwC/image.png" >}}


**PARA NADA**… Es más, existe una plataforma libre en la que puedes encontrar todo el contenido de mi canal y es totalmente libre. Incluso esta plataforma permite “donar” a los canales con las criptomonedas que generamos al ver contenido todos los días, o seguir personas. En {{< textlink url="/aplicaciones/2020/06/lbry-en-busca-de-la-libertad/" text="LBRY" >}} tanto los creadores y los usuarios son valorados y son recompensados sin publicidades.


{{< link url="https://lbry.tv/$/invite/@PatoJAD:d" text="Registrate en LBRY con mi invitación" >}}



## Pato y PeerTube



En lo personal intente darle muchas posibilidades a **PeerTube** pero no colabora en nada… No me permite siquiera automatizar la subida, ocupando mucho más tiempo y volviendo el trabajo muchisimo mas manual. Así mismo me registre en múltiples instancias y en casi todas obtuve problemas de habilitación de cuentas entre otras cosas.


{{< img src="https://i.postimg.cc/RFQKmmDQ/image.png" >}}


Tras pelear mucho para subir videos o replicar los subidos a **Youtube** me di cuenta que la plataforma, de alguna forma, te **penaliza por ser creador**, poniendo muchos palos en la rueda, asegurándose así que el contenido para los usuarios sea más de agrado con su filosofía. Teniendo como costo que los creadores con comunidades realmente chicas decidamos declinar el uso de esta plataforma.




## Entonces... ¿PeerTube es malo?


{{< img src="https://i.postimg.cc/sDBx9y46/image.png" >}}


**NO**, no es malo, todo lo contrario es muy bueno, y si algun dia me vuelvo un creador de contenido con una amplia comunidad o autosustentable intente darle otra oportunidad a **PeerTube**. Pero hoy, en mi estado de iniciante en este mundo de la creación de contenido PeerTube es mas un enemigo que un amigo...




---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: 'PatoJAD TV'
date: '2020-05-13 08:53:00'
description: '¿Queres ver la tele desde cualquier lado? Hicimos algo para que puedas llevar la tele a donde vayas'
type: 'aplicaciones'
tags: ["iptv","series","peliculas","tv","online"]
category: ["Aplicaciones"]
img: 'https://i.postimg.cc/zvm74cc2/Deepin-Screenshot-Seleccionar-rea-20200513085251.png'
authors: ["PatoJAD"]
---

Hoy venimos a hablar de un proyecto que puede ser algo útil durante esta cuarentena, si bien el proyecto llegó tarde nos alegra poder mostrar su **BETA para todos los usuarios interesados**, actualmente seguimos buscando formas de mejorarlo y para esto aceptamos criticas de todos.




## ¿Que es?



Es un sitio web que no es más ni menos que una **IPTV**, en al cual se puede reproducir contenido LIVE (en un futuro esperamos añadir OnDemand), Este sitio se encuentra alimentado de canales externos que nada tiene que ver con el mismo, es decir que nosotros desarrollamos el uso de IPTV y no de los canales.

Es posible que los canales aportados por externos se vean mejor o peor dependiendo de muchos factores, sin embargo el sitio **cuenta con una excelente grilla** y van sumándose día a día, al igual que funcionalidades nuevas.




## ¿Que ofrece?



* Más de 70 canales en Español
* Soporte para M3U8
* Programación variada
* Soporte para Youtube LIVE
* Usabilidad desde cualquier dispositivo
* Intervencion de la comunidad




## Mirando hacia adelante



Tenemos planeado **incorporar soporte a más plataformas de streaming**, adicional a esto creemos que estaría bueno poder sumar contenido on demand, sin embargo necesitamos la **colaboración de la comunidad** para poder sumar todo esto y es ahí donde entras vos, proba nuestro IPTV, Disfrutalo, Recomendalo y decinos eso que para vos te hace falta, o eso que queres ver, o simplemente colabora con el proyecto!


{{< link text="ir a PatoJAD TV" url="https://tv.patojad.com.ar/" >}}


Recorda **apoyarnos en nuestras redes** para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con **pequeños gestos**, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: 'Waifu'
date: '2020-04-23 09:59:00'
description: '¿Te gusta el Anime? Waifu es el popcorn creado para vos, podes encontrar todo el anime en una sola app'
type: 'aplicaciones'
tags: ["popcorn","series","peliculas","anime","waifu"]
category: ["Aplicaciones"]
img: 'https://img.utdstc.com/screen/1/waifu-3.jpg'
authors: ["PatoJAD"]
---

Waifu una aplicación de streaming para ver anime online P2P (basada en torrent) que te permite ver anime en 720p HD, con subtitulos en Español, ingles, portugues, frances, aleman. (actualmente ES, LA, EN, PT, FR, DE)

Todavia estamos en fase beta, por lo que si encuentras algún fallo no dudes en contactar con nosotros en Twitter o en nuestra página de Facebook.


{{< img src="http://www.waifu.ca/static/media/img3.png" >}}


## ¡Pruébalo por ti mismo!



Usando tecnología de peerflix, popcorntime, nodewebkit... Te permite ver tus animes preferidos en HD y con subtitulos.


{{< img src="http://www.waifu.ca/static/media/img2.png" >}}


¡iYa no hay escusas para no ver tus series favoritas! :D
Waifu es una aplicación que nos permitirá ver un montón de animes en streaming tanto clásicos como recientes casi todas las series animes se ven con subtítulos en varios idiomas tan sólo tendremos que activar la opción de subtitulado y elegir el idioma que queramos además podemos cambiar el tamaño de las letras de sus tutelados Es una exelente herramienta de reproducción de anime la aplicación ofrece una calidad de 720p.




## Instalacion



Aca es donde entra nuestra hermosa comunidad que te da una instalacion facil, segura y rapida... si tenes nuestro {{< textlink url="/repositorio/" text="repositorio" >}} instalado (si no lo tenes no se que esperas) podes ir directamente a la terminal (siempre despues de un update) y instalarlo de la siguiente manera



    sudo apt install waifu




Te esperamos en nuestro gurpo para contarnos que te parece!!!

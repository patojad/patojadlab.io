---
title: "Dejemos morir los 32 Bits"
date: "2021-05-12 11:18:00.00"
description: "Opinion personal sobre distros 32 bits"
type: "linux"
tags: ["morir","linux","32","bits"]
category: ["Linux"]
img: "https://www.linuxadictos.com/wp-content/uploads/chip-32-64-bit.jpg"
authors: ["PatoJAD"]
---

Esta semana el video tiene que ver con una crítica a la comunidad en general por las malas costumbres que mantenemos en lo que a revivir equipos antiguos respecta.

Antes que nada quiero recalcar que esto es una opinión meramente personal y me gustaría que comenten su opinión sobre el tema.

## Video

Puede que antes de ver este video te interese ver {{< textlink url="/linux/2021/05/locos-la-nueva-distro-ligera/" text="LocOS" >}}

{{< youtube code="gGbBXrKPKbM" >}}

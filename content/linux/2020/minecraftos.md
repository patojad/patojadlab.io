---
title: "MinecraftOS"
date: "2020-08-13 09:42:00"
description: "Hoy vamos a hablar de la distribución de linux que busca... Que puedas jugar al Minecraft"
type: "linux"
tags: ["minecraft", "linux", "minecrafos", "gnu", "ubuntu", "raro"]
category: ["Linux"]
img: "https://pbs.twimg.com/profile_banners/888420023118827521/1500673899/600x200"
authors: ["PatoJAD"]
---



Ya con anterioridad hablamos de {{< textlink url="/linux/2020/06/distros-raras/" text="distros raras" >}} un post donde buscábamos entre lo más recóndito de linux distribuciones distribuciones con un origen un tanto polémicos u oscuros. Sin embargo, en el mundo linux existen distribuciones muy específicas, que cumplen solo una o dos funciones. Y en algunos casos podemos pensar que esto es… un tanto… inutil… Pero nunca está bien juzgar a un libro por su tapa así que hoy te invito a probar… MinecraftOS una distro para… jugar al minecraft.




## Sobre MinecraftOS



Antes de arrancar quiero aclarar que esta distribución es totalmente funcional, ya que es un **ubuntu** con un poco de tuneo. Sin embargo lo “gracioso” de esta distribución es el hecho de que fue pensada/creada solo para poder jugar al minecraft y además grabarlo con **OBS**.

Según su página, ellos responden la pregunta ¿Qué es minecraft OS? de la siguiente manera… MinecraftOS es un sistema operativo basado en Ubuntu. MinecraftOS correrá Minecraft mucho más rápido que cualquier otro SO.




## Requerimientos mínimos del sistema



* CPU de 64 bits funcionando a 1 GHz o más.
* Al menos 2 GB de RAM.
* Al menos 5 GB de almacenamiento.
* Buena tarjeta gráfica (para ejecutar Minecraft).
* Acceso a Internet.



**Detalle de color, me llama mucho la atencion que mencione “Buena Tarjeta Grafica” para ejecutar el Minecraft pero pida solo 2GB de ram**




## ¿Cómo instalar?



1. Descarga Minecraft OS desde la página de descarga.
2. Lleve una unidad flash USB con 2 GB o más.
3. Descargue Universal USB Installer desde aquí.
4. Inserte su unidad flash USB y abra Universal USB Installer.
5. En Universal USB Installer, haga clic en "Acepto". En el paso 1, desplácese hacia abajo y seleccione: "Pruebe Linux iso no listado".
6. En el paso 2, haga clic en "Examinar" y seleccione la ISO que descargó.
7. En el paso 3, seleccione su unidad flash USB y seleccione la casilla que dice "Formatear (nombre de su unidad)".
8. Haga clic en "Crear". **NOTA !!!!:** *Se eliminará todo el contenido de su unidad flash USB.*
9. Cuando el programa termine, haga clic en "Cerrar".
10. Asegúrese de que su computadora esté apagada e inserte su unidad flash USB (con el sistema operativo Minecraft) en ella.
11. Encienda su computadora e ingrese al menú de inicio (presione el botón del menú de inicio, puede ser: F11, F12 ...).
12. Seleccione su unidad flash USB (no seleccione la opción UEFI).
13. Haga clic en "En vivo".
14. Espere hasta que se inicie Minecraft OS.
15. Cuando se inicie el sistema operativo Minecraft, haga clic en el logotipo de Ubuntu en la pantalla, o el logotipo de Windows o la tecla Comando en su teclado.
16. Busque "Minecraft Launcher" (si desea jugar Minecraft) o busque OBS (si desea grabar o transmitir en vivo) o busque Chromium (si desea usar Internet).



P.D .: Puede arrastrar cualquiera de las aplicaciones a la base para acceder rápidamente a ellas la próxima vez que las necesite. 17. Disfruta. PD: Debido a que Minecraft OS 2.0.0 es solo un CD en vivo (debido a un error que se solucionará en la próxima versión), cuando apagues tu computadora, todo lo que instalaste en Minecraft OS se eliminará (no eliminará nada que hizo en un servidor de Minecraft).


{{< link url="https://minecraftos.tk/download/" text="Descargar" >}}



## Actualidad



Lamentablemente esta distro parece estar descontinuada ya que la versión 2.0.0 fue subida el 17.5.17 con la versión de Ubuntu 17.04. Sin embargo tenemos la ISO colgada sobre la cual pesan muchas actualizaciones pero se puede usar. Aunque en julio, en su twitter anunció la continuación de la distro y que la misma está preparando su versión 3.0.0 basada en Ubuntu 20.04



<blockquote class="twitter-tweet" data-theme="dark"><p lang="en" dir="ltr">The new Minecraft OS will be called Minecraft OS 3.0.0!</p>&mdash; Minecraft OS (@Minecraft__OS) <a href="https://twitter.com/Minecraft__OS/status/1256699168053133312?ref_src=twsrc%5Etfw">May 2, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

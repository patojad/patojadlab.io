---
title: "Review MilagrOS"
date: "2020-08-04 08:43:00"
description: "Ya la presentamos antes a esta hermosa distribución, hoy vamos a hablar de mi opinion personal"
type: "linux"
tags: ["review", "linux", "milagros", "mineros", "mx", "mxlinux", "lpi", "tictac"]
category: ["Linux"]
img: "https://proyectotictac.files.wordpress.com/2020/04/pantallazo-menu-milagros-2.0.png"
authors: ["PatoJAD"]
---



Ya con anterioridad hablamos de esta {{< textlink text="hermosa distribucion y mostramos el paso a paso de su instalación" url="/instalaciones/2020/06/instalcion-milagros/" >}}.  Hoy venimos a completar o a sumar un poco desde mi punto de vista persona comentando que pienso yo de esta distribución.




## Review



Recuerden que esto es meramente mi opinión y siempre trato de que sea constructiva para poder ver en que mejorarla desde mi punto de vista (puede ser válido o no conforme a lo que busque la distro)


{{< youtube code="FOTre6B8LUw" >}}


Desde ya quiero agradecer a LPI del {{< textlink text="Proyecto TicTac" url="https://proyectotictac.com/" >}} por incluir los {{< textlink text="repos de la comunidad" url="/repositorio/" >}}! Y por siempre dar una mano!



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

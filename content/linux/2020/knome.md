---
title: "Knome ¿Que es?"
date: "2020-04-01 08:10:00"
description: "Hace pocas horas tanto la pagina como de KDE nos informaron Knome un nuevo proyecto que viene a unir los 2 mundos"
type: "linux"
tags: ["kde","linux","gnome","knome"]
category: ["Linux"]
img: "https://knome.org/assets/img/knome.png"
authors: ["PatoJAD"]
---

Visto la reciente novedad podemos ver que KNOME, nombre que recive este proyecto es una de las mas grandes... BROMAS que nos pueden hacer estas dos hermosas comunidades.


{{< img src="https://i.postimg.cc/85fTDmgm/Deepin-Screenshot-Seleccionar-rea-20200401075931.png" >}}


##  Anuncios



>En asociación con nuestros amigos de KDE estamos encantados de embarcarnos en este nuevo esfuerzo. La configurabilidad y el uso de recursos de GNOME emparejados con la simplicidad y el enfoque de KDE - todo enrollado en un solo paquete. Presentamos KNOME, construido usando QTK3 y Kutter.
>
>{{< citaname name="GNOME" >}}




>GNOME y KDE profundizan su compromiso de trabajar juntos y revelar KNOME, un nuevo escritorio que trae a los usuarios lo mejor de ambos mundos:
>
>https://knome.org
>
>Aleix Pol, presidente de KDE e. V., dice: " Una conferencia conjunta fue sólo el comienzo. KNOME, QTK3, Lollyrok... las posibilidades de desarrollo compartido ahora son ilimitadas "
>
>El director ejecutivo de GNOME, Neil McGovern, dice: " Vemos que la fragmentación final de escritorio es la clave del éxito de Linux. Sentirnos unidos y completamente aburridos en estos tiempos difíciles también contribuyó a nuestra decisión de fusionar ambos proyectos."
>
>{{< citaname name="KDE" >}}


{{< img src="https://i.postimg.cc/fTfDwbpp/Deepin-Screenshot-Seleccionar-rea-20200401080349.png" >}}


## La Web



En la web se la pasan diciendo pavadas que para quienes no prestan atencion pasan desapersividas, mas si el manejo del ingles no es bueno. Pero tambien se dedican a hacer chistes para que entendamos que esto es irreal.


{{< img src="https://knome.org/assets/img/ram.svg" >}}


(Aunque algunos creemos que GNOME si cree que mas es mejor...) y para colmo no se olvidan de su bien amigos Windows, ¿porque no? tambien lo tienen presente...


{{< img src="https://i.postimg.cc/PqBBbG01/Deepin-Screenshot-Seleccionar-rea-20200401080817.png" >}}


Por ultimo no te olvides de pasar por https://knome.org/ para divertirte intentandolo descargar!!!

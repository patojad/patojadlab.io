---
title: "Artix Linux"
date: "2020-08-11 09:01:00"
description: "Una hermosa distribución basada en Arch Linux sin SystemD y Altamente ligera"
type: "linux"
tags: ["systemd", "linux", "artix", "gnu", "free", "openrc", "s6", "runit", "arch", "manjaro"]
category: ["Linux"]
img: "https://i.ytimg.com/vi/1mT8AEJs4II/maxresdefault.jpg"
authors: ["PatoJAD"]
---



En esta oportunidad quiero hablar de una distribución de GNU/Linux que muy probablemente no conozcas, no es nueva pero son pocos los usuarios que me dijeron conocerla cuando la nombre, y creo que esta distro debería ser tan popular como otras o tal vez más. Por eso hoy vamos a hablar de **Artix** y si dije bien no es *Antix*




## Sobre Artix



* **Tipo de sistema operativo:** Linux
* **Basado en:** Arch
* **Origen:** Global
* **Arquitectura:** x86_64
* **Escritorios disponibles:**
    - Cinnamon
    - LXDE
    - LXQt
    - MATE
    - KDE Plasma
    - Xfce
    - i3 (beta)
    - Gnome (beta)
* **Categoría:** Desktop, Live Medium
* **Estado:** Activo


{{< img src="https://i.ytimg.com/vi/lz8hsZjjNZY/maxresdefault.jpg">}}


Como remarca desde su web y así la identifica totalmente, **Artix Linux** es una distro basada en Arch, por tanto sabemos que es rolling release, **libre de systemd** y que reemplaza a este con **Openrc**, **s6** o **runit**. Artix es la continuación de los icónicos ManjaroOpenRC y ArchOpenRC tomando una camino más firme con desarrollo continuo.




## Va dirigida…



En muchos lugares lei que Artix va dirigida a usuarios con mucho expertis en el mundo de GNU/Linux. Sin embargo creo que esta a nivel de aquellos que usan manjaro dia a dia. con la leve diferencia que deben arreglárselas para instalar el **pamac** o el gestor gráfico de preferencia.


{{< img src="https://upload.wikimedia.org/wikipedia/commons/a/ab/Artix_Community_FF-Inkscape_2020-02.png">}}


Viene con una instalación mínima lo cual nos permite de alguna forma elegir a nosotros las apps a utilizar, esto nos permite tener el sistema limpio para no arrastrar actualizaciones de aplicaciones que nunca usamos.




## Instalación


{{< img src="https://i0.wp.com/entornosgnulinux.com/wp-content/uploads/2019/03/Calamarares.jpg" >}}


La instalación se realiza mediante gestor gráfico (si es que descargaste la iso con algun escritorio disponible) conocido como **calamares** uno de los más populares que existen hoy en dia.


{{< youtube code="MabkJ5JmWt0" >}}


Como la mayoría de las distros las instalaciones son una conjugación de siguientes y completar datos y ya podemos disfrutar de la distro. Te recomiendo descargarla y probarla, y si aceptas otro consejo recomiendo MATE como escritorio.


{{< link url="https://artixlinux.org/download.php" text="Descargar Artix" >}}


---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: "Comunidades Toxicas"
date: "2020-05-26 08:25:00"
description: "Nos tomamos a modo de broma lo que pasó en una comunidad para poder reflexionar sobre Toxicidad."
type: "linux"
tags: ["comunidad","linux","toxica","pedro","latin"]
category: ["Linux"]
img: "https://www.diariodecuyo.com.ar/export/sites/diariodecuyo/img/2019/11/19/toxicos.jpg_2063720366.jpg"
authors: ["PatoJAD"]
---

Las comunidades pueden ser **muy tóxicas**, por esto decidimos comentar algunos sucesos en esta publicación. Creemos que nuestra comunidad es tan amistosa que tiene un espacio hasta para nuestros heaters.


{{< youtube code="wo2isE4xstc" >}}


Tomando más en serio el problema creo que es **importante no quedarse callado** ante estos casos de injusticia, tomarlos, reclamarlos y generar un lugar seguro para usuarios nuevo o usuarios que no tengan muchas ganas de andar perdiendo el tiempo en peleas sin sentido. Es increíble ver cómo las **comunidades se desmoronan** la perder el norte y así se puede desmoronar el futuro de linux por las comunidades pésimas que existen.



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

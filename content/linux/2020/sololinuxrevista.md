---
title: "La Revista SoloLinux edicion 20"
date: "2020-10-01 11:24:00"
description: "La edicion de Septiembre de esta hermosa revista y la numero 20"
type: "linux"
tags: ["sololinux", "solo", "linux", "revista", "edicion", "20", "septiembre"]
category: ["Linux"]
img: "https://www.sololinux.es/wp-content/uploads/2020/10/REVISTA-SOLOLINUX-N20-SEPTIEMBRE-2020-730x430.png"
authors: ["PatoJAD"]
---



Es primero de mes, y aunque por ahi todavia no cobraste, solo Linux siempre te alegra el primero de cada mes con su magnífica revista. Ya van por la edición nro 20, quien diría que pasó ya tanto tiempo desde que inicio esta hermosa revista.


{{< img src="https://www.sololinux.es/wp-content/uploads/2020/10/REVISTA-SOLOLINUX-N20-SEPTIEMBRE-2020-730x430.png" >}}



## REVISTA SOLOLINUX N20 – SEPTIEMBRE 2020



> Parece que cada mes los días pasan más rápido, será porque nos vamos adaptando a la nueva situación mundial, no lo sé. Lo único que puedo afirmar, es que hoy día 1 de septiembre del 2020 sale el nuevo número de la revista, y ya es el 20.
>
> El proyecto ideado por Adrián se va consolidando y, esta locomotora sigue su marcha. Eso sí, poco a poco, con paciencia y buen hacer seguimos caminando. Hoy os presentamos la nueva revista SoloLinux, con la editorial de costumbre además de unas entrevistas realmente interesantes.



Como dije Septiembre nos trajo la edición número 20. Y se ve que para el Staff esto está pasando más rápido de lo que ellos quisieran pero esto no les impide traer nuevamente una revista con un contenido impecable como todas las anteriores.




## Descargar


{{< img src="https://www.sololinux.es/wp-content/uploads/2020/10/Revista-Digital-Magazine-N20.jpg" >}}


No te olvides de descargar este PDF y coleccionarlos, Leelo y disfruta de todo lo que SoloLinux tiene para dar!!



{{< link url="https://www.sololinux.es/revista/REVISTA_SOLOLINUX_N20_SEPTIEMBRE_2020.pdf" text="Descargar la revista" >}}



---




Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: 'Seguí en vivo la emisión del Maratón Linuxero'
date: '2019-04-20'
description: 'Se nos acerca el FLISOL 2019 y La gente del maraton linuxero les dedica un espacio y nos trae todos los adelantos!'
type: 'noticias'
tags: ["Linux","Comunidad","luto"]
category: ["noticias"]
img: 'https://i.postimg.cc/1RW03Vjm/head-pato.png'
authors: ["PatoJAD"]
---

Queremos acompañar al maratón y te damos la posibilidad de escucharlo directamente! No te pierdas ni un minuto de este gran evento!

<div class="text-center"><video class="maratonvideo" controls="" autoplay="" name="media"><source src="https://emision.maratonlinuxero.org:8443/audio-sd.mp3" type="audio/mpeg"></video></div>


> Quieres saber TODOS los detalles de nuestra emisión de @maratonlinuxero #FLISOL2019 que vamos a tener en vivo en poco más de 8 horas??? Te esperamos #SoftwareLibre #sorteos #CompartiendoLibertad
> 👉  https://hackmd.io/s/Hkq9qt_q4

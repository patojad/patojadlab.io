---
title: "Seminario gratuito sobre administrador Linux"
date: "2019-11-20"
description: "Te invitamos a participar el próximo martes 26 de noviembre de un Seminario Gratuito y presencial sobre las principales bases de los servidores de Linux y su interacción con el ambiente empresarial. Además, cómo se configura, instala y mantiene este tipo de servidores."
type: "linux"
tags: ["EducacionIT","Linux","Administrador"]
category: ["Linux"]
img: "https://www.redeszone.net/app/uploads/2019/02/Linux-en-servidores.jpg"
authors: ["PatoJAD"]
---

## Descripción



En este seminario te introduciremos en el mundo de Linux, contándote todas las posibilidades que ofrece este sistema operativo a nivel profesional y laboral. Veremos cuáles son las etapas que deben atravesarse para obtener los conocimientos necesarios para realizar la configuración, instalación y mantenimiento de servidores Linux, como así también cuáles son las soluciones más utilizadas junto con las tecnologías que manejan las empresas en la actualidad.




## Tópicos a Cubrir



* ¡Linux importa!
  * Por su historia
  * Por su contribución tecnológica
  * Por su rol
* Linux en tu carrera profesional
  * En pequeñas y medianas empresas
  * En grandes empresas
  * En empresas corporativas
* Soluciones Linux
  * Linux en todos lados
  * Servicios Web con Linux
  * Virtualizacion y Nube con Linux
* ¡Wow Linux! Mirá a Linux en acción
* Roles de un Experto en Linux
* LPI la carrera hacia la certificación


{{< img src="https://www.educacionit.com/images/seminarios2/seminario-de-linux.jpg" >}}


## Modalidad



* Martes 26 NOV 2019
* Seminario Gnu/Linux Presencial
* Horario: 19:00 a 21:00 hs
* Costo: GRATIS




## A quien va dirigido



El seminario esta orientado a todos aquellos que no poseen conocimientos en Linux, y estén interesados en convertirse en expertos en este sistema operativo.




{{< link url="https://www.educacionit.com/seminario-de-linux" text="Inscribirme" >}}

---
title: 'Gnome quiere renovarse para revivir'
date: '2018-12-17'
description: 'Gnome quiere revivir y para eso pone todo su empeño en renovarse'
type: 'linux'
tags: ["Gnome","GNU/Linux","renovarse"]
category: ["linux"]
img: 'https://i.postimg.cc/0ytMkCFk/login-screen-gnome-redesign.jpg'
authors: ["PatoJAD"]
---

GNOME es, en mi punto de vista uno de los Desktops mas desactualizados y mas próximos a morir que existe, hace tiempo no sorprende para bien a nadie, solo se dedica a volverse mas lento y pesado. Sin embargo parece que sus nuevas ideas (Y por favor mantengamos esa palabra durante toda la publicación) son darnos un nuevo estilo para volver a enamorarnos y poder persistir mas tiempo en este mundo que avanza a zancos enormes.

Actualmente la idea consiste en darle una aparienciaa distinta al escritorio y quiero recalcar que lo que vemos aqui son solo ideas del equipo y ninguna fue tomada como definitiva aun.

Nuestro buen amigo BabyDora ha estado ocupado manteniendo a sus seguidores de Twitter al tanto de todos los últimos cambios en el tema de Adwaita GTK, ya que han llegado a las respectivas ramas nocturnas de GNOME.

Las ramas nocturnas, para aquellos que se preguntan, son las que permiten el desarrollo de GNOME de última generación. Gracias a Flatpak, es relativamente trivial que alguien juegue con estos sistemas basados en algo estable como Ubuntu o Fedora sin poner en peligro su sistema. Todo lo que necesitas son los tiempos de ejecución correctos y los repositorios configurados.

Según nuestros colegas de _maslinux_ la modificación se puede definir así:

> La actualización de Adwaita que se está probando actualmente parece una mezcla de Adwaita tal y como la conocemos (espaciosa, fría, funcional) y diseño Materia, pero con un guiño a macOS (degradados sutiles, esquinas redondeadas, botones biselados) e iOS (interruptores planos).

{{< img src="https://i.postimg.cc/SK3n8xmH/new-gnome-theme-gnome-software.jpg" >}}
<br>
{{< img src="https://i.postimg.cc/vTdDq2gz/new-gnome-flatpak-permission-controls.jpg" >}}
<br>
{{< img src="https://i.postimg.cc/sx9xtP5R/new-gnome-theme-2.jpg" >}}
<br>
{{< img src="https://i.postimg.cc/MK5X5DW7/adwaita-refresh.jpg" >}}

Los fans de los temas oscuros tampoco deben entrar en pánico. La actualización de Adwaita tiene una variante oscura que sigue los mismos cambios:

{{< img src="https://i.postimg.cc/N09MgvRD/new-gnome-theme-dark-mode.jpg" >}}

Nuevos iconos de GNOME:

{{< img src="https://i.postimg.cc/G3Jp3QTW/new-gnome-icons-1.jpg" >}}

Así como una actualización para Adwaita, los diseñadores de GNOME están trabajando en un conjunto de íconos renovados. Hay algunos cambios importantes desde la última vez que te mostramos los iconos, incluyendo nuevos íconos de carpetas para Nautilus.

En otros lugares:

{{< img src="https://maslinux.es/wp-content/uploads/2018/12/login-screen-gnome-redesign.jpg" >}}

Aparte del tema e íconos de Adwaita, los diseñadores de GNOME también están trabajando duro para mejorar otras áreas del shell del escritorio, como la pantalla de inicio de sesión, mejorar los diseños de las barras de encabezado, implementar permisos de aplicación de Flatpak en los ajustes, y un montón mas.

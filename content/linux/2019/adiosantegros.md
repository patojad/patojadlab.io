---
title: "Antegros se despide de sus usuarios"
date: "2019-05-21"
description: "Esta querida distribucion presento hoy su carta de despedida"
type: "linux"
tags: ["Adios","Antegros","Despedida"]
category: ["Linux"]
img:  "https://jugandoenlinux.com/media/k2/items/cache/e7b279be6a862d254f0e7cc4dde2874e_XL.jpg"
authors: ["PatoJAD"]
---
Hoy encontramos la siguiente publicacion en el sitio oficial de Antegros, donde ellos por motivos personales deciden despedirse del proyecto. Antegros es una distribucion con mucho trabajo y muy utilizada y para todos nosotros incluso los que no eramos usuarios cotidianos de esta distribucion es dificil procesar esta noticia teniendo en cuenta que el pasado 4 de abril sacaron una iso refresh con muchas correcciones!

>Lo que comenzó como un pasatiempo de verano hace siete años se convirtió rápidamente en una increíble distribución de Linux con una comunidad aún más impresionante a su alrededor. Nuestro objetivo era hacer que Arch Linux estuviera disponible para una audiencia más amplia de usuarios al proporcionar una experiencia simplificada y fácil de usar que incluye un lugar seguro para que los usuarios se comuniquen, aprendan y se ayuden entre sí. Se han realizado 931,439 descargas únicas de Antergos desde 2014 (cuando comenzamos a hacer un seguimiento).
>
>Creemos que es seguro decir que hemos logrado nuestro objetivo. Hoy estamos anunciando el final de este proyecto. Como muchos de ustedes probablemente notaron en los últimos meses, ya no tenemos suficiente tiempo libre para mantener adecuadamente los Antergos. Tomamos esta decisión porque creemos que seguir descuidando el proyecto sería un gran perjuicio para la comunidad. Tomar esta acción ahora, mientras el código del proyecto aún funciona, brinda una oportunidad para que los desarrolladores interesados ​​tomen lo que encuentren útil y comiencen sus propios proyectos.
>
>Para los usuarios de Antergos existentes: no hay necesidad de preocuparse por los sistemas instalados, ya que continuarán recibiendo actualizaciones directamente desde Arch. Pronto, lanzaremos una actualización que eliminará los repositorios de Antergos de su sistema junto con cualquier paquete específico de Antergos que ya no tenga un propósito debido a la finalización del proyecto. Una vez que se haya completado, todos los paquetes instalados desde el repositorio de Antergos que están en el AUR comenzarán a recibir actualizaciones desde allí.
>
>El Foro de Antergos y la Wiki seguirán estando disponibles hasta que quede claro que los usuarios se han trasladado a otros proyectos. Sin embargo, no anticipamos mantener el foro y la wiki operativos durante más de tres meses.
>
>Queremos agradecerles por todo su apoyo a lo largo de los años. Mientras trabajamos en este proyecto, hemos aprendido muchas habilidades invaluables que han mejorado enormemente nuestra vida personal y profesional. Por eso siempre estaremos agradecidos.
>
>
>{{< citaname name="-Dustin, Alex, & Gustau" >}}

Podes ver el texto original sin errores de traduccion en su {{< textlink text="pagina oficial" url="https://antergos.com/blog/antergos-linux-project-ends/" >}}

---
title: "¿Se puede ganar dinero con el Software Libre?"
date: "2019-01-03"
description: "SO a la carta"
type: "linux"
tags: ["Libertad","Dinero","Stallman"]
category: ["Linux"]
img:  "https://i.postimg.cc/TwxJm0dz/rmsdollar.jpg"
authors: ["PatoJAD"]
---

Hace un tiempo se armó un debate sobre el Software Libre y el privativo en un entorno de Desarrolladores. El fundamento mas pesado en el apoyo al software privativo se basa en _“los programadores tenemos que comer”_ lo cual es una realidad, y ahi me di cuenta que en el fondo, siempre me pesó esa inquietud. Y decidí decantar por completo. ¿Yo solo sería un Usuario de Software Libre, o también sería un Desarrollador de Software Libre?.

La respuesta, de tinte moral, no tardó en llegar, Yo estoy seguro de lo que quiero ser, pero también se que mi familia tiene que comer y eso en el inicio de mi búsqueda parecían dos realidades diferentes.

Quiero aclarar en este punto que no solo fue googlear el título de esta publicación, también decidí hablar con gente que vive con Software Libre y no saben cómo se puede sorprender Uno cuando camina por esos prados.

# ¿Está bien que el software Libre Gane Dinero?
Esta fue una de las primeras preguntas con la que me crucé y me sorprendió. Pero son muchos los que creen que no debería ganar dinero. A lo que quiero poner una linda comparación con la comida al mejor estilo Sergio. Supongamos que no sabes hacer un sándwich de lomito ahumado y tienes la necesidad o ganas de comer uno. Tienes dos opciones ir a un lugar donde te venden un empaque cerrado al vacío con la comida dentro y la puedes comer o ir a un lugar donde te muestran como lo hacen. Yo en lo personal prefiero ver que tiene mi comida y no que sea   _“a la buena de Dios”_.  El Software Libre es ese sándwich en el cual podés ver todo, que tiene, como esta hecho, y que hace ahi… ¿Pueden imaginarse las ventajas que tiene esto?. ¿Se imaginan hablando de robar información o datos de personas?. Es casi imposible esconder esas cosas si puedo saber todo lo que hace tu programa.

### Las Donaciones
Al llegar a este punto podemos creer o concordar que el Software Libre es quien tiene mejor merecido su pago. La duda es ahora: ¿De dónde sale?. Lamentablemente la gente no valora lo suficiente al Software Libre como para que las donaciones puedan mantener una empresa o un equipo de desarrollo, incluso es difícil que mantengan una Persona correctamente. Sin embargo, hay grandes proyectos que pueden seguir mediante donaciones, y estas, forman una parte importante en el inicio y la sustentación de los proyectos por las cuales es uno de los primeros recursos de los Desarrolladores de Software Libre.

### Cursos y Asesorías
Hoy en día es común escuchar que muchas empresas migren de MS Windows o de algunos de sus servicios (Office, server, etc.) como una forma de implementar un menor gasto. Sin embargo, esto requiere constantes capacitaciones en Software Libre (Libre Office, por ejemplo) y este es un área que puede ser altamente explotada por los Desarrolladores. ¿Quiénes saben cómo usar un programa mejor que aquellos que lo hicieron?.

### Soporte Técnico y Personalización
Volviendo al caso anterior también muchas empresas se migran completamente y Nosotros, como grandes conocedores estamos capacitados a dar un buen soporte técnico (seguramente ayudados de algún foro o comunidad) y ese terreno no solo nos va a permitir trabajar con Software Libre, también nos permite mejorarlo con reportes y obtener un ingreso fijo. Además de esto muchas empresas quieren un nivel de personalización mucho mayor y eso nos abre un gran terreno para ganar dinero con el Software Libre.

### Publicidad y Patrocinio
Aqui simplemente no logré hacerme uno con el tema, pero se, que por ejemplo, Mozilla pudo y recibió grandes ingresos de su competencia Google por incluirlo como navegador predeterminado. Esto hizo que pueda florecer en el mercado y a los Usuarios finales no les presento gran cambio o molestar porque puede cambiarse el buscador por defecto.

### Ingresos directos
Por último, la salida que muchos Programadores tomamos como tiempo libre Freelance. No solo vas a poder resolver problemas en eventos esporádicos por dinero, sino que también podrás dejarlo documentado y demostrar tu conocimiento. ¡Y todo con Software Libre!.

Desde mi punto de vista tenemos mucho por hacer con Software Libre y mucho Software Libre por hacer. No caigas en privatizar tu código, lo único que harás es que la comunidad no pueda mejorarlo y te quedes acotado a tus ideas.




# FAQ


> Estoy trabajando hace años en programación para una empresa pero quiero ser Freelancer con Software Libre.

Bien, en estos casos hay que iniciar la _“migración”_ lentamente (teniendo en cuenta que no se tenga la posibilidad de una desestabilización laboral temporaria). En su mayoría es un proceso muy cansador dado que adicional al trabajo, se tienen que tomar pequeños proyectos Freelance para poder acostumbrarse y abrirse al mercado. Si bien es un proceso cansador a medida que más energía se le implementa, más simple y rápido pasara la transición permitiendo en pocos meses trabajar por cuenta propia.



> Recién me inicio, me interesa el Software Libre pero trabajo para una compañía privativa.

Este caso es muy similar al anterior, pero se cuenta con menos experiencia en proyectos, por lo cual se tiende a demorar mas tiempo en lograr independizarse. Se debe saber también que muchas veces se puede migrar la empresa lentamente al uso de Software Libre. Si bien el fin no es el mismo, esto ayuda a reacomodarse para el día de mañana lograr desempeñarse como Freelancer sin tener que adaptarse a todo y teniendo una experiencia que es muy útil.



> Soy alguien que quiere iniciarse en la programación para luego que sea mi fuente de ingreso cuál sería el camino más adecuado.

Este es uno de los mejores casos dado que se tiene un gran camino y se inicia desde cero como Freelancer. Sin embargo se tiene una contra que suele notarse en algunos casos mas que en otros. La falta de experiencia y portfolio, obliga inicialmente a ser mal pagado o a tomar trabajos menos emocionantes. También se dependerá de los conocimientos que se tengan en áreas como programación o sistemas, de la habilidad para vender la propia fuerza de trabajo y la capacidad para interactuar con el cliente. Es el desafío más grande pero una vez que se logra el movimiento continuo será mucho más simple y se tendrá trabajo constantemente.

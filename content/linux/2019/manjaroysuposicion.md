---
title: "Manjaro y su Posición"
date: "2019-08-01"
description: "Manjaro sorprendió con su decisión, ahora interpretemos su objetivo..."
type: "linux"
tags: ["Libertad","GNU","Stallman"]
category: ["Linux"]
img: "https://i.blogs.es/02d4c6/manjaro-linux/1366_2000.jpg"
authors: ["PatoJAD"]
---

En estos días todos nos enteramos que Manjaro tomo la decisión de modificar su sistema de ofimática por defecto de LibreOffice a FreeOffice.



Lo primero que tenemos que entender es que Manjaro es la versión User Friendly de Arch (pese a quien le pese) y como tal sus decisiones las toma 100% pensando en lo mejor para la usabilidad y muchas veces esto nos duele a los usuarios que llevamos la bandera del Software libre!



{{< img src="https://i.blogs.es/1b2460/manjaro-linux-freeoffice/1366_2000.png" >}}



Si bien muchos preferimos el software libre hay que remarcar que la decisión que tomo manjero es la mejor implementa su eslogan “Enjoy the Simplicity” por lo cual quienes critiquen esta decisión es porque prácticamente no comprendieron la orientación de Manjaro.



 



Una realidad es que todos los que preferimos el software 100% libre sabemos, o tenemos una noción de como instalar libre office (O al menos las ganas suficientes de buscarlo en internet) como para cumplir nuestras necesidades. Sin embargo, el usuario que se instala manjaro quiere tener un sistema que se encuentre funcionando y sea compatible con el sistema del que viene (en su mayoría Windows) y por esto es que la decisión está bien encaminada a su fin. Aun así, todos los usuarios que están en desacuerdo tienen la posibilidad de instalarlo y sacar todo eso que les molesta. Entiendo que el espíritu de Linux y de los usuarios (Tema que hable en {{< textlink url="https://patojad.com.ar/noticias/elroldelusuario/" text="esta publicación">}}) no se basa en criticar una decisión, podemos no estar de acuerdo, pero la realidad es que su decisión sigue su rumbo y no debería ser tan crítica, aunque signifique una leve perdida para el Software Libre.

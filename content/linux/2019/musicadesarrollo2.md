---
title: 'Música y Desarrollo 2'
date: '2019-03-25'
description: 'Música Desarrollo'
type: 'linux'
tags: ["Música","Desarrollo","Programación","GNU/Linux"]
category: ["Linux"]
img: 'https://i.postimg.cc/xCG6TDX9/musicadesarrollo2-svg.png'
authors: ["tanomarcelo"]
---
_________________________________________________________________________
## Sonidos y Código.
##### Sonidos.

Veníamos de [aquí](https://patojad.com.ar/linux/musicadesarrollo/).
Había quedado como tema de continuación, la reproducción de audio en entornos GNU/Linux

Lo que aqui se observe, es *mi caso de uso*. Y nada mas que eso.

Reproducción Multimedia en GNU/Linux
=====================================
Hay una lista soberbia de programas dedicados a tal fin, con interface gráfica algunos, para interface de línea de comandos otros.

*VLC Media PLayer* es un verdadero animal reproductor a la hora de guapear diretes sonoros. Conozco poco prontuario, pero tampoco es mi única opinión, este *gigante* libre como el viento y de código abierto tira fuego por el caño de escape sin necesidad de instalar códecs y reproduce un listado de formatos que de poco nada tiene, lleva licencia *GPLv2.1*, puede correr sobre *GNU/Linux*, *Windows*, *macOS*, Flia. *BSD*, *Solaris*, *Android*, ¡ Ostia !, falta instalarlo en un reloj a cuerda y estamos todos.

{{< img src="https://i.postimg.cc/2ybgwjTr/vlc-void.png" >}}

Este Pibe camina bien en serio.

La *lista soberbia* de reproductores en GNU/Linux no se reduce a la friolera suma de un solo reproductor. Es el único que conozco.
No es que haya sido dura mi vida, me quedo con lo que funciona bien. Tampoco hay mas.

#### Los *otros* ...
... que no tienen interface gráfica y que conozoco, son:

>
1. mpd ncmpcpp
2. moc
3. mpv
>

#### <p style="text-align: center;"> 1. *mpd ncmpcpp*</p>
*mpd* es el servidor de audio, que si bien no será *VLC Media Player*, tengo que admitir que se defiende como gato panza arriba. Soporta formatos tales como *Ogg Vorbix*, *FLAC*, *Opus*, *WavPack*, *MP2*, *MP3*, *MP4*, *AAC*, puede transmitir sonido via streamming usando el protocolo HTTP, o en otras palabras, se puede gestionar el servidor de sonido a través de la red. <br> *ncmpcpp* es el cliente del servidor anterior. Esto quiere decir que una vez ejecutado el motor de audio *(mpd)*, con escribir el comando *ncmpcpp* en la terminal, ya tendríamos el reproductor corriendo, con lo que se vería algo parecido a esto:

{{< img src="https://i.postimg.cc/DZ8BLjyX/2019-02-14-20-17-14-1280x720.png">}}

#### <p style="text-align: center;"> 2. *moc*</p>
Es un clasicazo de esos que uno lleva clavado en el pecho. <br>Juntos atravesamos épocas y devenires. Le guardo un cariño de la ostia. <br>Es un fierrazo, incolgable y pesa menos que un saquito de te. Lo cito aqui con captureta incluída, ya que nobleza obliga.

{{< img src="https://i.postimg.cc/yNWFbJ4q/2019-03-26-17-40-22-1280x720.png">}}

#### <p style="text-align: center;"> 3. *mpv*</p>

Sería lógico pensar que luego de mencionar a *VLC Media Player*, es un acto osado citar a un reproductor de video y sonido como *mpv*.

Es aqui donde rompo una lanza en favor de este último. <br>
Que no tenga una interface gráfica por defecto acorde a la ceremonia *VLC* no significa que haya que dejarlo de lado, No Señor. Es tan potente como el mencionado rey del trono en esta entrada, pero un quilombo peliagudo manejarlo hasta que no se dominan los modificadores que hay que escribir en la terminal para que empiece a mostrar de una vez por todas el bendecido video en cuestión.

Desde que aprendí a manejarlo, mi vida sigue igual, a que negarlo, pero anda como los dioses de bien (que no es poco) y lo declaro fiel ladero del aqui escribiente.

{{< img src="https://i.postimg.cc/YSqHKHbB/mpv-void.png">}}

He mencionado cuatro excelentes reproductores multimedia, *Buenos Muchachos* todos.<br>
Una reseña breve de mi paso por este mundo escuchando cosas que canta el Querido Pingüino.

Antes que me olvide
==================

[Aqui](https://t.me/comunidadsoftwarelibre) nos juntamos cuando tenemos tiempo, charlamos sobre la vida, las cacharras y algún que otro detalle mientras escuchamos la música que a *caduno* le gusta usando por supuesto, el Reproductor Multimedia que se tenga instalado.

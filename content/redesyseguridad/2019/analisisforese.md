---
title: 'Análisis forense en Sistemas de Control Industrial'
date: '2019-11-28'
description: 'En esta guía se recogen las técnicas requeridas para llevar a cabo un análisis forense en los entornos industriales, planteando las diferentes fases del análisis, siempre teniendo en cuenta sus características especiales.'
type: 'redesyseguridad'
tags: ["Analisis","Forense","Guia", "incibe-cert","Seguridad"]
category: ["redesyseguridad"]
img: 'https://i.postimg.cc/Nf0TpSjK/audit-4189560-1280.jpg'
authors: ["PatoJAD"]
---

La gente de incibe-cert nos facilita en este caso una perfecta guia sobre analisis forense que vale la pena tener en cuenta. Es de una simple lectura y su contenido es muy amplio y explisito.



Se proporcionan unos conocimientos básicos que sirvan de punto de partida para despertar la curiosidad y el interés, ofreciendo unas nociones básicas de cómo abordar ciertos problemas. El principal objetivo de concienciar sobre determinadas técnicas de análisis forense que, tradicionalmente, se han aplicado en entornos corporativos y que se están empezando a aplicar en el ámbito de los sistemas de control.


 


## El documento se divide en:



* Introducción al mundo del análisis forense y a los últimos incidentes detectados a nivel industrial.
* Descripción del modelado de ataques en SCI.
* Repaso al análisis forense en SCI.
* Herramientas para llevar a cabo el análisis forense en redes OT.
* Caso práctico de análisis forense.
* Conclusiones.



{{< link url="https://www.incibe-cert.es/sites/default/files/contenidos/guias/doc/incibe_guia_analisis_forense_sci.pdf" text="Descargar Guia" >}}

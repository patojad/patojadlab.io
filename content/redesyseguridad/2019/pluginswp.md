---
title: 'Cuidado con los plugins y temas gratuitos de WORDPRESS'
date: '2019-11-07'
description: 'Al parecer existen diversas páginas que ofrecen versiones de plugins y temas de WordPress que podrían comprometer el sitio web donde hayan sido instalados.'
type: 'redesyseguridad'
tags: ["Plugins","Worpress","Seguridad"]
category: ["redesyseguridad"]
img: 'https://noticiasseguridad.com/nsnews_u/2017/05/Wordpress-1.jpg'
authors: ["PatoJAD"]
---

WordPress es, probablemente, el más popular sistema de gestión de contenido (CMS) de la actualidad, por lo que no es extraño que también sea objeto de múltiples amenazas de ciberseguridad. A mayor cantidad de usuarios mas probable es que alguien quiera victimizarlos.


{{< img src="https://s.w.org/images/mobile/devices.png?1" >}}


## ¿Como funciona?



Un reporte publicado por la plataforma especializada ZDNet aporta amplios detalles acerca de esta campaña de ataques, abordando un tema con especial interés: el hecho de que estos hackers no explotan vulnerabilidades para infiltrarse en los sitios comprometidos e instalar backdoors, sino que usan versiones piratas de temas y plugins, por lo que sólo deben esperar a que un administrador de sitios web descargue e instale el software infectado.


{{< img src="https://uncomocorreo.com/wp-content/uploads/2018/07/wordpress-hackeado.png" >}}


Los expertos en ciberseguridad detectaron múltiples indicios de actividad de estos hackers en sitios web fraudulentos, que ofrecen versiones pirateadas de plugins y temas de WordPress de paga. Además, todos estos sitios maliciosos cuentan con buenas clasificaciones en los resultados de búsqueda debido a que reciben impulso de palabras clave de todos los sitios de WordPress que ya hayan sido hackeados, reportan los expertos en ciberseguridad, por lo que es realmente fácil que un usuario encuentre este software malicioso.



## Los sitios donde se detectó esta actividad maliciosa son:



* www.download-freethemes.download
* www.downloadfreethemes.co
* www.downloadfreethemes.space
* www.downloadnulled.pw
* www.downloadnulled.top
* www.freenulled.top
* www.nulledzip.download
* www.themesfreedownload.net
* www.themesfreedownload.top
* www.vestathemes.com


 


Después de que los administradores de sitios web descargan cualquiera de los plugins o temas infectados, pasan sólo unos cuantos segundos para que su sitio de WordPress sea completamente comprometido. La descarga de estos componentes agrega al sitio objetivo un backdoor identificado como ‘100010010’, lo que garantiza que los hackers tienen una forma de acceder a la instalación.

Posteriormente, el malware WP-VCD se agrega a todos los temas usados en el sitio, para evitar que éste desaparezca del sistema debido a una posible desinstalación. Finalmente, si el malware actúa en un entorno de alojamiento compartido, se pude propagar a otros servidores, infectando otros sitios alojados en el mismo sistema.



## Conclusion



Es importante entender que la piratería no solo daña a los desarrolladores de software sino también a los usuarios con la posible inserción de código malicioso.

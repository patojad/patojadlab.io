---
title: '¿Usas LastPass? Más vale Actualizarlo'
date: '2019-09-17'
description: 'Se ha detectado una vulnerabilidad en el gestor de contraseñas LastPass. Al parecer el fallo estaba provocado por un error en la actualización de la caché que afectaba a la extensión de la aplicación para los navegadores.'
type: 'redesyseguridad'
tags: ["Firefox","LastPass","Error"]
category: ["redesyseguridad"]
img: 'https://www.redeszone.net/app/uploads-redeszone.net/2019/09/vulnerabilidad-en-lastpass-930x487.jpg'
authors: ["PatoJAD"]
---

>Las contraseñas representan una barrera fundamental para proteger nuestras cuentas y registros. Tener claves fuertes y complejas puede evitar la entrada de intrusos. Ahora bien, hacer uso de gestores de contraseñas son herramientas interesantes para administrar las claves y también para generarlas. Hoy nos hacemos eco de una vulnerabilidad que afecta a LastPass y que pone en riesgo las contraseñas de los usuarios.


{{< img src="https://sophosnews.files.wordpress.com/2016/07/lp-1200.png?w=780&h=408&crop=1" >}}


El error se basaba en la ejecución de código JavaScript malicioso, algo que puede ser potencialmente explotable y representar una importante amenaza para los usuarios. Según las fuentes consultadas LastPass podría filtrar las últimas contraseñas y credenciales utilizadas por el usuario. Lo cual representa una vulnerabilidad grabe en cara al usuario. Todas nuestras pass podrían ser filtradas y expuestas con facilidad, sin embargo, tenemos una solución para esto.


 


## La importancia de Actualizar

El error ya ha sido solucionado en la versión 4.33.0 de LastPass por lo que se recomienda encarecidamente actualizar a dicha versión. Por lo cual, como decimos siempre, es imprescindible tener al día las actualizaciones de seguridad. De esta forma siempre estaremos lo más seguro dentro de lo posible. Como en este caso que debemos tener la última versión.

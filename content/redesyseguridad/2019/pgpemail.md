---
title: 'Cifrado seguro de correo electrónico con PGP'
date: '2019-12-19'
description: 'En esta guía se pretende mostrar cómo mantener la confidencialidad, integridad y autenticidad en las comunicaciones por medio del correo electrónico.'
type: 'redesyseguridad'
tags: ["PGP","INCIBE-CERT","Email"]
category: ["redesyseguridad"]
img: 'https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fblog.securegroup.com%2Fhubfs%2FSECURE_GROUP%2FBlog%2FPost_Images%2FPGP_encryption_for_email_security.jpg%23keepProtocol&f=1&nofb=1'
authors: ["PatoJAD"]
---

Nuestros compañeros de INCIBE-CERT nos ofrecen un manual con un gran contenido y nosotros deseamos acercártelo para que puedas disfrutarlo.



## Cifrado seguro de correo electrónico con PGP


{{< img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.shortpixel.ai%2Fclient%2Fq_glossy%2Cret_img%2Cw_660%2Fhttps%3A%2F%2Fbontekoe.technology%2Fwp-content%2Fuploads%2F2019%2F05%2F101535064_gettyimages-638879324.jpg&f=1&nofb=1" >}}


El estándar OpenPGP tiene como principales objetivos la comprobación de la integridad y autenticidad de los correos electrónicos, así como el cifrado del contenido del correo.



A lo largo de esta guía se describen los procesos de firma y verificación de correos, cifrado y descifrado tanto de correos como de ficheros adjuntos, tanto para el cliente Outlook como para Thunderbird. A modo de ejemplo y para ilustrar esta guía, se lleva a cabo un proceso detallado utilizando los certificados que proporciona INCIBE-CERT.


{{< link text="Descarga" url="https://www.incibe-cert.es/sites/default/files/contenidos/guias/doc/incibe_cert_guia_para_el_uso_de_pgp_en_clientes_de_correo_electronico.pdf" >}}

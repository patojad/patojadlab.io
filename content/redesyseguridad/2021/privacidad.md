---
title: 'Privacidad ¿Cuál es el verdadero problema?'
date: '2021-01-12 08:51:00'
description: '¿Que es el cibercrimen?¿Como funciona? Todo lo que tenes que saber'
tags: ["facebook", "whatsapp", "datos", "privacidad", "seguridad", "analisis", "opinion"]
type: 'redesyseguridad'
category: ["redesyseguridad"]
img: 'https://www.milenio.com/uploads/media/2021/01/06/politicas-whatsapp-desatan-memes-redes.jpg'
authors: ["PatoJAD"]
---



La noticia reciente sobre los cambios de políticas de whatsapp que nos obligan a confirmar el cruce de datos para mantener la aplicación activa generó una gran repercusión en general. En esta oportunidad voy a utilizar como disparador este evento para indagar un poco dónde está realmente el problema de privacidad.




## Video



Como dicho tema se volvió extenso para escribir tomé la decisión de plasmarlo en un video donde tocaré el tema desde mi experiencia y punto de vista.


{{< youtube code="5Gcq9q4nu-I" >}}



## ¿Conclusión?



Desde mi perspectiva creo que el problema radica en que esos datos existan, no solo en el hecho de que vayan a juntarse para obtener información más precisa de nosotros, el problema siempre parte desde el punto de que la información existe, y ellos la guardan y como tal, cada vez que obtienen un poco mas de informacion nos hacen un poco mas vulnerables.



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

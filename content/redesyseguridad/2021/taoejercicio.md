---
title: '¿Te gusta la seguridad Informática? No te podes perder este evento'
date: '2021-09-10 08:25:00'
description: 'En este curso aprenderás Seguridad Informática desde Cero'
tags: ["evento", "seguridad", "informatica", "hacking", "tao", "ciberseguridad", "simulacion", "incidentes"]
type: 'redesyseguridad'
category: ["redesyseguridad"]
img: 'https://is4-ssl.mzstatic.com/image/thumb/Podcasts115/v4/e7/fc/9e/e7fc9e78-fb0b-743b-8b76-66772888a6fb/mza_4442571569969948978.jpg/1200x1200bb.jpg'
authors: ["PatoJAD"]
---

A lo largo de mi carrera académica, tuve la oportunidad de conocer muchos iconos de la tecnología y con ellos muchos proyectos dignos de compartir día a día. **El Tao del Hacking** es un gran proyecto del que he hablado mucho en Telegram pero nunca le dediqué una publicación… Hoy, no voy a hablar precisamente de este proyecto pero estoy seguro que si te gusta la seguridad no te podes perder este gran evento que viene de la mano del mismo...

{{< img src="https://i.postimg.cc/tJ6sgXdT/Bb9dg1-WN-400x400.webp" >}}

En esta oportunidad nos ofrecen un gran evento que está destinado para todos aquellos que ya están metidos en el mundo o al menos tienen algunos conocimientos. Sin embargo, siempre intentando llevar la seguridad a todos y cada uno de nosotros para aquellos que no nos animemos a participar del evento activamente nos invitan a verlo en vivo por Twitch para conocer un poco más esto, y porque no prepararnos para el próximo que hagan...

{{< info text="Por primera vez estamos abriendo este juego a gente externa de ciertos entornos que no sean ni alumnos ni clientes, y por eso los invitamos como parte del grupo El Tao del Hacking 💪" >}}
<br/>

{{<tgcita post="ElTaodelHackingComunidad/11724" color="F7B100" >}}

De esta forma el gran **Fede Pacheco** nos presenta este gran evento que trae la gran oportunidad del conocimiento y es, según lo que vi en mi búsqueda de más información, un evento muy entretenido y con mucho para dar a los participantes y espectadores.


## Inscripción

{{< warning text="Para jugador tiene que tener algo de experiencia en seguridad. En caso de no tener experiencia se puede ver en vivo para ponerse en tema y prepararse para el siguiente evento." >}}

Esta invitación es una extensión de la que realizaron en el grupo. Recordamos que este evento se está organizando desde Argentina por lo cual el horario anunciado es en dicho país. Sin embargo, como la comunidad de PatoJAD es muy mixta te vamos a poner un hermoso timer que te diga cuánto falta independientemente del país donde estés...

* Formato Virtual
* Discord
* Sábado 18 de Septiembre de 15 a 18 hs (ARG)

Tiempo para el inicio...

{{< countdown dia="18" mes="09" anio="2021" hora="18" minuto="00" >}}

Vamos a estar apoyando el evento seguramente y viéndolos juntos hosteado en nuestro twitch y cuando dispongamos más información seguramente avisaremos en nuestras redes sociales.

{{< info text="Si nunca participaron en algo parecido no se preocupen que vamos a enviarles información sobre el juego a todos los que se anoten." >}}

{{< link url="https://forms.gle/1H8fXEuY7RmUnLTy8" text="Formulario de Inscripción" >}}

---
title: 'Guias de uso seguro para Android  y iOS'
date: '2020-03-27 09:21:00'
description: 'CSIRT-CV ha publicado en su portal de concienciación concienciaT, una nueva versión de las guías de "Uso seguro de Android" y "Uso seguro de iOS"'
type: 'redesyseguridad'
tags: ["http","red","internet","protocolo","educacionit"]
category: ["redesyseguridad"]
img: 'https://claroblog.com.pa/wp-content/uploads/2014/12/smartphone-mobile-security.jpeg'
authors: ["PatoJAD"]
---

Nuestros amigos de CSIRT-CV Actualizaron sus manuales para Android e iOS y es una informacion util para todos los usarios de las respectivas plataformas. Lo ideal es siempre tenerlos a mano y darles una ojeada para estar al dia con nuestra seguridad.

Las guías explican los principales aspectos a tener en cuenta para utilizar de forma segura nuestros dispositivos: configuración ideal, instalación segura de aplicaciones, protección de la información y de las comunicaciones etc.




## GUÍA DE USO SEGURO DE IOS


{{< img src="https://concienciat.gva.es/wp-content/uploads/2018/03/uso-seguro-ios.jpg" >}}


Descarga esta guía para estar al día y disfrutar de forma segura de tu dispositivo iOS.

La presente guía explica los principales aspectos a tener en cuenta para utilizar un dispositivo iOS de forma segura: configuración ideal, instalación, seguridad de aplicaciones, protección de la información y de las comunicaciones. El análisis de la guía se va a realizar sobre dispositivos con una versión de iOS original, es decir, sobre los que no se ha realizado “jailbreak” que permitiría realizar otro tipo de acciones, ya que suprimiría las limitaciones impuestas por Apple.


{{< link url="https://concienciat.gva.es/wp-content/uploads/2018/03/infor_uso_seguro_ios.pdf" text="Descargar Manual para iOS" >}}



## GUÍA DE USO SEGURO DE ANDROID


{{< img src="https://concienciat.gva.es/wp-content/uploads/2018/03/seguridad_android.jpg" >}}


Descarga esta guía para estar al día y disfrutar de forma segura de tu dispositivo Android.

La presente guía explica los principales aspectos a tener en cuenta para utilizar un dispositivo Android de forma segura: configuración ideal, instalación segura de aplicaciones, protección de la información y de las comunicaciones. Quedan para otro momento temas avanzados como las implicaciones de hacer “root” a un dispositivo, cómo limitar los permisos de las aplicaciones o las ventajas y riesgos de instalar “roms” alternativas.


{{< link url="https://concienciat.gva.es/wp-content/uploads/2018/03/info_guia_uso_seguro_android.pdf" text="Descargar Manual para Android" >}}



Fuente: {{< textlink url="http://www.csirtcv.gva.es/" text="CSIRT-CV">}}

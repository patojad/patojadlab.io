---
title: '¿Compras Online? Estas en Riesgo'
date: '2020-06-24 12:17:00'
description: 'Los hackers aprovechan el uso de Google Analytics en sitios de comercio electrónico para robar las tarjetas de crédito.'
type: 'redesyseguridad'
tags: ["compras","online","robo","datos","tajetas","credito","inseguridad","google","analytics","hackers"]
category: ["redesyseguridad"]
img: 'https://thehackernews.com/images/-2tLoHFw6poU/XvHC4hPp1aI/AAAAAAAAAfc/QjDS17YJCnMcodRnVkNYHNCigRGgzpaKgCLcBGAsYHQ/s728-e100/google-analytics.jpg'
authors: ["PatoJAD"]
---



Todos sabemos que los sitios web de comercio electrónico que utilizan el servicio de **análisis web de Google** para rastrear visitantes, conocer su origen y mejorar sus ventas. Para esto suelen incluir en la lista blanca los dominios asociados en su política de seguridad de contenido (CSP). Los mal llamados "hackers" comenzaron a aprovechar esto para ejecutar código malicioso que les permite hacerse con los datos bancarios de los clientes que realizan compras online en los sitios atacados.



> Los atacantes inyectaron código malicioso en los sitios, que recopilaron todos los datos ingresados ​​por los usuarios y luego los enviaron a través de Analytics. Como resultado, los atacantes podían acceder a los datos robados en su cuenta de Google Analytics




## Un problema, una "solucion"



Hasta que se solucione este problema de la seguridad de ese servicio de Google, lo **único** que podemos hacer como clientes de la venta online, es navegar en **modo desarrollador**, ya que de esta manera se evita ser atacado por los "hackers". En el caso del navegador Chrome, basta con ir al menú *"Más herramientas"* y elegir *"Herramientas para desarrolladores"* (o mas simple tocando f12), reduciendo el tamaño de la ventana que se nos abrirá y donde no deberemos cambiar nada, ya que aparentemente podemos cambiar el contenido o aspecto de la web que estamos visitando, pero en realidad esos cambios no quedan guardados. En el caso de usar **Firefox** lo mas simple es usar ctrl + shift + D (No explico mucho Firefox porque tocando eso siempre sale, en cambio chrome y amigos suelen tener problemitas)


{{< img src="https://i.ibb.co/j8GXSfD/Screenshot-20200624-091057.png" >}}


Entendemos que esto no es realmente lo que podemos llamar una solución pero si es un “parche” a utilizar mientras esta inseguridad exista… Como sabe muchos yo no soy fanatico de estos “parches” pero cuando no podemos solucionarlo nosotros si nos sirven (Aunque entiendo que todos los usan Ubuntu *- Bugbuntu -* van a estar encantados de usar el parche y desean nunca obtener una solucion… Asi se mueve ubuntu *- Pequeño chiste -*)




## ¿Una solución tan simple?



Seamos realista, la solución o más bien “parche” es demasiado simple, como a un “hacker” no se le ocurre esto… En realidad esta “solución” se debe a un método que los “hackers”  utilizan para evitar ser descubiertos, al detectar el modo desarrollador no ejecuta nada evitando levantar sospechas



> Para hacer que los ataques sean más encubiertos, los atacantes también determinan si el modo desarrollador, una característica que a menudo se usa para detectar solicitudes de red y errores de seguridad, entre otras cosas, está habilitado en el navegador del visitante y procede solo si el resultado de esa verificación es negativo .



Como clientes, desafortunadamente, no hay mucho que podamos hacer para protegernos de estos tipos de ataques. Activar el modo desarrollador en los navegadores puede ayudar al realizar compras en línea.

Pero...



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

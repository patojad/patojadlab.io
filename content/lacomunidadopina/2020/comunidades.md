---
title: "Las comunidades de Linux..."
date: "2020-03-23 15:32:00"
description: "Linux esta lleno de comunidades online, esto se supone que deberia llenarnos de alegria pero..."
type: "comunidad"
tags: ["comunidad","problema","sensura"]
category: ["Comunidad"]
img: "https://pbs.twimg.com/media/ETfyy7nWkAAU5IG?format=png&name=small"
authors: ["PatoJAD"]
---

Linux, mas alla de todo tipo de encuesta, esta creciendo mucho, y eso lo podemos ver en la gran cantidad de comunidades que se generan, ya sea en Telegram, Facebook, o cualquier otra red social.




## Alegrias que duran poco



Esto genera mucha a alegria a personas como yo, ver que existen muchas comunidades y mas alla de los usuarios que siempre trollean muchos otros siempre estan dispuestos a ayudar y a buscar el bien comun.

Sin embargo a medida que mas conocemos las comunidades detectamos que no todas son lo que dicen ser y muchas generan mas daño al mismo GNU/Linux de lo que parecen...




## Mi propio Frankenstein



Hace años junto a un colega apodado @elpeladogeek iniciamos un proyecto llamado LatinLinux, una comunidad que venia a saldar este problema donde los que son mas experimentados ayuden amablemente a quienes no lo son y asi todos juntos podamos aprender.

Lamentablemente hoy en dia ni @elpeladogeek ni yo formamos parte de ese proyecto por varios motivos diferentes que nos llevaron a distanciarnos, por mi lado genere algo mas chico (este pequeño blog).

Sin embargo de la propia comunidad que cree fui expulsado por las dudas... o algo asi:


{{< img src="https://pbs.twimg.com/media/ETfyy7nWkAAU5IG?format=png&name=small" >}}


## La censura en las comunidades



Lamentablemente uno no entiende lo que pasa hasta que le pasa a uno mismo. Asi fue donde choque por primera vez una censura. Y reaccione en miles de comentarios que se realizan en muchas comunidades constantemente.




## El futuro de las comunidades



Cuanto mas tenemos que aguantar este tipo de cosas, Tenemos que nosotros mismos generar comunidades sanas y eso basta con entender que el otro es diferente y puede pensar diferente y sin necesidad de censurar sin motivo alguno.

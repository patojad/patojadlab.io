---
title: "Vota tus comunidades Favoritas"
date: "2021-03-16 08:43:00.00"
description: "Apoya a tus comunidades favoritas dandoles un voto en esta gran encuesta"
type: "comunidad"
tags: ["comunidad", "comunidades", "vota", "vlogs", "blogs", "podcast", "encuesta"]
category: ["Comunidad"]
img: "https://blogs.iadb.org/conocimiento-abierto/wp-content/uploads/sites/10/2018/01/Vota-Inteligente-banner-6.png"
authors: ["PatoJAD"]
---

El sitio web del Proyecto Tic Tac inició su primer encuesta del año. La misma busca encontrar los 10 Blogs, vlogs y podcast que la gente prefiere. Si bien este es un resumen arduo de todo el trabajo que realizan ellos nos gustaría invitarte a participar de su encuesta.

## Nuestros consejos

De más está decir que siempre nos gusta apoyar a nuestra comunidad, por eso queremos recomendarte algunas cosas que deberías conocer antes de votar porque seguro vas a querer que lleguen a lo más alto:

{{< img src="https://image.freepik.com/vector-gratis/diseno-voto-elecciones-dibujos-animados_24877-14734.jpg" >}}

* {{< textlink url="https://www.youtube.com/channel/UCl8XUDjAOLc7GNKcDp9Nepg" text="Locos Por Linux" >}}: Claramente este canal no necesita presentación y menos de un blog de esta escala, pero si por esas casualidades no conoces este canal de Youtube te recomiendo que veas algun video antes de votar porque claramente vas a terminar dándole el primer voto a ellos.
* {{< textlink url="https://proyectotictac.com/" text="Proyecto TicTac" >}}: Claramente Tampoco necesita presentación pero si es necesario remarcar que el gran trabajo que ellos realizan lo vuelven una de las primeras opciones a tener en cuenta a la hora de dar un voto.
*Linuxito: Un gran e importante blog que esta relacionado estrechamente con el canal de Telegram que cambió la forma de jugar en Linux

También recuerden que tenemos nuestro blog disponible para votar así que si quieren tranquilamente pueden votarlo, claro, si creen que vale un voto.

## Reglas de la encuesta

{{< img src="https://www.larepublica.net/storage/images/2019/09/30/20190930071736.20190826094033voto.jpg" >}}

La encuesta es de múltiple selección, eso quiere decir que podes elegir hasta 10 opciones antes de enviar tu resultado. Podes votar a tus sitios favoritos, youtubers, podcasters y obviamente a todas esas comunidades que buscamos cambiar el terreno de linux (Las mencionadas arriba por ejemplo)

{{< link url="https://proyectotictac.com/2021/03/15/encuesta-10-preferidos-blogs-vlogs-audioblogs-podcasts-espanol-software-libre-codigo-abierto-gnu-linux-2021/" text="Ir a la Encuesta" >}}
10

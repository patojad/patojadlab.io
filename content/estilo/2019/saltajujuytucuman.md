---
title: "Regalo de Vacaciones"
date: "2019-03-06"
description: "Despues de estas vacaciones les traigo un pequeño regalo"
type: "estilo"
tags: ["fondos","GTK","KDE","QT","wallpapers"]
category: ["Estilo"]
img: "https://i.postimg.cc/W403DrFy/patojad.jpg"
authors: ["PatoJAD"]
---

¡Buenas! Ya estamos de regreso después de unas vacaciones por el norte argentino. Este post merece una pequeña explicación, eventualmente cuando una persona va de vacaciones suele traer recuerdos o regalos para sus seres queridos. ¡Estos días pensé mucho en mi comunidad y en que podía traerle (Imaginan que distribuir alfajores se hacía un poco difícil) por lo cual mi mujer me dio una solución usando sus habilidades!

Lamentablemente no les traemos nada para comer... Pero si algo para poder tener en su fondo de pantalla. A mi mujer le gusta mucho la fotografía (Aunque aún no estudio para esto) pero debo decir que se lució con las imágenes que nos dejó para adornar nuestros escritorios.

### ¿Que podemos encontrar?

El zip contiene 51 imágenes, algunas con el nombre del blog, otras simplemente dicen Linux y otras dejan hablar a la naturaleza por sí misma. Les dejo una muestra de algunas de las imágenes que podemos encontrar entre otras tantas

{{< img src="https://i.postimg.cc/RZkhVGFf/el-obelisco.jpg" >}}

{{< img src="https://i.postimg.cc/5032gVmq/el-sapo.jpg" >}}

{{< img src="https://i.postimg.cc/cHZJfHfL/garganta-del-diablo.jpg" >}}

{{< img src="https://i.postimg.cc/9QRzhsWC/linux.jpg" >}}

{{< img src="https://i.postimg.cc/kMs2Fx13/salinas.jpg" >}}



{{< link url="https://www.mediafire.com/file/an2kqaitaafv0o1/Salta%2C_jujuy_y_Tucuman.zip/file" text="Descarga" >}}

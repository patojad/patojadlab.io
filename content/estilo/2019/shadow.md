---
title: "Shadow"
date: "2019-09-24"
description: "Cambia la forma en que miras tu escritorio. Obtenga un tema de iconos cuidadosamente diseñado, brillante y limpio para Gnome Desktop."
type: "estilo"
tags: ["icons","GTK","KDE","QT"]
category: ["Estilo"]
img: "https://cn.opendesktop.org/img/a/f/7/c/021c1e7d2a6452bd22f958294a9db254b872.png"
authors: ["PatoJAD"]
---

> Cambia la forma en que miras tu escritorio. Obtenga un tema de iconos cuidadosamente diseñado, brillante y limpio para Gnome Desktop.
>
> {{< citaname name="Equipo de Shadow">}}


{{< img src="https://cn.opendesktop.org/img/1/5/6/5/3bb9df3b9e5c354866e22379831898d62658.png" >}}


Como ellos afirman son iconos cuidadosamente diseñado y pensados para generar un sutil pero notable cambio a nuestro escritorio. Sin embargo, si bien fueron diseñados para Gnome todos aquellos que utilizamos KDE tenemos la oportunidad de probarlos con un hermoso acabado final.


 


## Nueva solicitud de icono


{{< img src="https://cn.opendesktop.org/img/a/f/7/c/021c1e7d2a6452bd22f958294a9db254b872.png" >}}


Su equipo o mejor dicho su usuario creador nos da la posibilidad de realizar la petición de nuevos iconos. Si bien no nos asegura poder implementar todo lo que pidamos el promete hacer todo lo posible por tratar de expandir el theme de iconos. Esto podemos hacerlo a través de su GitHub, en su repositorio agregando un {{< textlink text="Issu" url="https://github.com/rudrab/Shadow/issues" >}} con la etiqueta "Icon request"



## Instrucciones para Mate y Elementary

El archivo changecolor.py puede ser reescrito por completo. Esto nos da la posibilidad de elegir el color independientemente del escritorio que tenga. Esto nos da la posibilidad de elegir entre nosotros el color que queramos y podemos modificarlo a gusto. El autor nos pide informar en su GitHub si el archivo falla.



## Descargar

{{< link text="Genome Look" url="https://www.gnome-look.org/content/show.php/Shadow?content=170398" >}}

{{< link text="GitHub" url="https://github.com/rudrab/Shadow" >}}

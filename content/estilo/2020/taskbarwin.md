---
title: "TaskBarX Windows"
date: "2020-10-05 11:29:00"
description: "Hoy hablamos de TaskBarX una app que permite editar los registros para mejorar la visualizacion de la Barra de Tareas."
type: "estilo"
tags: ["taskbarx", "barra", "tareas", "windows", "task", "bar", "windows", "centrar", "iconos", "blur"]
category: ["Estilo"]
img: "https://chrisandriessen.nl/images/rsz_hero3.png"
authors: ["PatoJAD"]
---



Como hablamos con anterioridad en *{{< textlink url="/noticias/2020/09/el-camino-del-blog/" text="el camino del blog" >}}* nuestra idea es ir a por la tecnología en general. Y en este caso vamos a hablar de **Windows**, y un poco de como cambiar su estilo. Si bien esto no genera un **SUPER** cambio, nos permite darle un estilo un tanto más llamativo y desde mi punto de vista cambia muchísimo.


{{< img src="https://chrisandriessen.nl/images/Mockup.png" >}}


> TaskbarX le permite controlar la posición de los iconos de la barra de tareas. TaskbarX le dará una sensación original de dock de Windows. Los íconos se moverán al centro oa la posición dada por el usuario cuando se agregue o elimine un ícono de la barra de tareas. Se le dará la opción de elegir entre una variedad de animaciones diferentes y cambiar sus velocidades. Las animaciones se pueden desactivar si no le gustan las animaciones y desea que se muevan en un instante. La posición central también se puede cambiar para llevar sus iconos más hacia la izquierda o hacia la derecha según la posición central. Actualmente, todas las configuraciones de la barra de tareas son compatibles, incluida la barra de tareas vertical. Y barras de tareas ilimitadas :)




## ¿Un Dock en Windows?


{{< img src="https://www.deskmodder.de/blog/wp-content/uploads/2019/06/falconx-icons-in-der-taskleiste-zentrieren-windows-10-001.jpg" >}}


Esta app, que no es más que una UI que edita registros, nos permite darle un toque más estético a nuestro Windows, en base a eso nos permite agregar animaciones, blur, centrado de iconos, entre otras cosas. Esto deja un resultado que cambia en gran medida nuestro Escritorio.




## Descargar



Actualmente este programita se encuentra en la version 1.6.1.0 y podemos descargarlo desde su {{< textlink url="https://chrisandriessen.nl/taskbarx" text="página oficial" >}} o mas simple desde el siguiente enlace


{{< link url="https://github.com/ChrisAnd1998/TaskbarX/releases/download/1.6.1.0/TaskbarX_1.6.1.0.zip" text="Descargar" >}}



---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento

---
title: "Ubuntu Web Remix"
date: "2020-11-24 10:13:00.00"
description: "Distro ligera basada en MX Linux con un alto potencial"
type: "instalacion"
tags: ["ubuntu","web","instalacion", "remix", "manjaro", "webdad", "jade"]
category: ["instalacion"]
img:  "https://www.linuxadictos.com/wp-content/uploads/Ubuntu-Web.png.webp"
authors: ["PatoJAD"]
---


{{< img src="https://www.linuxadictos.com/wp-content/uploads/Ubuntu-Web.png.webp" >}}


Hace ya unos días comenzamos a escuchar sobre esta distribución en todos lados. Una distro basada en Ubuntu que pretende plantarle cara a **ChromeOS**. Ya está disponible la ISO para que cualquiera de nosotros la utilice y hoy vamos a hablar un poco de esta distro, como instalarla y mi opinión personal de lo que promete y lo que es.




## Instalación + Yapa



La instalación es realmente simple, les dejo a continuación el video con la instalación y una breve opinión sobre esta distro y lo que propone vs lo que ofrece.


{{< youtube code="Y_14ugXVv0w" >}}



## Mucho ruido y pocas nueces


{{< img src="https://liliputing.com/wp-content/uploads/2020/11/open-web-store-700x438.jpg" >}}


Realmente la propuesta de Ubuntu Web es interesante, sin embargo está lejos de que esa propuesta se convierta en algo real. Por ahora falta mucho que pulir, y el desarrollo que queda para avanzar es fuerte. Yo en particular creo que necesitaba más tiempo para poder salir pero su esfuerzo es valorable.




## Manjaro WebDad


{{< img src="https://i.ytimg.com/vi/_dqusjF4oqM/maxresdefault.jpg" >}}


Siempre digo que estos proyectos que no tienen gran impulso tienen mucho. En particular Manjaro WebDad es un manjaro con JADesktop un escritorio web muy bueno. Y su herramienta para apps conocida como JAK permite obtener muy buenos resultados en webapps nativas. Si bien UbuntuWeb Promete ser gran cosas creo que debería potenciarse un poco del trabajo realizado por la gente de WebDad. Si queres saber mas de este proyecto te dejo la web!


{{< link url="https://manjaro.org/downloads/developer/webdad-preview/" text="Manjaro WebDad" >}}


---



Recorda **apoyarnos** en nuestras redes para poder seguir adelante con todo lo que implica PatoJAD es un proyecto que poco a poco crece e intenta estar al lado de todos de los usuarios dándoles dia a dia herramientas o funcionalidades. Mostranos tu apoyo con pequeños gestos, desactivando el bloqueador de anuncios, apoyándonos en las redes sociales, donando o compartiendo el contenido, esto solo es posible gracias a cada lector y cada persona que demuestra su agradecimiento
